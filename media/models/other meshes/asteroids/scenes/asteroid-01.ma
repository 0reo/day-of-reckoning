//Maya ASCII 2009 scene
//Name: asteroid-01.ma
//Last modified: Thu, Nov 19, 2009 06:30:10 PM
//Codeset: 1252
requires maya "2009";
requires "Mayatomr" "10.0.1.8m - 3.7.1.26 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya Unlimited 2009";
fileInfo "version" "2009";
fileInfo "cutIdentifier" "200809110030-734661";
fileInfo "osv" "Microsoft Windows XP Service Pack 2 (Build 2600)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -46.505503435153464 26.239124316934912 27.72377963836864 ;
	setAttr ".r" -type "double3" -26.738352729565825 -59.399999999963548 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 60.269328410244825;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.1762750730459075 -0.87706533392378239 0.32475036336882557 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	setAttr ".t" -type "double3" 0 -0.0010086815389982462 0 ;
	setAttr ".s" -type "double3" 16.049302522966229 11.234885421575171 13.82197167143118 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	addAttr -ci true -h true -sn "OgreMaxObjectSettings" -ln "OgreMaxObjectSettings" 
		-bt "UNKN" -at "compound" -nc 97;
	addAttr -ci true -h true -sn "ID" -ln "ID" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserDataReference" -ln "UserDataReference" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserData" -ln "UserData" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserDataClass" -ln "UserDataClass" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "ExtendedValues" -ln "ExtendedValues" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 5;
	addAttr -ci true -h true -sn "ExtendedValueID" -ln "ExtendedValueID" -bt "UNKN" 
		-dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -sn "ExtendedValueAttributesType" -ln "ExtendedValueAttributesType" 
		-bt "UNKN" -dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesFloats" -ln "ExtendedValueAttributesFloats" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesStrings" -ln "ExtendedValueAttributesStrings" 
		-bt "UNKN" -dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesObjects" -ln "ExtendedValueAttributesObjects" 
		-bt "UNKN" -at "message" -p "ExtendedValues";
	addAttr -ci true -h true -sn "GenerateLOD" -ln "GenerateLOD" -bt "UNKN" -dv 2 -min 
		0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NumLODLevels" -ln "NumLODLevels" -bt "UNKN" -min 0 
		-max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODDistance" -ln "LODDistance" -bt "UNKN" -min 0 -max 
		100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODReductionType" -ln "LODReductionType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODReductionAmount" -ln "LODReductionAmount" -bt "UNKN" 
		-min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UpdateCounter" -ln "UpdateCounter" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Type" -ln "Type" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseInOgreMaxViewports" -ln "UseInOgreMaxViewports" 
		-bt "UNKN" -dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Visibility" -ln "Visibility" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VisibilityAffectObjectOnly" -ln "VisibilityAffectObjectOnly" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "TargetType" -ln "TargetType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsModel" -ln "IsModel" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ModelName" -ln "ModelName" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeAnimationSampleInterval" -ln "NodeAnimationSampleInterval" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeAnimationSampleType" -ln "NodeAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "NoteTrackNames" -ln "NoteTrackNames" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderQueueName" -ln "RenderQueueName" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderQueueAdd" -ln "RenderQueueAdd" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "CustomParameterIDs" -ln "CustomParameterIDs" -bt "UNKN" 
		-dv 2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "CustomParameterValues" -ln "CustomParameterValues" 
		-bt "UNKN" -at "double4" -p "OgreMaxObjectSettings" -nc 4;
	addAttr -ci true -h true -sn "CustomParameterValues0" -ln "CustomParameterValues0" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -sn "CustomParameterValues1" -ln "CustomParameterValues1" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -sn "CustomParameterValues2" -ln "CustomParameterValues2" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -sn "CustomParameterValuesx" -ln "CustomParameterValuesX" -bt "UNKN" 
		-dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -m -sn "QueryFlagNames" -ln "QueryFlagNames" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "QueryFlagBits" -ln "QueryFlagBits" -bt "UNKN" -dv 
		2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "VisibilityFlagNames" -ln "VisibilityFlagNames" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "VisibilityFlagBits" -ln "VisibilityFlagBits" -bt "UNKN" 
		-dv 2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsStatic" -ln "IsStatic" -bt "UNKN" -min 0 -max 1 
		-at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexBufferUsage" -ln "VertexBufferUsage" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexBufferShadowed" -ln "VertexBufferShadowed" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IndexBufferUsage" -ln "IndexBufferUsage" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IndexBufferShadowed" -ln "IndexBufferShadowed" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "TextureCoordinateSetCount" -ln "TextureCoordinateSetCount" 
		-bt "UNKN" -dv 1 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "MeshName" -ln "MeshName" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "FastExport" -ln "FastExport" -bt "UNKN" -dv 2 -min 
		0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AlwaysExportSkeleton" -ln "AlwaysExportSkeleton" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AlwaysExportPoses" -ln "AlwaysExportPoses" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RemoveBonesWithNoVertexInfluence" -ln "RemoveBonesWithNoVertexInfluence" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonName" -ln "SkeletonName" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "MeshAnimationType" -ln "MeshAnimationType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonAnimationSampleInterval" -ln "SkeletonAnimationSampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonAnimationSampleType" -ln "SkeletonAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexAnimationSampleInterval" -ln "VertexAnimationSampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexAnimationSampleType" -ln "VertexAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AnimatedRoot" -ln "AnimatedRoot" -bt "UNKN" -at "message" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExportMeshAnimationsToSeparateFile" -ln "ExportMeshAnimationsToSeparateFile" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LinkMeshAnimationsToMainObject" -ln "LinkMeshAnimationsToMainObject" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "PrimitiveType" -ln "PrimitiveType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExportVertexColors" -ln "ExportVertexColors" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateTangents" -ln "GenerateTangents" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateBinormals" -ln "GenerateBinormals" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SplitMirroredTangents" -ln "SplitMirroredTangents" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SplitRotatedTangents" -ln "SplitRotatedTangents" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "StoreTangentParityInW" -ln "StoreTangentParityInW" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateEdgeLists" -ln "GenerateEdgeLists" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseCustomTextureCoordinateSets" -ln "UseCustomTextureCoordinateSets" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "SubmeshesTextureCoordinateSets" -ln "SubmeshesTextureCoordinateSets" 
		-bt "UNKN" -at "compound" -p "OgreMaxObjectSettings" -nc 6;
	addAttr -ci true -h true -sn "TextureCoordinateSetSubmeshName" -ln "TextureCoordinateSetSubmeshName" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "TextureCoordinateSetMaterial" -ln "TextureCoordinateSetMaterial" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "TextureCoordinateSetMaterialObject" -ln "TextureCoordinateSetMaterialObject" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetSources" -ln "TextureCoordinateSetSources" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetDestinations" -ln "TextureCoordinateSetDestinations" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetTypes" -ln "TextureCoordinateSetTypes" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "LODType" -ln "LODType" -bt "UNKN" -min 0 -max 100 
		-at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "LODObjects" -ln "LODObjects" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "LODDistances" -ln "LODDistances" -bt "UNKN" -dv 
		3.4028234663852886e+038 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "FileName" -ln "FileName" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExternalItemType" -ln "ExternalItemType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "EnableSky" -ln "EnableSky" -bt "UNKN" -dv 1 -min 0 
		-max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "DrawSkyFirst" -ln "DrawSkyFirst" -bt "UNKN" -dv 1 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Scale" -ln "Scale" -bt "UNKN" -dv 1 -min 0 -max 100 
		-at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Curvature" -ln "Curvature" -bt "UNKN" -min 0 -max 
		100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Tiling" -ln "Tiling" -bt "UNKN" -at "float2" -p "OgreMaxObjectSettings" 
		-nc 2;
	addAttr -ci true -h true -sn "Tiling0" -ln "Tiling0" -bt "UNKN" -at "float" -p "Tiling";
	addAttr -ci true -h true -sn "Tiling1" -ln "Tiling1" -bt "UNKN" -at "float" -p "Tiling";
	addAttr -ci true -h true -sn "Bow" -ln "Bow" -bt "UNKN" -min 0 -max 100 -at "float" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardType" -ln "BillboardType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardOrigin" -ln "BillboardOrigin" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardCommonDirection" -ln "BillboardCommonDirection" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardCommonDirection0" -ln "BillboardCommonDirection0" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonDirection1" -ln "BillboardCommonDirection1" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonDirection2" -ln "BillboardCommonDirection2" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonUpVector" -ln "BillboardCommonUpVector" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardCommonUpVector0" -ln "BillboardCommonUpVector0" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardCommonUpVector1" -ln "BillboardCommonUpVector1" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardCommonUpVector2" -ln "BillboardCommonUpVector2" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardRotationType" -ln "BillboardRotationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardPoolSize" -ln "BillboardPoolSize" -bt "UNKN" 
		-min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AutoExtendBillboardPool" -ln "AutoExtendBillboardPool" 
		-bt "UNKN" -dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "CullBillboardsIndividually" -ln "CullBillboardsIndividually" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SortBillboards" -ln "SortBillboards" -bt "UNKN" -min 
		0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseBillboardAccurateFacingModel" -ln "UseBillboardAccurateFacingModel" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -uac -h true -sn "BillboardColor" -ln "BillboardColor" -bt "UNKN" 
		-at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardColorr" -ln "BillboardColorR" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardColorg" -ln "BillboardColorG" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardColorb" -ln "BillboardColorB" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle" -ln "BillboardTextureCoordinateRectangle" 
		-bt "UNKN" -at "double4" -p "OgreMaxObjectSettings" -nc 4;
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle0" -ln "BillboardTextureCoordinateRectangle0" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle1" -ln "BillboardTextureCoordinateRectangle1" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle2" -ln "BillboardTextureCoordinateRectangle2" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -sn "BillboardTextureCoordinateRectanglex" -ln "BillboardTextureCoordinateRectangleX" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -uac -h true -sn "LightSpecularColor" -ln "LightSpecularColor" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "LightSpecularColorr" -ln "LightSpecularColorR" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightSpecularColorg" -ln "LightSpecularColorG" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightSpecularColorb" -ln "LightSpecularColorB" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightAttenuationConstant" -ln "LightAttenuationConstant" 
		-bt "UNKN" -dv 1 -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LightAttenuationLinear" -ln "LightAttenuationLinear" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LightAttenuationQuadric" -ln "LightAttenuationQuadric" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateNormals" -ln "GenerateNormals" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "CreateMovablePlane" -ln "CreateMovablePlane" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsAttachedObject" -ln "IsAttachedObject" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IgnoreChildren" -ln "IgnoreChildren" -bt "UNKN" -min 
		0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ChildSorting" -ln "ChildSorting" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ChildOrder" -ln "ChildOrder" -bt "UNKN" -min 0 -max 
		100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderingDistance" -ln "RenderingDistance" -bt "UNKN" 
		-min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeTranslationInterpolationType" -ln "NodeTranslationInterpolationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeRotationInterpolationType" -ln "NodeRotationInterpolationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "NodeAnimations" -ln "NodeAnimations" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 14;
	addAttr -ci true -h true -sn "NodeAnimation_UpdateCounter" -ln "NodeAnimation_UpdateCounter" 
		-bt "UNKN" -min 0 -max 100 -at "long" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_AnimationName" -ln "NodeAnimation_AnimationName" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeStart" -ln "NodeAnimation_TimeStart" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeEnd" -ln "NodeAnimation_TimeEnd" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeScaleType" -ln "NodeAnimation_TimeScaleType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeScale" -ln "NodeAnimation_TimeScale" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_CopyFirstKeyToLast" -ln "NodeAnimation_CopyFirstKeyToLast" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_UseAnimationStartTime" -ln "NodeAnimation_UseAnimationStartTime" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_Enabled" -ln "NodeAnimation_Enabled" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_Looped" -ln "NodeAnimation_Looped" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_SampleInterval" -ln "NodeAnimation_SampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_SampleType" -ln "NodeAnimation_SampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TranslationInterpolationType" -ln "NodeAnimation_TranslationInterpolationType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_RotationInterpolationType" -ln "NodeAnimation_RotationInterpolationType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimations" -ln "MeshAnimations" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 23;
	addAttr -ci true -h true -sn "MeshAnimation_UpdateCounter" -ln "MeshAnimation_UpdateCounter" 
		-bt "UNKN" -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TrackName" -ln "MeshAnimation_TrackName" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_AnimationName" -ln "MeshAnimation_AnimationName" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeStart" -ln "MeshAnimation_TimeStart" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeEnd" -ln "MeshAnimation_TimeEnd" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeScaleType" -ln "MeshAnimation_TimeScaleType" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeScale" -ln "MeshAnimation_TimeScale" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_CopyFirstKeyToLast" -ln "MeshAnimation_CopyFirstKeyToLast" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportAsMorph" -ln "MeshAnimation_ExportAsMorph" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_MorphWholeObject" -ln "MeshAnimation_MorphWholeObject" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_UseAnimationStartTime" -ln "MeshAnimation_UseAnimationStartTime" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_Looped" -ln "MeshAnimation_Looped" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_SampleInterval" -ln "MeshAnimation_SampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_SampleType" -ln "MeshAnimation_SampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportToSeparateFile" -ln "MeshAnimation_ExportToSeparateFile" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_LinkToMainObject" -ln "MeshAnimation_LinkToMainObject" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportToSeparateFileInherited" -ln "MeshAnimation_ExportToSeparateFileInherited" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_LinkToMainObjectInherited" -ln "MeshAnimation_LinkToMainObjectInherited" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_BoneTranslationMaskObjects" -ln "MeshAnimation_BoneTranslationMaskObjects" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_BoneTranslationMasks" -ln "MeshAnimation_BoneTranslationMasks" 
		-bt "UNKN" -at "float3" -p "MeshAnimations" -nc 3;
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks0" -ln "MeshAnimation_BoneTranslationMasks0" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks1" -ln "MeshAnimation_BoneTranslationMasks1" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks2" -ln "MeshAnimation_BoneTranslationMasks2" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBonesObjects" -ln "MeshAnimation_AllowExportBonesObjects" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBoneAllows" -ln "MeshAnimation_AllowExportBoneAllows" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBoneRecursives" -ln "MeshAnimation_AllowExportBoneRecursives" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "MeshAnimations";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49419070339521987 0.50000005960464478 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 866 ".pt";
	setAttr ".pt[0:165]" -type "float3"  -0.23791325 -0.065120302 0.073518075 
		-0.17732081 -0.045277301 0.056276821 -0.13429156 -0.041150704 0.055182543 -0.086495414 
		-0.046229731 0.041177027 -0.046269141 -0.048303377 0.027340408 -0.023118887 -0.048917603 
		0.023180479 -0.011280158 -0.040386483 0.029549405 -0.0012417159 -0.029135644 0.042497959 
		0.011340334 -0.02152231 0.060405292 0.028237637 -0.019486014 0.079963505 0.052605934 
		-0.01796997 0.082374968 0.064849183 0.0037255939 0.050111853 0.045910127 0.023401765 
		0.022990165 -0.27121499 -0.032945573 0.12706205 -0.14410815 -0.025694862 0.10641716 
		-0.086314961 -0.019308407 0.095629044 -0.052672211 -0.025220109 0.092473999 -0.031537931 
		-0.033898264 0.091674365 -0.019161517 -0.03648987 0.09522295 -0.011869979 -0.032112982 
		0.10462347 -0.003441229 -0.025555314 0.11916726 0.0091387695 -0.020829877 0.140398 
		0.030376941 -0.020218298 0.16372257 0.068460084 -0.026092198 0.17972596 0.086438045 
		-0.015606868 0.1144613 0.080976315 -0.010770502 0.050147727 -0.26338485 -0.048748184 
		0.12411927 -0.14037541 -0.040779732 0.11522579 -0.066528708 -0.026574694 0.11382549 
		-0.036039811 -0.02203664 0.12319159 -0.024628326 -0.025972191 0.13445681 -0.018510092 
		-0.026999919 0.145413 -0.013698235 -0.023732778 0.15910414 -0.006717701 -0.017693104 
		0.17756897 0.0082761701 -0.011647157 0.19435389 0.033186868 -0.0050710179 0.2080193 
		0.072146364 -0.0032326104 0.19314957 0.1074017 -0.0021294656 0.15305233 0.11668786 
		-0.014395132 0.07690867 -0.30126795 -0.059985191 0.12831531 -0.14298494 -0.05303866 
		0.14378266 -0.055836953 -0.039632957 0.14664042 -0.029304966 -0.031068763 0.15424247 
		-0.022247024 -0.028885249 0.16816632 -0.018954478 -0.025929175 0.18334988 -0.016397722 
		-0.02085577 0.2000595 -0.0081836767 -0.014761889 0.21583371 0.010127573 -0.0071988772 
		0.22638123 0.040761527 0.0011583685 0.22471279 0.076144896 0.0019794165 0.2017291 
		0.10728382 0.0010475784 0.16664064 0.13087097 -0.016480522 0.090408735 -0.34316894 
		-0.042870875 0.11147917 -0.13873874 -0.043438349 0.1669919 -0.053053431 -0.037565921 
		0.17757036 -0.026812932 -0.036429014 0.18630268 -0.021784374 -0.036141563 0.19874507 
		-0.016945809 -0.033090848 0.21111977 -0.013597476 -0.028183455 0.224516 -0.0047039432 
		-0.022202019 0.23647633 0.013718301 -0.013775538 0.24323322 0.046400044 -0.010832502 
		0.23212168 0.080333702 -0.0095798736 0.20862646 0.10892938 -0.01472595 0.17230143 
		0.14701873 -0.023481851 0.10409562 -0.38663846 -0.020771116 0.14319696 -0.11846374 
		-0.018119222 0.1937063 -0.041045211 -0.026063468 0.2046064 -0.028246695 -0.03540124 
		0.2153234 -0.022818752 -0.042144585 0.22880854 -0.012788024 -0.039960008 0.23589115 
		-0.011806252 -0.037476465 0.24589096 -0.0049677948 -0.034709085 0.25614557 0.013963936 
		-0.0334104 0.25911132 0.04982001 -0.031869784 0.24709269 0.087472349 -0.03103435 
		0.22107683 0.11715205 -0.032982793 0.18508697 0.16930981 -0.027707739 0.12132958 
		-0.25277421 0.020459116 0.13762519 -0.029348468 0.016444938 0.211944 -0.025643559 
		-0.011315086 0.21688144 -0.037489951 -0.029945863 0.23566169 -0.028897481 -0.040725194 
		0.25875172 -0.0080446582 -0.042409655 0.25900832 -0.013497521 -0.045989923 0.26662397 
		-0.012664929 -0.045637362 0.28102025 0.0079230266 -0.044757839 0.28611153 0.052537423 
		-0.046977662 0.27483848 0.10114088 -0.042551506 0.24199477 0.12741481 -0.034042589 
		0.20571503 0.19049925 -0.016822223 0.14482036 -0.13190067 0.04052975 0.077258095 
		-0.044944547 0.020437026 0.15909357 -0.031717062 -0.003375862 0.20471676 -0.058107894 
		-0.021454982 0.24790935 -0.03160619 -0.021775741 0.28491181 -0.01318959 -0.035237852 
		0.2848801 -0.01716022 -0.043521736 0.29553425 -0.024027472 -0.041970085 0.31180421 
		-0.011560213 -0.037169345 0.32541582 0.054084986 -0.033836346 0.31886575 0.12570252 
		-0.023920851 0.26257718 0.12185729 -0.014908333 0.22691326 0.17593688 0.0093307346 
		0.18950072 -0.076356076 0.040085912 0.044409815 -0.048515636 0.020368092 0.11839549 
		-0.041528046 0.0043793735 0.17435053 -0.045930084 -0.0050347513 0.22892629 -0.024621375 
		0.0014188974 0.25235346 -0.041304141 -0.0037249648 0.28001416 -0.028855573 -0.023956979 
		0.32926244 -0.022170508 -0.015813092 0.3365097 -0.026843593 0.014816427 0.34184963 
		0.044319786 0.034918502 0.31876573 0.12677082 0.036867861 0.2512742 0.095567383 0.025349073 
		0.21574442 0.18953273 0.029403547 0.13780096 -0.042350244 0.031912122 0.018609613 
		-0.039218672 0.017041927 0.08332029 -0.037760988 0.0072865537 0.13521706 -0.03628229 
		0.0031652909 0.17507704 -0.029027477 0.0044026463 0.19908665 -0.028871555 0.0045479089 
		0.22263028 -0.024498373 0.0044073639 0.24789329 -0.029964888 0.018435001 0.2667836 
		-0.0016630238 0.040217336 0.2832303 0.054466825 0.059062369 0.26258537 0.078010023 
		0.041571759 0.2121938 0.10253894 0.030046884 0.16359173 0.17275974 0.036904041 0.096028827 
		-0.015306655 0.016612049 -0.0057814419 -0.024880383 0.0065296441 0.047382485 -0.029351182 
		0.0026722956 0.091404207 -0.028971208 0.0028772359 0.12320463 -0.025403123 0.0073629734 
		0.1474053 -0.021780539 0.014526679 0.16805272 -0.017584298 0.024928784 0.18679522 
		-0.014286038 0.038035292 0.19859625 0.00025359701 0.05024619 0.20354691 0.027394515 
		0.048943244 0.19120589 0.056470279 0.031760436 0.15596595 0.088817142 0.023478266 
		0.11245399 0.13517506 0.029825402 0.053276181 0.013207618 -0.011752082 -0.032571118 
		-0.010236546 -0.014418365 0.0090152286 -0.01994016 -0.01242119 0.041385259 -0.022254692 
		-0.006179458 0.067962714 -0.01986281 0.0027080961 0.087005183 -0.016086683 0.012862995 
		0.10059132 -0.012479053 0.028865675 0.11478806 -0.010557311 0.03629737 0.12002529 
		-0.002229101 0.034254417 0.12049972 0.018962145 0.022534039 0.11080389 0.04509199 
		0.0096947215 0.089953803 0.071922988 0.0025439188 0.058874361 0.08597064 0.00045952294 
		0.01018781 0.033741362 -0.057384331 -0.051417373 -0.0016381033 -0.04755418 -0.033830807 
		-0.014328996 -0.037632357 -0.014006564 -0.016272778 -0.02996444 0.00062366202 -0.014362935 
		-0.021448722 0.01153316 -0.011378767 -0.0077816732 0.021659469 -0.0091344528 0.0056317318 
		0.028345829 -0.0086152609 0.0079967445 0.030130414 -0.0017859061 -0.0022642992 0.029457228 
		0.016492959 -0.014828829 0.026009697;
	setAttr ".pt[166:331]" 0.04139613 -0.026467908 0.016370928 0.060731068 -0.039946731 
		-0.0026330296 0.051803719 -0.04907601 -0.01972794 0.013581517 -0.035450876 -0.012457894 
		-0.010963198 -0.0030063763 -0.0033542253 -0.013955267 0.010156874 0.0029033795 -0.012211057 
		0.014993813 0.008482445 -0.0097021516 0.020671707 0.012990396 -0.007188709 0.029748352 
		0.016417721 -0.0055451104 0.036408868 0.017103968 -0.005735945 0.035624012 0.016466828 
		-1.5021651e-005 0.02211971 0.017556949 0.018981522 0.012641657 0.019083241 0.047287971 
		0.0033484865 0.017096505 0.073173217 -0.010261901 0.011862498 0.084652975 -0.031809147 
		0.011071887 -0.0047725635 -0.011864623 0.00048224148 -0.013947764 0.023744537 0.0015871844 
		-0.012229721 0.047167908 0.004366531 -0.0071175843 0.043819424 0.0062965695 -0.0040635373 
		0.041437481 0.0062295725 -0.0027263055 0.041704345 0.0062329336 -0.0015891984 0.040000077 
		0.0026045106 -0.0014152626 0.030960739 0.0020011687 0.0016433271 0.02253443 0.00525002 
		0.021081591 0.0234062 0.01006774 0.055934645 0.021463975 0.013149367 0.092451096 
		0.012866441 0.01425189 0.1220794 -0.011038635 0.017587975 -0.015264826 0.0086065754 
		0.0013311593 -0.015813725 0.03979677 -0.00016958152 -0.0083425045 0.042359628 6.6822293e-005 
		-0.0036549594 0.035145205 0.00086586521 -0.0012310413 0.028969646 0.0008189521 -8.1020524e-005 
		0.025046991 0.0003076366 0.00086349971 0.019073792 -0.0007405738 0.0021274402 0.012012661 
		-0.00085256307 0.004644447 0.005892999 1.9971165e-005 0.026798006 0.01903994 0.0038183089 
		0.06304875 0.030594748 0.0071396055 0.10832521 0.030772813 0.0094803292 0.15270159 
		0.0077537838 0.012166001 -0.010271622 0.0094455406 -0.00027499374 -0.010775452 0.039759457 
		-0.0017093415 -0.0038840319 0.036067255 -0.0013974755 -2.0699896e-005 0.027912518 
		-0.0008103911 0.0014379806 0.021194965 -0.00053764845 0.0020316674 0.016448973 -0.00048653039 
		0.0031259435 0.012241158 -0.00041188876 0.0057329256 0.0081174057 -0.00012627011 
		0.012608452 0.0079552 4.3218868e-005 0.030378282 0.013954064 0.00044812571 0.066345029 
		0.033119392 0.0020600387 0.11636411 0.040696852 0.0031236408 0.17241195 0.020503059 
		0.0035493274 0.0075127459 -0.0067468155 -0.0029823871 0.0010288329 0.028454516 -0.0049545304 
		0.004468129 0.028745543 -0.0026880978 0.0052272058 0.02326992 -0.0011958319 0.0046321489 
		0.018427541 -0.00046048232 0.0042576501 0.015538404 -0.00016000109 0.0050999937 0.013660979 
		-6.8296977e-006 0.0089255115 0.01431264 0.00013333878 0.018550353 0.01813552 0.00023409983 
		0.039015971 0.02253921 0.00051193562 0.069628693 0.03237408 0.00075476232 0.12046859 
		0.041871287 0.00085966173 0.18072288 0.025611771 6.5417145e-005 0.025865579 -0.024143953 
		-0.0054670325 0.019354369 0.0053218827 -0.0046776258 0.015801292 0.01621021 -0.0020703038 
		0.011947631 0.019693311 -0.00053204061 0.0087933298 0.018981 0.00030125593 0.0066714943 
		0.018670436 0.00038933978 0.0064415764 0.019154921 0.00026556116 0.010302348 0.023120536 
		0.00028125549 0.021491535 0.031279411 0.00038271493 0.043665383 0.038260758 0.00065934792 
		0.076914407 0.041323375 0.0016062498 0.11883231 0.043165382 0.0032388454 0.17304836 
		0.03686934 0.0057589356 0.047328938 -0.048995484 -0.0019975847 0.037310563 -0.018784944 
		6.756006e-005 0.028599259 0.0017785821 0.0014991976 0.021102445 0.012566157 0.0023129426 
		0.014512708 0.019526264 0.0019668923 0.0096930424 0.023977041 0.0010821254 0.0085474411 
		0.027104979 0.00024268361 0.012765807 0.033800807 0.00022645816 0.024431882 0.043857407 
		0.00047748938 0.045737408 0.051755853 0.00064694288 0.079226516 0.052259266 0.0014864806 
		0.12322421 0.043040298 0.0042565186 0.18503137 0.019247588 0.011935465 0.067609623 
		-0.070712939 0.005189715 0.054185409 -0.038468134 0.0076312451 0.041616384 -0.013033962 
		0.0080186278 0.030434616 0.0051921532 0.0069177942 0.021315113 0.017686106 0.0045320583 
		0.014705952 0.027463598 0.0015732099 0.013977818 0.033357523 -0.00042667214 0.017679373 
		0.042571913 -0.00094873086 0.028012468 0.052212536 -0.00037518077 0.04823773 0.057927962 
		0.00015541562 0.07899563 0.057169564 0.0005636033 0.12003306 0.044030566 0.0018550205 
		0.17355116 0.012057648 0.0045318888 0.087378368 -0.09105657 0.017515358 0.070261128 
		-0.056743063 0.019001035 0.054099403 -0.029755207 0.017708985 0.039974786 -0.0061856983 
		0.013653298 0.028958401 0.010926734 0.0077944328 0.022662302 0.020321837 0.0028737327 
		0.01961486 0.032794021 -0.0016049262 0.020680755 0.047132038 -0.0037057493 0.030893052 
		0.056582011 -0.0039925459 0.049288258 0.061693523 -0.0021729276 0.077088617 0.060200404 
		-0.0012007497 0.11201009 0.045895614 -0.00074927398 0.14758977 0.0054804171 -0.0002415046 
		0.1063249 -0.11059407 0.035678573 0.084747009 -0.075996816 0.034993812 0.064806074 
		-0.05015764 0.031008083 0.048803847 -0.029508481 0.024030965 0.037036724 -0.012092038 
		0.015127055 0.028846152 0.0048396033 0.0055545163 0.023697464 0.024539148 -0.0036583089 
		0.024077173 0.04168766 -0.0096392231 0.033438489 0.054089509 -0.01231685 0.051204462 
		0.060000326 -0.0091857836 0.077108383 0.058795597 -0.005519053 0.1071885 0.046250172 
		-0.0056105573 0.12696128 8.2217157e-005 -0.0053829788 0.12364641 -0.12850018 0.062762313 
		0.093602188 -0.10224257 0.059738044 0.070281178 -0.081048764 0.051896185 0.053344697 
		-0.061740149 0.040776987 0.041405786 -0.042115174 0.026821963 0.032654643 -0.020536885 
		0.011082482 0.026336141 0.0016860217 -0.003793275 0.025533538 0.023116793 -0.016871562 
		0.034787662 0.039857067 -0.025272174 0.054069117 0.047639638 -0.021686511 0.079230092 
		0.045880094 -0.01238893 0.099915706 0.027271885 -0.009501609 0.10155768 -0.0098588401 
		-0.0071042306 0.13652651 -0.14232048 0.10843508 0.093624279 -0.13052897 0.095618077 
		0.067924559 -0.11615579 0.08074379 0.051610589 -0.10086934 0.065046236 0.041133117 
		-0.083396107 0.046823077 0.033160623 -0.064621195 0.027318239 0.027007481 -0.043091651 
		0.005326869 0.024496114 -0.018090416 -0.019440379 0.033469394 0.0050995713 -0.04188538 
		0.059327543 0.0086287763 -0.035288416 0.088120751 0.0037631195 -0.018084869 0.096595138 
		-0.013483678 0.0038932899 0.080050692 -0.024057144 0.018208234 0.1063163 -0.09163247 
		0.093018949 0.072029978 -0.089200802 0.064272776 0.051442888 -0.080654643 0.040595833 
		0.039642267 -0.069853321 0.020279344 0.033377841 -0.056705017 -0.00092595443 0.029673938 
		-0.040337164 -0.026224269 0.024408827 -0.023301847 -0.053789001;
	setAttr ".pt[332:497]" 0.020702865 -0.0094287563 -0.084828161 0.02735908 -0.0014945554 
		-0.12735018 0.066220529 0.00023786118 -0.11602744 0.094814152 0.0050705471 -0.083026662 
		0.10594469 0.0058102217 -0.043038588 0.098233551 0.0039510326 0.0042024944 0.058545977 
		-0.057452746 0.071549274 0.034040924 -0.058642838 0.036180999 0.021024227 -0.054914158 
		0.0099125691 0.017818205 -0.047351867 -0.014227491 0.018263701 -0.038849663 -0.035747524 
		0.022201499 -0.02764703 -0.063191153 0.018249152 -0.015781652 -0.092135407 0.010128481 
		-0.011131529 -0.12228893 0.019136215 -0.017465675 -0.14177094 0.059743743 -0.0074341847 
		-0.15145707 0.096880876 0.010515669 -0.12807529 0.11960113 0.021179108 -0.083043873 
		0.12872201 0.024148833 -0.024629207 -0.0049464228 -0.034282807 0.0520841 -0.019312186 
		-0.037446395 0.015057136 -0.022861458 -0.036300458 -0.012082919 -0.017607097 -0.032240286 
		-0.034790225 -0.006194442 -0.026793351 -0.057049137 0.011662629 -0.020488234 -0.085705362 
		0.011617684 -0.0097832978 -0.11479297 0.0054917913 -0.0037763256 -0.13627753 0.0074642384 
		0.0013482923 -0.15976524 0.047656752 0.011901437 -0.18748635 0.094212018 0.031437382 
		-0.17633358 0.13622478 0.044413928 -0.13446262 0.17335427 0.050526477 -0.074205294 
		-0.079091758 -0.017308898 0.028299343 -0.08046063 -0.021576589 -0.0036009927 -0.07495176 
		-0.022163421 -0.025746683 -0.061939381 -0.020154731 -0.043999869 -0.044218894 -0.015929097 
		-0.065117911 -0.0220751 -0.0095820632 -0.092308015 -0.0036448294 -0.00014678536 -0.12668405 
		0.0037259033 0.003540098 -0.14775445 0.0022791619 0.00084048149 -0.17878513 0.032361157 
		0.013561295 -0.22725314 0.079784162 0.042338502 -0.23206957 0.14767936 0.061491039 
		-0.20096497 0.23263262 0.073628478 -0.14590682 -0.15494646 -0.006239892 0.0025003757 
		-0.14104906 -0.010383039 -0.02016669 -0.12672751 -0.01160165 -0.035248358 -0.10573128 
		-0.01061729 -0.049894139 -0.081389472 -0.0071020261 -0.071054928 -0.050190829 -0.00094594166 
		-0.10336315 -0.015864044 0.0076545454 -0.14331657 0.010797554 0.012073747 -0.14857255 
		0.0066522714 0.0059832563 -0.18984982 0.010895403 0.0012570164 -0.27166653 0.028319603 
		0.036473177 -0.2948592 0.13304245 0.036859881 -0.24294889 0.24263497 0.047813725 
		-0.17616697 -0.20981413 -0.0016975163 -0.01202555 -0.18967247 -0.0035308988 -0.03075633 
		-0.16798677 -0.0047117374 -0.038896564 -0.14315024 -0.0041702855 -0.051305596 -0.11010778 
		-0.0016300943 -0.075283177 -0.070153013 0.0034141981 -0.11471476 -0.023374302 0.010482243 
		-0.16321425 0.019869365 0.016119299 -0.17463477 0.017601935 0.012818558 -0.21302976 
		0.010757258 -0.0087024979 -0.28615677 0.012838953 -0.011714701 -0.28294918 0.12118436 
		-0.0015477992 -0.24829233 0.22124358 0.011703985 -0.17948009 -0.23732543 -0.00024724426 
		-0.020792175 -0.21537605 -0.00036036206 -0.030879261 -0.19491667 -0.00091536349 -0.036389507 
		-0.16564685 -0.00090053229 -0.048928928 -0.12794898 0.00049156474 -0.074165702 -0.081792407 
		0.0033302766 -0.11369912 -0.028367367 0.0072369231 -0.16455954 0.019806003 0.011415697 
		-0.20449959 0.027228596 0.016701331 -0.23367016 0.030119183 0.004460861 -0.28700531 
		0.050480332 -0.029547686 -0.27552018 0.1248217 -0.028919658 -0.2403847 0.20807639 
		-0.013103571 -0.16500118 -0.2436493 0.0012363403 -0.013970154 -0.22475621 0.0010769186 
		-0.025084071 -0.20499 0.00040955067 -0.02981328 -0.17640373 0.00018069735 -0.043483555 
		-0.13728945 0.00068342761 -0.069478728 -0.08826571 0.0018102748 -0.10803135 -0.032462016 
		0.0030522298 -0.15814324 0.014304227 0.0045670792 -0.21448933 0.032469831 0.0092403684 
		-0.2480305 0.062059239 -0.0071822139 -0.28191841 0.082433335 -0.034753278 -0.25989056 
		0.13221483 -0.034940779 -0.21790183 0.19375174 -0.022481918 -0.14481817 -0.23285659 
		0.00068131526 -0.0030753729 -0.22318035 0.00085175381 -0.016774043 -0.20842102 0.00051670818 
		-0.021186545 -0.18313093 0.00012493237 -0.034410633 -0.14310458 2.9436414e-006 -0.061188411 
		-0.092139065 0.00054076669 -0.099474199 -0.034614377 0.00089296186 -0.14751466 0.0082375603 
		-0.0041661575 -0.20219532 0.027780307 -0.0098641338 -0.22678994 0.079868905 -0.026174376 
		-0.27080014 0.093909971 -0.033879422 -0.23283836 0.12857613 -0.029946115 -0.18652247 
		0.17096627 -0.024012225 -0.11800387 -0.21986344 -0.00039395606 0.019704573 -0.21963541 
		0.00077303388 -0.0049934643 -0.20998916 0.00052454614 -0.012222449 -0.19069611 -0.00047198468 
		-0.021855392 -0.15171185 -0.001558212 -0.046970781 -0.095122211 -0.00073984492 -0.087228514 
		-0.032412607 0.0052508945 -0.13432367 0.0092730829 0.011719454 -0.18146369 0.041213945 
		-0.012317325 -0.19824353 0.07014548 -0.030660219 -0.19922845 0.089587927 -0.028030727 
		-0.17815553 0.11590561 -0.022226023 -0.14173712 0.14074357 -0.021684917 -0.083090886 
		-0.20465527 0.0043459916 0.034321479 -0.21290573 0.0058566649 0.013609478 -0.20668939 
		0.0041927584 0.0045775641 -0.18882763 2.78636e-005 -0.0077814935 -0.15197156 -0.005905929 
		-0.03115957 -0.091039725 -0.011041136 -0.07016065 -0.028670551 -0.0040811598 -0.10964628 
		0.0075566759 -0.001136127 -0.13494593 0.032424264 -0.010246398 -0.12123397 0.057705726 
		-0.01580747 -0.11382519 0.080718748 -0.016438195 -0.10497559 0.10061242 -0.010271243 
		-0.083114967 0.10487203 -0.0089292042 -0.03917481 -0.18314451 0.034745686 0.049750697 
		-0.20813906 0.022544267 0.040277883 -0.20624876 0.015466694 0.036338884 -0.19117787 
		0.006052847 0.026481675 -0.1533218 -0.0090359449 0.0043438505 -0.079896517 -0.034423131 
		-0.039508995 -0.019338652 -0.026595321 -0.048640512 0.0038209266 -0.00697141 -0.037947826 
		0.025217094 0.001611253 -0.029521091 0.048836701 0.0039913747 -0.025915531 0.071982972 
		0.0057522273 -0.023808887 0.088660136 0.013947723 -0.018337199 0.07660196 0.025364695 
		-0.0069696899 -0.21209905 0.002367038 0.0019248722 -0.21565446 -0.031943314 0.0021451712 
		-0.2093263 -0.045268714 0.0039655566 -0.1978412 -0.057917345 0.0024104838 -0.16404146 
		-0.079489209 -0.0063183056 -0.065608844 -0.1114055 -0.0087631922 -0.013184894 -0.091957748 
		-0.014093698 0.0028534913 -0.066325679 -0.016836438 0.020272495 -0.053999476 -0.016568085 
		0.043248363 -0.048685562 -0.018587403 0.069564693 -0.043669503 -0.022106664 0.094266519 
		-0.031271692 -0.026686642 0.10730356 -0.0029937918 -0.034200098 -0.25344843 -0.04491742 
		-0.022733513 -0.22471394 -0.08846724 -0.019435028 -0.209503 -0.096563525 -0.013535686 
		-0.205755 -0.1058848 -0.0070515196;
	setAttr ".pt[498:663]" -0.17639697 -0.1263485 -0.0056417137 -0.041422434 -0.14072582 
		-0.003623852 -0.0047295187 -0.12663344 -0.001747306 0.0033113246 -0.10178949 -0.0040278733 
		0.018529098 -0.091431744 -0.006973193 0.040795203 -0.087836429 -0.011645985 0.06915053 
		-0.084817983 -0.017218405 0.10367998 -0.073496006 -0.024630211 0.13857971 -0.039125115 
		-0.033455025 -0.30515414 -0.10608988 -0.026875759 -0.22468358 -0.14362617 -0.020728661 
		-0.19231988 -0.13482973 -0.015066126 -0.17834887 -0.13613744 -0.0070183864 -0.13958313 
		-0.14495473 -0.0031744065 -0.036929954 -0.14436193 -0.00058732048 -0.00010859504 
		-0.14246315 -5.2615091e-005 0.0019105655 -0.12035687 -0.00035521865 0.020596506 -0.11647815 
		-0.0028401103 0.036619972 -0.11960872 -0.005439139 0.064833425 -0.11914016 -0.010500723 
		0.10611412 -0.10671881 -0.017720755 0.15349734 -0.076554947 -0.026207346 -0.34545889 
		-0.14444889 -0.0056016236 -0.24308564 -0.17282726 -0.0032536385 -0.18013355 -0.16523893 
		-0.0031834401 -0.15311043 -0.15624677 -0.001613649 -0.11433733 -0.15571986 0.00012108891 
		-0.041830178 -0.15432538 0.0012052082 -4.4623786e-005 -0.14434917 3.6113574e-006 
		2.1145777e-006 -0.13017534 -1.7138934e-006 0.032544609 -0.13468494 -0.0017152097 
		0.035145786 -0.13797435 0.00033746159 0.058473323 -0.13610512 -0.0030760928 0.1002818 
		-0.12656437 -0.0092997393 0.15002954 -0.082664587 -0.016018897 -0.34980577 -0.17426147 
		0.011919605 -0.24128205 -0.19906633 0.011428278 -0.1602388 -0.18458202 0.0079694996 
		-0.13468583 -0.16826375 0.0050309566 -0.099444255 -0.16284001 0.0041792472 -0.043067779 
		-0.15907562 0.0031926581 -9.4772317e-005 -0.14460695 4.0873652e-005 1.8719226e-007 
		-0.13579431 -1.1061358e-007 0.029328691 -0.13886437 -0.00094994513 0.032259502 -0.1387569 
		-0.0012626466 0.054481771 -0.13158795 -0.0019787329 0.091841832 -0.11321164 -0.0033812192 
		0.1374469 -0.070217848 -0.0050976025 -0.34685212 -0.19570695 0.011294175 -0.24242489 
		-0.2127137 0.014103623 -0.16506964 -0.19024853 0.012521254 -0.12803133 -0.17319225 
		0.0097648464 -0.093633786 -0.16729805 0.0077503202 -0.046810403 -0.16583799 0.0053818254 
		-0.0026695766 -0.14866312 0.00058992777 0.0019373999 -0.14415453 -0.00038194208 0.023653869 
		-0.14066727 -0.0032683972 0.025300037 -0.13947822 -0.0024600653 0.047655731 -0.12847796 
		0.00095740263 0.084687635 -0.10490245 0.0036655187 0.12913218 -0.065830216 0.0058666337 
		-0.35719529 -0.20099746 0.019134803 -0.24785303 -0.21764645 0.019858787 -0.1750471 
		-0.19059768 0.017236056 -0.12963934 -0.173491 0.013498554 -0.092856809 -0.16996613 
		0.010425348 -0.051248651 -0.17459087 0.0076484666 -0.0059697819 -0.17285877 0.0019635134 
		0.011050065 -0.1659587 -0.0031680476 0.020593561 -0.15351562 -0.0029342663 0.025243184 
		-0.14191845 0.0010900949 0.039481141 -0.12882963 0.0061235991 0.080798469 -0.10084024 
		0.011501716 0.12655601 -0.058979027 0.015821839 -0.34956989 -0.1962955 0.045272 -0.24623837 
		-0.20654713 0.036125828 -0.17714797 -0.17989662 0.024248134 -0.13149078 -0.16717768 
		0.016577382 -0.093817867 -0.16828105 0.012193873 -0.054013494 -0.17924553 0.010605915 
		-0.012603367 -0.19311617 0.0082924794 0.01124624 -0.17800362 0.0030991992 0.022523347 
		-0.15846443 0.0044098585 0.032216743 -0.13867971 0.0092275599 0.051113047 -0.11676259 
		0.014473701 0.080832914 -0.093313672 0.018248271 0.12182172 -0.045698501 0.023120411 
		-0.31869027 -0.17586282 0.067094862 -0.23144193 -0.17375453 0.044893194 -0.17294118 
		-0.16018496 0.02715498 -0.12928775 -0.15593275 0.016815295 -0.089120127 -0.16072068 
		0.010745236 -0.050483473 -0.1723911 0.010918208 -0.017541585 -0.18449001 0.016598029 
		0.0069068917 -0.17405911 0.011963767 0.021966711 -0.15416434 0.014834327 0.035558946 
		-0.13009021 0.020756736 0.053916283 -0.10223353 0.025768926 0.077684268 -0.070996679 
		0.029008821 0.10416578 -0.026159555 0.032408308 -0.28944948 -0.13310857 0.079486214 
		-0.22401527 -0.14514281 0.052452814 -0.17132188 -0.14499678 0.030571623 -0.12181594 
		-0.14436238 0.01558507 -0.076572701 -0.150813 0.00781985 -0.040974919 -0.15793587 
		0.0037712322 -0.016285861 -0.15703814 0.010707378 0.0026609118 -0.14608644 0.019008776 
		0.018540965 -0.13338441 0.028436454 0.034546733 -0.11326074 0.036971126 0.052365448 
		-0.083564177 0.040519085 0.070456654 -0.051194742 0.040709376 0.084340021 -0.0083883982 
		0.04214219 -0.25733256 -0.088118173 0.080768801 -0.20748511 -0.10430079 0.057465784 
		-0.16172966 -0.11131224 0.04021797 -0.11021905 -0.12224106 0.022349676 -0.061790671 
		-0.12325529 0.01586825 -0.030797454 -0.12497308 0.012108759 -0.012969078 -0.11510736 
		0.019517874 0.0007283747 -0.10003988 0.032325696 0.014532449 -0.087683596 0.04714096 
		0.030429425 -0.076981559 0.060302034 0.049990654 -0.057741698 0.059408929 0.063512906 
		-0.028074492 0.049701329 0.064625613 0.0073557571 0.044649743 0.1746382 -0.025344007 
		-0.054498803 0.23186466 -0.053071164 -0.049554743 0.27566239 -0.089483365 -0.036245253 
		0.26619065 -0.089942954 -0.023729401 0.25598818 -0.084969722 -0.0069934386 0.24875133 
		-0.082987316 0.0079691699 0.25501266 -0.087725885 0.020477815 0.23077337 -0.06723056 
		0.027232345 0.20274805 -0.048698593 0.036323559 0.17098033 -0.033106219 0.047813039 
		0.13193348 -0.019510839 0.055147383 0.23464102 -0.028819663 -0.084490791 0.30901554 
		-0.044389967 -0.071264774 0.35273752 -0.06066148 -0.053830195 0.36297083 -0.074171834 
		-0.03435272 0.3591736 -0.081890076 -0.0078817252 0.34673545 -0.082873307 0.011772303 
		0.33679253 -0.081804171 0.02329961 0.31306353 -0.071677878 0.02931702 0.28174308 
		-0.05786382 0.039115805 0.24148618 -0.043847304 0.054078337 0.18846405 -0.028773412 
		0.069725968 0.29041988 -0.020386497 -0.11451131 0.36872637 -0.022794422 -0.091747664 
		0.41757798 -0.02815295 -0.072200969 0.42447084 -0.044353157 -0.042287707 0.4235777 
		-0.05853653 -0.0099730305 0.4126007 -0.066698208 0.01544976 0.39718166 -0.068990424 
		0.027735777 0.38550296 -0.065792166 0.02861659 0.35908788 -0.059686713 0.037688248 
		0.29919663 -0.043876886 0.056821309 0.22375225 -0.030967915 0.077605106 0.33292925 
		-0.0081375036 -0.13433558 0.42031339 0.00082570082 -0.10669791 0.46786049 0.00077310565 
		-0.086064145 0.4828907 -0.012502119 -0.052867465 0.48179442 -0.027261224 -0.016296823 
		0.47252798 -0.039263636 0.014076084 0.45924053 -0.042247258 0.02955436;
	setAttr ".pt[664:829]" 0.45033675 -0.037223574 0.030795958 0.43758765 -0.031780098 
		0.03428768 0.36562458 -0.032603774 0.054780658 0.26851621 -0.029742572 0.086623728 
		0.36301988 0.0047958693 -0.14698252 0.45224848 0.021946404 -0.11577357 0.49965888 
		0.024631711 -0.094583683 0.53368312 0.021463886 -0.063301489 0.54008096 0.0094016017 
		-0.022547593 0.53079653 -0.0016602962 0.0082739918 0.51948708 -0.0057959096 0.025760652 
		0.50924593 -0.0059349537 0.03257535 0.49267778 -0.0073131388 0.039144628 0.43363279 
		-0.012669411 0.054721843 0.33134118 -0.021175204 0.094041876 0.37253848 0.024664832 
		-0.15110804 0.45955724 0.036499381 -0.11843273 0.51756161 0.04328588 -0.096486881 
		0.55406135 0.044681404 -0.06241646 0.57442814 0.042305969 -0.025813138 0.57109231 
		0.03463839 0.0032179523 0.56137222 0.027382722 0.021416444 0.54669446 0.019775158 
		0.032205161 0.53206456 0.011009925 0.041723751 0.48065478 0.0040530609 0.060559534 
		0.37891546 -0.0032832865 0.099596262 0.37215579 0.044730015 -0.13907114 0.44743031 
		0.044797577 -0.10907632 0.51920116 0.053381976 -0.088425979 0.56022829 0.060937755 
		-0.055081055 0.58094358 0.065044433 -0.022184163 0.58906138 0.064592585 0.0024565514 
		0.58588648 0.056715377 0.018553164 0.56765908 0.043693054 0.029850386 0.54653537 
		0.031679921 0.04409216 0.49990648 0.026773209 0.072325192 0.39660782 0.021734158 
		0.11537066 0.3336269 0.055723634 -0.1099892 0.41597751 0.046899728 -0.088063791 0.50139499 
		0.054012999 -0.070263028 0.54736114 0.069534913 -0.039631668 0.57009631 0.082057796 
		-0.010325996 0.574049 0.086989306 0.004021096 0.56977016 0.07927867 0.0071387105 
		0.56175125 0.067189135 0.01827989 0.53398442 0.061393883 0.046862952 0.4625718 0.053149346 
		0.081154585 0.34704965 0.040927868 0.10980242 0.26396227 0.040983189 -0.064362459 
		0.35832646 0.039386339 -0.057236858 0.45099974 0.052151941 -0.046392512 0.5234158 
		0.074318804 -0.018344982 0.54038942 0.092572272 0.015482944 0.52019441 0.096969418 
		0.0071963868 0.52726954 0.095214866 -0.015725257 0.51697165 0.088894062 0.0029054082 
		0.46942756 0.078036778 0.035975087 0.40913624 0.069859199 0.069384448 0.30440477 
		0.052071471 0.089281827 0.20497757 0.022064326 -0.030300101 0.27652854 0.019845277 
		-0.025016502 0.36703196 0.034981642 -0.022832364 0.4788678 0.072557472 0.006391977 
		0.4682079 0.073747829 0.040074795 0.45655739 0.078892484 0.008666534 0.45314267 0.086078942 
		-0.01430008 0.42881754 0.084807783 0.00051253429 0.38747191 0.076568298 0.024675133 
		0.32136691 0.05956094 0.046040028 0.23597124 0.041879527 0.055917356 0.15575407 0.0076108216 
		-0.01191237 0.20937523 0.0081815505 -0.0070426455 0.2616905 0.014919802 -0.0030368122 
		0.3503457 0.023395468 0.0090882406 0.37168613 0.051912092 0.03188571 0.35051063 0.063356057 
		0.0082266871 0.3374669 0.069283009 -0.0067127957 0.3148306 0.065873414 0.0012898311 
		0.27538025 0.050252572 0.016936958 0.22347777 0.030783758 0.027865101 0.16076581 
		0.01281972 0.027488396 -0.26412773 -0.0099651199 -0.00073488359 -0.34308663 -0.034313325 
		-0.026903637 -0.44332129 -0.052133601 -0.038999721 -0.48394647 -0.08595141 -0.021428809 
		-0.48722395 -0.12014623 -0.0082120243 -0.49399918 -0.14805587 0.003289931 -0.49665534 
		-0.14828262 0.017639877 -0.49301031 -0.14087586 0.036410268 -0.491099 -0.14128342 
		0.072059616 -0.45819762 -0.14692204 0.12066908 -0.37429041 -0.094640218 0.12228683 
		-0.28908196 -0.0059411265 -0.0039509251 -0.35874599 -0.011622877 -0.016002178 -0.44532311 
		-0.025520917 -0.036403999 -0.49980998 -0.053437617 -0.035482924 -0.52114755 -0.078563623 
		-0.02207621 -0.53462851 -0.091464035 -0.0047617122 -0.55042458 -0.088172108 0.011657488 
		-0.56032228 -0.079885274 0.027425576 -0.57460785 -0.0836282 0.064820811 -0.55440176 
		-0.11080914 0.1259826 -0.4292016 -0.092717953 0.16289201 -0.30155867 -0.0048021451 
		-0.0089849103 -0.35825884 -0.023936743 -0.015671769 -0.43549392 -0.047774952 -0.042854626 
		-0.4955031 -0.064403459 -0.053760272 -0.52996761 -0.063906834 -0.039052866 -0.54838985 
		-0.054866098 -0.014449955 -0.56035709 -0.040972896 0.0081684571 -0.57669896 -0.025522957 
		0.027587838 -0.60361439 -0.013995026 0.051845841 -0.61102808 -0.023970762 0.11083014 
		-0.47672558 -0.064869985 0.14239575 -0.34553689 -0.0054545784 -0.030006828 -0.37309694 
		-0.034750458 -0.029355614 -0.44469401 -0.066101998 -0.048975576 -0.49652606 -0.071222514 
		-0.063936971 -0.52466714 -0.047388121 -0.051089581 -0.54020834 -0.020502254 -0.021248203 
		-0.55343872 0.0038005523 0.0072958302 -0.56616968 0.026550092 0.025983911 -0.58303702 
		0.042172443 0.041985959 -0.59741503 0.036240708 0.089693286 -0.54978412 -0.036553528 
		0.112434 -0.33110392 -0.0088328067 -0.046978142 -0.39830703 -0.032166462 -0.045760162 
		-0.4476243 -0.032615643 -0.040150523 -0.47639999 -0.015756384 -0.050035834 -0.48876223 
		0.0088531077 -0.043040037 -0.49917495 0.028654873 -0.018605165 -0.50929636 0.045717798 
		0.0079693422 -0.52102059 0.064963877 0.026733443 -0.5216735 0.071842432 0.03375328 
		-0.52436179 0.061830997 0.047780961 -0.54598838 0.036964431 0.059129704 -0.31026092 
		-0.00089423545 -0.042969201 -0.39709979 0.00040634209 -0.040121652 -0.41641518 0.011124879 
		-0.0274678 -0.42926002 0.03505728 -0.034497213 -0.4348276 0.057836287 -0.03276303 
		-0.4424485 0.07059288 -0.011993073 -0.44807315 0.07875099 0.012694453 -0.44972205 
		0.087669559 0.030356437 -0.43778136 0.087943532 0.034980576 -0.41515902 0.075740948 
		0.039958663 -0.37051558 0.052125368 0.065273724 -0.24001117 0.00075049186 -0.025395876 
		-0.30372581 0.014336919 -0.025625518 -0.32837236 0.033296533 -0.018062253 -0.34391323 
		0.05913686 -0.022965755 -0.35558525 0.080263741 -0.020466944 -0.36799377 0.092081815 
		-0.0021590409 -0.37604275 0.099804997 0.021062011 -0.37007111 0.10270038 0.036110152 
		-0.34930205 0.099345326 0.038042698 -0.31178892 0.083876058 0.034494735 -0.24926165 
		0.063034639 0.047360055 -0.15070899 -0.0081860386 -0.0048929565 -0.19499157 0.0067263162 
		-0.011347431 -0.22529234 0.03104008 -0.011333265 -0.24513204 0.058664039 -0.016737934 
		-0.26595488 0.07879518 -0.011691538 -0.28445002 0.095678866 0.0048430366 -0.29591098 
		0.10843206 0.02726097 -0.28768837 0.11023059 0.042060547;
	setAttr ".pt[830:865]" -0.26277813 0.099633977 0.03967401 -0.22523835 0.080679215 
		0.031499423 -0.16803706 0.060695183 0.034086831 -0.060575139 -0.025447857 0.018526951 
		-0.10199661 -0.010599258 0.0039042602 -0.13232321 0.011087513 -0.0038658448 -0.15339363 
		0.038043275 -0.011540403 -0.177185 0.063342869 -0.0086009288 -0.19990802 0.088935226 
		0.0054748538 -0.21464181 0.10767508 0.027372088 -0.20528021 0.10295684 0.036102828 
		-0.18910265 0.088282034 0.032414362 -0.16136946 0.068336837 0.02405962 -0.11389118 
		0.049543764 0.021337271 0.012493612 -0.049183063 0.037509408 -0.023385854 -0.035161085 
		0.017779535 -0.051791906 -0.015752165 0.0049134772 -0.075526878 0.0075265383 -0.0039705504 
		-0.097861141 0.031769652 -0.0055955555 -0.11852695 0.054395653 0.0011862642 -0.13261671 
		0.072233215 0.011496607 -0.13559422 0.078107461 0.019082509 -0.13329156 0.066278405 
		0.017821146 -0.11600214 0.048321605 0.012701939 -0.074455284 0.031271104 0.0077017634 
		0.072248958 -0.08306127 0.053992853 0.0441701 -0.068938427 0.029652115 0.019976595 
		-0.049749356 0.012655717 -0.0025401427 -0.02807807 0.0012026029 -0.024603507 -0.0053631691 
		-0.0038607805 -0.045532126 0.016461896 -0.0027596192 -0.062802255 0.033423476 0.0018992946 
		-0.075853147 0.042836331 0.0057314094 -0.083415344 0.04133825 0.0061042407 -0.067736752 
		0.023485333 0.0046916446 -0.032982804 0.004013407 -0.0054300204;
createNode transform -n "spotLight1";
	setAttr ".t" -type "double3" 0 41.607711862162589 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
	setAttr ".s" -type "double3" 6.8062813654656127 6.8062813654656127 6.8062813654656127 ;
createNode spotLight -n "spotLightShape1" -p "spotLight1";
	addAttr -ci true -h true -sn "OgreMaxObjectSettings" -ln "OgreMaxObjectSettings" 
		-bt "UNKN" -at "compound" -nc 97;
	addAttr -ci true -h true -sn "ID" -ln "ID" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserDataReference" -ln "UserDataReference" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserData" -ln "UserData" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UserDataClass" -ln "UserDataClass" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "ExtendedValues" -ln "ExtendedValues" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 5;
	addAttr -ci true -h true -sn "ExtendedValueID" -ln "ExtendedValueID" -bt "UNKN" 
		-dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -sn "ExtendedValueAttributesType" -ln "ExtendedValueAttributesType" 
		-bt "UNKN" -dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesFloats" -ln "ExtendedValueAttributesFloats" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesStrings" -ln "ExtendedValueAttributesStrings" 
		-bt "UNKN" -dt "string" -p "ExtendedValues";
	addAttr -ci true -h true -m -sn "ExtendedValueAttributesObjects" -ln "ExtendedValueAttributesObjects" 
		-bt "UNKN" -at "message" -p "ExtendedValues";
	addAttr -ci true -h true -sn "GenerateLOD" -ln "GenerateLOD" -bt "UNKN" -dv 2 -min 
		0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NumLODLevels" -ln "NumLODLevels" -bt "UNKN" -min 0 
		-max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODDistance" -ln "LODDistance" -bt "UNKN" -min 0 -max 
		100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODReductionType" -ln "LODReductionType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LODReductionAmount" -ln "LODReductionAmount" -bt "UNKN" 
		-min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UpdateCounter" -ln "UpdateCounter" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Type" -ln "Type" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseInOgreMaxViewports" -ln "UseInOgreMaxViewports" 
		-bt "UNKN" -dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Visibility" -ln "Visibility" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VisibilityAffectObjectOnly" -ln "VisibilityAffectObjectOnly" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "TargetType" -ln "TargetType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsModel" -ln "IsModel" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ModelName" -ln "ModelName" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeAnimationSampleInterval" -ln "NodeAnimationSampleInterval" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeAnimationSampleType" -ln "NodeAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "NoteTrackNames" -ln "NoteTrackNames" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderQueueName" -ln "RenderQueueName" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderQueueAdd" -ln "RenderQueueAdd" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "CustomParameterIDs" -ln "CustomParameterIDs" -bt "UNKN" 
		-dv 2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "CustomParameterValues" -ln "CustomParameterValues" 
		-bt "UNKN" -at "double4" -p "OgreMaxObjectSettings" -nc 4;
	addAttr -ci true -h true -sn "CustomParameterValues0" -ln "CustomParameterValues0" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -sn "CustomParameterValues1" -ln "CustomParameterValues1" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -sn "CustomParameterValues2" -ln "CustomParameterValues2" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -sn "CustomParameterValuesx" -ln "CustomParameterValuesX" -bt "UNKN" 
		-dv 3.4028234663852886e+038 -at "double" -p "CustomParameterValues";
	addAttr -ci true -h true -m -sn "QueryFlagNames" -ln "QueryFlagNames" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "QueryFlagBits" -ln "QueryFlagBits" -bt "UNKN" -dv 
		2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "VisibilityFlagNames" -ln "VisibilityFlagNames" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "VisibilityFlagBits" -ln "VisibilityFlagBits" -bt "UNKN" 
		-dv 2147483647 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsStatic" -ln "IsStatic" -bt "UNKN" -min 0 -max 1 
		-at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexBufferUsage" -ln "VertexBufferUsage" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexBufferShadowed" -ln "VertexBufferShadowed" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IndexBufferUsage" -ln "IndexBufferUsage" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IndexBufferShadowed" -ln "IndexBufferShadowed" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "TextureCoordinateSetCount" -ln "TextureCoordinateSetCount" 
		-bt "UNKN" -dv 1 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "MeshName" -ln "MeshName" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "FastExport" -ln "FastExport" -bt "UNKN" -dv 2 -min 
		0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AlwaysExportSkeleton" -ln "AlwaysExportSkeleton" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AlwaysExportPoses" -ln "AlwaysExportPoses" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RemoveBonesWithNoVertexInfluence" -ln "RemoveBonesWithNoVertexInfluence" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonName" -ln "SkeletonName" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "MeshAnimationType" -ln "MeshAnimationType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonAnimationSampleInterval" -ln "SkeletonAnimationSampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SkeletonAnimationSampleType" -ln "SkeletonAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexAnimationSampleInterval" -ln "VertexAnimationSampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "VertexAnimationSampleType" -ln "VertexAnimationSampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AnimatedRoot" -ln "AnimatedRoot" -bt "UNKN" -at "message" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExportMeshAnimationsToSeparateFile" -ln "ExportMeshAnimationsToSeparateFile" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LinkMeshAnimationsToMainObject" -ln "LinkMeshAnimationsToMainObject" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "PrimitiveType" -ln "PrimitiveType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExportVertexColors" -ln "ExportVertexColors" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateTangents" -ln "GenerateTangents" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateBinormals" -ln "GenerateBinormals" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SplitMirroredTangents" -ln "SplitMirroredTangents" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SplitRotatedTangents" -ln "SplitRotatedTangents" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "StoreTangentParityInW" -ln "StoreTangentParityInW" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateEdgeLists" -ln "GenerateEdgeLists" -bt "UNKN" 
		-dv 2 -min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseCustomTextureCoordinateSets" -ln "UseCustomTextureCoordinateSets" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "SubmeshesTextureCoordinateSets" -ln "SubmeshesTextureCoordinateSets" 
		-bt "UNKN" -at "compound" -p "OgreMaxObjectSettings" -nc 6;
	addAttr -ci true -h true -sn "TextureCoordinateSetSubmeshName" -ln "TextureCoordinateSetSubmeshName" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "TextureCoordinateSetMaterial" -ln "TextureCoordinateSetMaterial" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "TextureCoordinateSetMaterialObject" -ln "TextureCoordinateSetMaterialObject" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetSources" -ln "TextureCoordinateSetSources" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetDestinations" -ln "TextureCoordinateSetDestinations" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -m -sn "TextureCoordinateSetTypes" -ln "TextureCoordinateSetTypes" 
		-bt "UNKN" -dt "string" -p "SubmeshesTextureCoordinateSets";
	addAttr -ci true -h true -sn "LODType" -ln "LODType" -bt "UNKN" -min 0 -max 100 
		-at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "LODObjects" -ln "LODObjects" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "LODDistances" -ln "LODDistances" -bt "UNKN" -dv 
		3.4028234663852886e+038 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "FileName" -ln "FileName" -bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ExternalItemType" -ln "ExternalItemType" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "EnableSky" -ln "EnableSky" -bt "UNKN" -dv 1 -min 0 
		-max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "DrawSkyFirst" -ln "DrawSkyFirst" -bt "UNKN" -dv 1 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Scale" -ln "Scale" -bt "UNKN" -dv 1 -min 0 -max 100 
		-at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Curvature" -ln "Curvature" -bt "UNKN" -min 0 -max 
		100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "Tiling" -ln "Tiling" -bt "UNKN" -at "float2" -p "OgreMaxObjectSettings" 
		-nc 2;
	addAttr -ci true -h true -sn "Tiling0" -ln "Tiling0" -bt "UNKN" -at "float" -p "Tiling";
	addAttr -ci true -h true -sn "Tiling1" -ln "Tiling1" -bt "UNKN" -at "float" -p "Tiling";
	addAttr -ci true -h true -sn "Bow" -ln "Bow" -bt "UNKN" -min 0 -max 100 -at "float" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardType" -ln "BillboardType" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardOrigin" -ln "BillboardOrigin" -bt "UNKN" 
		-dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardCommonDirection" -ln "BillboardCommonDirection" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardCommonDirection0" -ln "BillboardCommonDirection0" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonDirection1" -ln "BillboardCommonDirection1" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonDirection2" -ln "BillboardCommonDirection2" 
		-bt "UNKN" -at "float" -p "BillboardCommonDirection";
	addAttr -ci true -h true -sn "BillboardCommonUpVector" -ln "BillboardCommonUpVector" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardCommonUpVector0" -ln "BillboardCommonUpVector0" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardCommonUpVector1" -ln "BillboardCommonUpVector1" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardCommonUpVector2" -ln "BillboardCommonUpVector2" 
		-bt "UNKN" -at "float" -p "BillboardCommonUpVector";
	addAttr -ci true -h true -sn "BillboardRotationType" -ln "BillboardRotationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "BillboardPoolSize" -ln "BillboardPoolSize" -bt "UNKN" 
		-min 0 -max 100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "AutoExtendBillboardPool" -ln "AutoExtendBillboardPool" 
		-bt "UNKN" -dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "CullBillboardsIndividually" -ln "CullBillboardsIndividually" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "SortBillboards" -ln "SortBillboards" -bt "UNKN" -min 
		0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "UseBillboardAccurateFacingModel" -ln "UseBillboardAccurateFacingModel" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -uac -h true -sn "BillboardColor" -ln "BillboardColor" -bt "UNKN" 
		-at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "BillboardColorr" -ln "BillboardColorR" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardColorg" -ln "BillboardColorG" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardColorb" -ln "BillboardColorB" -bt "UNKN" 
		-at "float" -p "BillboardColor";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle" -ln "BillboardTextureCoordinateRectangle" 
		-bt "UNKN" -at "double4" -p "OgreMaxObjectSettings" -nc 4;
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle0" -ln "BillboardTextureCoordinateRectangle0" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle1" -ln "BillboardTextureCoordinateRectangle1" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -h true -sn "BillboardTextureCoordinateRectangle2" -ln "BillboardTextureCoordinateRectangle2" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -sn "BillboardTextureCoordinateRectanglex" -ln "BillboardTextureCoordinateRectangleX" 
		-bt "UNKN" -at "double" -p "BillboardTextureCoordinateRectangle";
	addAttr -ci true -uac -h true -sn "LightSpecularColor" -ln "LightSpecularColor" 
		-bt "UNKN" -at "float3" -p "OgreMaxObjectSettings" -nc 3;
	addAttr -ci true -h true -sn "LightSpecularColorr" -ln "LightSpecularColorR" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightSpecularColorg" -ln "LightSpecularColorG" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightSpecularColorb" -ln "LightSpecularColorB" -bt "UNKN" 
		-at "float" -p "LightSpecularColor";
	addAttr -ci true -h true -sn "LightAttenuationConstant" -ln "LightAttenuationConstant" 
		-bt "UNKN" -dv 1 -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LightAttenuationLinear" -ln "LightAttenuationLinear" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "LightAttenuationQuadric" -ln "LightAttenuationQuadric" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "GenerateNormals" -ln "GenerateNormals" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "CreateMovablePlane" -ln "CreateMovablePlane" -bt "UNKN" 
		-dv 1 -min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IsAttachedObject" -ln "IsAttachedObject" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "IgnoreChildren" -ln "IgnoreChildren" -bt "UNKN" -min 
		0 -max 1 -at "bool" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ChildSorting" -ln "ChildSorting" -bt "UNKN" -dt "string" 
		-p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "ChildOrder" -ln "ChildOrder" -bt "UNKN" -min 0 -max 
		100 -at "long" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "RenderingDistance" -ln "RenderingDistance" -bt "UNKN" 
		-min 0 -max 100 -at "float" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeTranslationInterpolationType" -ln "NodeTranslationInterpolationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -sn "NodeRotationInterpolationType" -ln "NodeRotationInterpolationType" 
		-bt "UNKN" -dt "string" -p "OgreMaxObjectSettings";
	addAttr -ci true -h true -m -sn "NodeAnimations" -ln "NodeAnimations" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 14;
	addAttr -ci true -h true -sn "NodeAnimation_UpdateCounter" -ln "NodeAnimation_UpdateCounter" 
		-bt "UNKN" -min 0 -max 100 -at "long" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_AnimationName" -ln "NodeAnimation_AnimationName" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeStart" -ln "NodeAnimation_TimeStart" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeEnd" -ln "NodeAnimation_TimeEnd" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeScaleType" -ln "NodeAnimation_TimeScaleType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TimeScale" -ln "NodeAnimation_TimeScale" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_CopyFirstKeyToLast" -ln "NodeAnimation_CopyFirstKeyToLast" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_UseAnimationStartTime" -ln "NodeAnimation_UseAnimationStartTime" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_Enabled" -ln "NodeAnimation_Enabled" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_Looped" -ln "NodeAnimation_Looped" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_SampleInterval" -ln "NodeAnimation_SampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_SampleType" -ln "NodeAnimation_SampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_TranslationInterpolationType" -ln "NodeAnimation_TranslationInterpolationType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -sn "NodeAnimation_RotationInterpolationType" -ln "NodeAnimation_RotationInterpolationType" 
		-bt "UNKN" -dt "string" -p "NodeAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimations" -ln "MeshAnimations" -bt "UNKN" 
		-at "compound" -p "OgreMaxObjectSettings" -nc 23;
	addAttr -ci true -h true -sn "MeshAnimation_UpdateCounter" -ln "MeshAnimation_UpdateCounter" 
		-bt "UNKN" -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TrackName" -ln "MeshAnimation_TrackName" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_AnimationName" -ln "MeshAnimation_AnimationName" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeStart" -ln "MeshAnimation_TimeStart" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeEnd" -ln "MeshAnimation_TimeEnd" 
		-bt "UNKN" -min 0 -max 100 -at "double" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeScaleType" -ln "MeshAnimation_TimeScaleType" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_TimeScale" -ln "MeshAnimation_TimeScale" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_CopyFirstKeyToLast" -ln "MeshAnimation_CopyFirstKeyToLast" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportAsMorph" -ln "MeshAnimation_ExportAsMorph" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_MorphWholeObject" -ln "MeshAnimation_MorphWholeObject" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_UseAnimationStartTime" -ln "MeshAnimation_UseAnimationStartTime" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_Looped" -ln "MeshAnimation_Looped" -bt "UNKN" 
		-min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_SampleInterval" -ln "MeshAnimation_SampleInterval" 
		-bt "UNKN" -min 0 -max 100 -at "float" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_SampleType" -ln "MeshAnimation_SampleType" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportToSeparateFile" -ln "MeshAnimation_ExportToSeparateFile" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_LinkToMainObject" -ln "MeshAnimation_LinkToMainObject" 
		-bt "UNKN" -min 0 -max 1 -at "bool" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_ExportToSeparateFileInherited" -ln "MeshAnimation_ExportToSeparateFileInherited" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -sn "MeshAnimation_LinkToMainObjectInherited" -ln "MeshAnimation_LinkToMainObjectInherited" 
		-bt "UNKN" -dv 2 -min 0 -max 100 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_BoneTranslationMaskObjects" -ln "MeshAnimation_BoneTranslationMaskObjects" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_BoneTranslationMasks" -ln "MeshAnimation_BoneTranslationMasks" 
		-bt "UNKN" -at "float3" -p "MeshAnimations" -nc 3;
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks0" -ln "MeshAnimation_BoneTranslationMasks0" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks1" -ln "MeshAnimation_BoneTranslationMasks1" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -sn "MeshAnimation_BoneTranslationMasks2" -ln "MeshAnimation_BoneTranslationMasks2" 
		-bt "UNKN" -dv 3.4028234663852886e+038 -at "float" -p "MeshAnimation_BoneTranslationMasks";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBonesObjects" -ln "MeshAnimation_AllowExportBonesObjects" 
		-bt "UNKN" -dt "string" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBoneAllows" -ln "MeshAnimation_AllowExportBoneAllows" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "MeshAnimations";
	addAttr -ci true -h true -m -sn "MeshAnimation_AllowExportBoneRecursives" -ln "MeshAnimation_AllowExportBoneRecursives" 
		-bt "UNKN" -dv 2147483647 -at "long" -p "MeshAnimations";
	setAttr -k off ".v";
	setAttr ".ca" 110.08172957795132;
	setAttr ".phi" 8000;
	setAttr ".algt" yes;
createNode rectangularLightLocator -n "_spotLight1_mrLoc" -p "spotLight1";
	setAttr -k off ".v";
createNode lightLinker -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	setAttr ".sw" 12;
	setAttr ".sh" 12;
	setAttr ".sd" 12;
	setAttr ".cuv" 4;
createNode polyMapCut -n "polyMapCut1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[3]";
createNode polyMapCut -n "polyMapCut2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[2]";
createNode polyMapCut -n "polyMapCut3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1]";
createNode polyMapCut -n "polyMapCut4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[4:5]";
createNode polyTweakUV -n "polyTweakUV1";
	setAttr ".uopa" yes;
	setAttr -s 949 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.03142976 0.0019920468 -0.027159736 
		0.0019920468 -0.022889592 0.0019920468 -0.018619567 0.0019920468 -0.014349446 0.0019920468 
		-0.010079361 0.0019920468 -0.0058092475 0.0019920468 -0.0015392229 0.0019920468 0.0027307793 
		0.0019920468 0.0070009455 0.0019920468 0.011271067 0.0019920468 0.015541054 0.0019920468 
		0.019811084 0.0019920468 -0.03142976 0.0019090276 -0.027159736 0.0019090276 -0.022889592 
		0.0019090276 -0.018619567 0.0019090276 -0.014349446 0.0019090276 -0.010079361 0.0019090276 
		-0.0058092475 0.0019090276 -0.0015392229 0.0019090276 0.0027307793 0.0019090276 0.0070009455 
		0.0019090276 0.011271067 0.0019090276 0.015541054 0.0019090276 0.019811084 0.0019090276 
		-0.03142976 0.0018260665 -0.027159736 0.0018260665 -0.022889592 0.0018260665 -0.018619567 
		0.0018260665 -0.014349446 0.0018260665 -0.010079361 0.0018260665 -0.0058092475 0.0018260665 
		-0.0015392229 0.0018260665 0.0027307793 0.0018260665 0.0070009455 0.0018260665 0.011271067 
		0.0018260665 0.015541054 0.0018260665 0.019811084 0.0018260665 -0.03142976 0.0017430497 
		-0.027159736 0.0017430497 -0.022889592 0.0017430497 -0.018619567 0.0017430497 -0.014349446 
		0.0017430497 -0.010079361 0.0017430497 -0.0058092475 0.0017430497 -0.0015392229 0.0017430497 
		0.0027307793 0.0017430497 0.0070009455 0.0017430497 0.011271067 0.0017430497 0.015541054 
		0.0017430497 0.019811084 0.0017430497 -0.03142976 0.001660059 -0.027159736 0.001660059 
		-0.022889592 0.001660059 -0.018619567 0.001660059 -0.014349446 0.001660059 -0.010079361 
		0.001660059 -0.0058092475 0.001660059 -0.0015392229 0.001660059 0.0027307793 0.001660059 
		0.0070009455 0.001660059 0.011271067 0.001660059 0.015541054 0.001660059 0.019811084 
		0.001660059 -0.03142976 0.0015770905 -0.027159736 0.0015770905 -0.022889592 0.0015770905 
		-0.018619567 0.0015770905 -0.014349446 0.0015770905 -0.010079361 0.0015770905 -0.0058092475 
		0.0015770905 -0.0015392229 0.0015770905 0.0027307793 0.0015770905 0.0070009455 0.0015770905 
		0.011271067 0.0015770905 0.015541054 0.0015770905 0.019811084 0.0015770905 -0.03142976 
		0.0014941072 -0.027159736 0.0014941072 -0.022889592 0.0014941072 -0.018619567 0.0014941072 
		-0.014349446 0.0014941072 -0.010079361 0.0014941072 -0.0058092475 0.0014941072 -0.0015392229 
		0.0014941072 0.0027307793 0.0014941072 0.0070009455 0.0014941072 0.011271067 0.0014941072 
		0.015541054 0.0014941072 0.019811084 0.0014941072 -0.03142976 0.0014111015 -0.027159736 
		0.0014111015 -0.022889592 0.0014111015 -0.018619567 0.0014111015 -0.014349446 0.0014111015 
		-0.010079361 0.0014111015 -0.0058092475 0.0014111015 -0.0015392229 0.0014111015 0.0027307793 
		0.0014111015 0.0070009455 0.0014111015 0.011271067 0.0014111015 0.015541054 0.0014111015 
		0.019811084 0.0014111015 -0.03142976 0.0013281108 -0.027159736 0.0013281108 -0.022889592 
		0.0013281108 -0.018619567 0.0013281108 -0.014349446 0.0013281108 -0.010079361 0.0013281108 
		-0.0058092475 0.0013281108 -0.0015392229 0.0013281108 0.0027307793 0.0013281108 0.0070009455 
		0.0013281108 0.011271067 0.0013281108 0.015541054 0.0013281108 0.019811084 0.0013281108 
		-0.03142976 0.00124512 -0.027159736 0.00124512 -0.022889592 0.00124512 -0.018619567 
		0.00124512 -0.014349446 0.00124512 -0.010079361 0.00124512 -0.0058092475 0.00124512 
		-0.0015392229 0.00124512 0.0027307793 0.00124512 0.0070009455 0.00124512 0.011271067 
		0.00124512 0.015541054 0.00124512 0.019811084 0.00124512 -0.03142976 0.0011621143 
		-0.027159736 0.0011621143 -0.022889592 0.0011621143 -0.018619567 0.0011621143 -0.014349446 
		0.0011621143 -0.010079361 0.0011621143 -0.0058092475 0.0011621143 -0.0015392229 0.0011621143 
		0.0027307793 0.0011621143 0.0070009455 0.0011621143 0.011271067 0.0011621143 0.015541054 
		0.0011621143 0.019811084 0.0011621143 -0.03142976 0.0010791236 -0.027159736 0.0010791236 
		-0.022889592 0.0010791236 -0.018619567 0.0010791236 -0.014349446 0.0010791236 -0.010079361 
		0.0010791236 -0.0058092475 0.0010791236 -0.0015392229 0.0010791236 0.0027307793 0.0010791236 
		0.0070009455 0.0010791236 0.011271067 0.0010791236 0.015541054 0.0010791236 0.019811084 
		0.0010791236 -0.03142976 0.00099614775 -0.027159736 0.00099614775 -0.022889592 0.00099614775 
		-0.018619567 0.00099614775 -0.014349446 0.00099614775 -0.010079361 0.00099614775 
		-0.0058092475 0.00099614775 -0.0015392229 0.00099614775 0.0027307793 0.00099614775 
		0.0070009455 0.00099614775 0.011271067 0.00099614775 0.015541054 0.00099614775 0.019811084 
		0.00099614775 -0.03142976 0.00091315689 -0.027159736 0.00091315689 -0.022889592 0.00091315689 
		-0.018619567 0.00091315689 -0.014349446 0.00091315689 -0.010079361 0.00091315689 
		-0.0058092475 0.00091315689 -0.0015392229 0.00091315689 0.0027307793 0.00091315689 
		0.0070009455 0.00091315689 0.011271067 0.00091315689 0.015541054 0.00091315689 0.019811084 
		0.00091315689 -0.03142976 0.0008301661 -0.027159736 0.0008301661 -0.022889592 0.0008301661 
		-0.018619567 0.0008301661 -0.014349446 0.0008301661 -0.010079361 0.0008301661 -0.0058092475 
		0.0008301661 -0.0015392229 0.0008301661 0.0027307793 0.0008301661 0.0070009455 0.0008301661 
		0.011271067 0.0008301661 0.015541054 0.0008301661 0.019811084 0.0008301661 -0.03142976 
		0.00074717531 -0.027159736 0.00074717531 -0.022889592 0.00074717531 -0.018619567 
		0.00074717531 -0.014349446 0.00074717531 -0.010079361 0.00074717531 -0.0058092475 
		0.00074717531 -0.0015392229 0.00074717531 0.0027307793 0.00074717531 0.0070009455 
		0.00074717531 0.011271067 0.00074717531 0.015541054 0.00074717531 0.019811084 0.00074717531 
		-0.03142976 0.00066418445 -0.027159736 0.00066418445 -0.022889592 0.00066418445 -0.018619567 
		0.00066418445 -0.014349446 0.00066418445 -0.010079361 0.00066418445 -0.0058092475 
		0.00066418445 -0.0015392229 0.00066418445 0.0027307793 0.00066418445 0.0070009455 
		0.00066418445 0.011271067 0.00066418445 0.015541054 0.00066418445 0.019811084 0.00066418445 
		-0.03142976 0.00058119366 -0.027159736 0.00058119366 -0.022889592 0.00058119366 -0.018619567 
		0.00058119366 -0.014349446 0.00058119366 -0.010079361 0.00058119366 -0.0058092475 
		0.00058119366 -0.0015392229 0.00058119366 0.0027307793 0.00058119366 0.0070009455 
		0.00058119366 0.011271067 0.00058119366 0.015541054 0.00058119366 0.019811084 0.00058119366 
		-0.03142976 0.00049820286 -0.027159736 0.00049820286 -0.022889592 0.00049820286 -0.018619567 
		0.00049820286 -0.014349446 0.00049820286 -0.010079361 0.00049820286 -0.0058092475 
		0.00049820286 -0.0015392229 0.00049820286 0.0027307793 0.00049820286 0.0070009455 
		0.00049820286 0.011271067 0.00049820286 0.015541054 0.00049820286 0.019811084 0.00049820286 
		-0.03142976 0.00041521204 -0.027159736 0.00041521204 -0.022889592 0.00041521204;
	setAttr ".uvtk[250:499]" -0.018619567 0.00041521204 -0.014349446 0.00041521204 
		-0.010079361 0.00041521204 -0.0058092475 0.00041521204 -0.0015392229 0.00041521204 
		0.0027307793 0.00041521204 0.0070009455 0.00041521204 0.011271067 0.00041521204 0.015541054 
		0.00041521204 0.019811084 0.00041521204 -0.03142976 0.00033222121 -0.027159736 0.00033222121 
		-0.022889592 0.00033222121 -0.018619567 0.00033222121 -0.014349446 0.00033222121 
		-0.010079361 0.00033222121 -0.0058092475 0.00033222121 -0.0015392229 0.00033222121 
		0.0027307793 0.00033222121 0.0070009455 0.00033222121 0.011271067 0.00033222121 0.015541054 
		0.00033222121 0.019811084 0.00033222121 -0.03142976 0.00024923039 -0.027159736 0.00024923039 
		-0.022889592 0.00024923039 -0.018619567 0.00024923039 -0.014349446 0.00024923039 
		-0.010079361 0.00024923039 -0.0058092475 0.00024923039 -0.0015392229 0.00024923039 
		0.0027307793 0.00024923039 0.0070009455 0.00024923039 0.011271067 0.00024923039 0.015541054 
		0.00024923039 0.019811084 0.00024923039 -0.03142976 0.00016623958 -0.027159736 0.00016623958 
		-0.022889592 0.00016623958 -0.018619567 0.00016623958 -0.014349446 0.00016623958 
		-0.010079361 0.00016623958 -0.0058092475 0.00016623958 -0.0015392229 0.00016623958 
		0.0027307793 0.00016623958 0.0070009455 0.00016623958 0.011271067 0.00016623958 0.015541054 
		0.00016623958 0.019811084 0.00016623958 -0.03142976 8.3248771e-005 -0.027159736 8.3248771e-005 
		-0.022889592 8.3248771e-005 -0.018619567 8.3248771e-005 -0.014349446 8.3248771e-005 
		-0.010079361 8.3248771e-005 -0.0058092475 8.3248771e-005 -0.0015392229 8.3248771e-005 
		0.0027307793 8.3248771e-005 0.0070009455 8.3248771e-005 0.011271067 8.3248771e-005 
		0.015541054 8.3248771e-005 0.019811084 8.3248771e-005 -0.03142976 2.2838934e-007 
		-0.027159736 2.2838934e-007 -0.022889592 2.2838934e-007 -0.018619567 2.2838934e-007 
		-0.014349446 2.2838934e-007 -0.010079361 2.2838934e-007 -0.0058092475 2.2838934e-007 
		-0.0015392229 2.2838934e-007 0.0027307793 2.2838934e-007 0.0070009455 2.2838934e-007 
		0.011271067 2.2838934e-007 0.015541054 2.2838934e-007 0.019811084 2.2838934e-007 
		-0.03142976 -8.2762308e-005 -0.027159736 -8.2762308e-005 -0.022889592 -8.2762308e-005 
		-0.018619567 -8.2762308e-005 -0.014349446 -8.2762308e-005 -0.010079361 -8.2762308e-005 
		-0.0058092475 -8.2762308e-005 -0.0015392229 -8.2762308e-005 0.0027307793 -8.2762308e-005 
		0.0070009455 -8.2762308e-005 0.011271067 -8.2762308e-005 0.015541054 -8.2762308e-005 
		0.019811084 -8.2762308e-005 -0.03142976 -0.00016575301 -0.027159736 -0.00016575301 
		-0.022889592 -0.00016575301 -0.018619567 -0.00016575301 -0.014349446 -0.00016575301 
		-0.010079361 -0.00016575301 -0.0058092475 -0.00016575301 -0.0015392229 -0.00016575301 
		0.0027307793 -0.00016575301 0.0070009455 -0.00016575301 0.011271067 -0.00016575301 
		0.015541054 -0.00016575301 0.019811084 -0.00016575301 -0.03142976 -0.00024874369 
		-0.027159736 -0.00024874369 -0.022889592 -0.00024874369 -0.018619567 -0.00024874369 
		-0.014349446 -0.00024874369 -0.010079361 -0.00024874369 -0.0058092475 -0.00024874369 
		-0.0015392229 -0.00024874369 0.0027307793 -0.00024874369 0.0070009455 -0.00024874369 
		0.011271067 -0.00024874369 0.015541054 -0.00024874369 0.019811084 -0.00024874369 
		-0.03142976 -0.00033173439 -0.027159736 -0.00033173439 -0.022889592 -0.00033173439 
		-0.018619567 -0.00033173439 -0.014349446 -0.00033173439 -0.010079361 -0.00033173439 
		-0.0058092475 -0.00033173439 -0.0015392229 -0.00033173439 0.0027307793 -0.00033173439 
		0.0070009455 -0.00033173439 0.011271067 -0.00033173439 0.015541054 -0.00033173439 
		0.019811084 -0.00033173439 -0.03142976 -0.0004147251 -0.027159736 -0.0004147251 -0.022889592 
		-0.0004147251 -0.018619567 -0.0004147251 -0.014349446 -0.0004147251 -0.010079361 
		-0.0004147251 -0.0058092475 -0.0004147251 -0.0015392229 -0.0004147251 0.0027307793 
		-0.0004147251 0.0070009455 -0.0004147251 0.011271067 -0.0004147251 0.015541054 -0.0004147251 
		0.019811084 -0.0004147251 -0.03142976 -0.00049771578 -0.027159736 -0.00049771578 
		-0.022889592 -0.00049771578 -0.018619567 -0.00049771578 -0.014349446 -0.00049771578 
		-0.010079361 -0.00049771578 -0.0058092475 -0.00049771578 -0.0015392229 -0.00049771578 
		0.0027307793 -0.00049771578 0.0070009455 -0.00049771578 0.011271067 -0.00049771578 
		0.015541054 -0.00049771578 0.019811084 -0.00049771578 -0.03142976 -0.00058070646 
		-0.027159736 -0.00058070646 -0.022889592 -0.00058070646 -0.018619567 -0.00058070646 
		-0.014349446 -0.00058070646 -0.010079361 -0.00058070646 -0.0058092475 -0.00058070646 
		-0.0015392229 -0.00058070646 0.0027307793 -0.00058070646 0.0070009455 -0.00058070646 
		0.011271067 -0.00058070646 0.015541054 -0.00058070646 0.019811084 -0.00058070646 
		-0.03142976 -0.0006636972 -0.027159736 -0.0006636972 -0.022889592 -0.0006636972 -0.018619567 
		-0.0006636972 -0.014349446 -0.0006636972 -0.010079361 -0.0006636972 -0.0058092475 
		-0.0006636972 -0.0015392229 -0.0006636972 0.0027307793 -0.0006636972 0.0070009455 
		-0.0006636972 0.011271067 -0.0006636972 0.015541054 -0.0006636972 0.019811084 -0.0006636972 
		-0.03142976 -0.00074668787 -0.027159736 -0.00074668787 -0.022889592 -0.00074668787 
		-0.018619567 -0.00074668787 -0.014349446 -0.00074668787 -0.010079361 -0.00074668787 
		-0.0058092475 -0.00074668787 -0.0015392229 -0.00074668787 0.0027307793 -0.00074668787 
		0.0070009455 -0.00074668787 0.011271067 -0.00074668787 0.015541054 -0.00074668787 
		0.019811084 -0.00074668787 -0.03142976 -0.00082967855 -0.027159736 -0.00082967855 
		-0.022889592 -0.00082967855 -0.018619567 -0.00082967855 -0.014349446 -0.00082967855 
		-0.010079361 -0.00082967855 -0.0058092475 -0.00082967855 -0.0015392229 -0.00082967855 
		0.0027307793 -0.00082967855 0.0070009455 -0.00082967855 0.011271067 -0.00082967855 
		0.015541054 -0.00082967855 0.019811084 -0.00082967855 -0.03142976 -0.00091266929 
		-0.027159736 -0.00091266929 -0.022889592 -0.00091266929 -0.018619567 -0.00091266929 
		-0.014349446 -0.00091266929 -0.010079361 -0.00091266929 -0.0058092475 -0.00091266929 
		-0.0015392229 -0.00091266929 0.0027307793 -0.00091266929 0.0070009455 -0.00091266929 
		0.011271067 -0.00091266929 0.015541054 -0.00091266929 0.019811084 -0.00091266929 
		-0.03142976 -0.00099565997 -0.027159736 -0.00099565997 -0.022889592 -0.00099565997 
		-0.018619567 -0.00099565997 -0.014349446 -0.00099565997 -0.010079361 -0.00099565997 
		-0.0058092475 -0.00099565997 -0.0015392229 -0.00099565997 0.0027307793 -0.00099565997 
		0.0070009455 -0.00099565997 0.011271067 -0.00099565997 0.015541054 -0.00099565997 
		0.019811084 -0.00099565997 -0.03142976 -0.0010786507 -0.027159736 -0.0010786507 -0.022889592 
		-0.0010786507 -0.018619567 -0.0010786507 -0.014349446 -0.0010786507 -0.010079361 
		-0.0010786507 -0.0058092475 -0.0010786507 -0.0015392229 -0.0010786507 0.0027307793 
		-0.0010786507 0.0070009455 -0.0010786507 0.011271067 -0.0010786507 0.015541054 -0.0010786507 
		0.019811084 -0.0010786507 -0.03142976 -0.0011616413 -0.027159736 -0.0011616413 -0.022889592 
		-0.0011616413 -0.018619567 -0.0011616413 -0.014349446 -0.0011616413 -0.010079361 
		-0.0011616413;
	setAttr ".uvtk[500:749]" -0.0058092475 -0.0011616413 -0.0015392229 -0.0011616413 
		0.0027307793 -0.0011616413 0.0070009455 -0.0011616413 0.011271067 -0.0011616413 0.015541054 
		-0.0011616413 0.019811084 -0.0011616413 -0.03142976 -0.0012446321 -0.027159736 -0.0012446321 
		-0.022889592 -0.0012446321 -0.018619567 -0.0012446321 -0.014349446 -0.0012446321 
		-0.010079361 -0.0012446321 -0.0058092475 -0.0012446321 -0.0015392229 -0.0012446321 
		0.0027307793 -0.0012446321 0.0070009455 -0.0012446321 0.011271067 -0.0012446321 0.015541054 
		-0.0012446321 0.019811084 -0.0012446321 -0.03142976 -0.0013276228 -0.027159736 -0.0013276228 
		-0.022889592 -0.0013276228 -0.018619567 -0.0013276228 -0.014349446 -0.0013276228 
		-0.010079361 -0.0013276228 -0.0058092475 -0.0013276228 -0.0015392229 -0.0013276228 
		0.0027307793 -0.0013276228 0.0070009455 -0.0013276228 0.011271067 -0.0013276228 0.015541054 
		-0.0013276228 0.019811084 -0.0013276228 -0.03142976 -0.0014106134 -0.027159736 -0.0014106134 
		-0.022889592 -0.0014106134 -0.018619567 -0.0014106134 -0.014349446 -0.0014106134 
		-0.010079361 -0.0014106134 -0.0058092475 -0.0014106134 -0.0015392229 -0.0014106134 
		0.0027307793 -0.0014106134 0.0070009455 -0.0014106134 0.011271067 -0.0014106134 0.015541054 
		-0.0014106134 0.019811084 -0.0014106134 -0.03142976 -0.0014936042 -0.027159736 -0.0014936042 
		-0.022889592 -0.0014936042 -0.018619567 -0.0014936042 -0.014349446 -0.0014936042 
		-0.010079361 -0.0014936042 -0.0058092475 -0.0014936042 -0.0015392229 -0.0014936042 
		0.0027307793 -0.0014936042 0.0070009455 -0.0014936042 0.011271067 -0.0014936042 0.015541054 
		-0.0014936042 0.019811084 -0.0014936042 -0.03142976 -0.0015765949 -0.027159736 -0.0015765949 
		-0.022889592 -0.0015765949 -0.018619567 -0.0015765949 -0.014349446 -0.0015765949 
		-0.010079361 -0.0015765949 -0.0058092475 -0.0015765949 -0.0015392229 -0.0015765949 
		0.0027307793 -0.0015765949 0.0070009455 -0.0015765949 0.011271067 -0.0015765949 0.015541054 
		-0.0015765949 0.019811084 -0.0015765949 -0.03142976 -0.0016595855 -0.027159736 -0.0016595855 
		-0.022889592 -0.0016595855 -0.018619567 -0.0016595855 -0.014349446 -0.0016595855 
		-0.010079361 -0.0016595855 -0.0058092475 -0.0016595855 -0.0015392229 -0.0016595855 
		0.0027307793 -0.0016595855 0.0070009455 -0.0016595855 0.011271067 -0.0016595855 0.015541054 
		-0.0016595855 0.019811084 -0.0016595855 -0.03142976 -0.0017425762 -0.027159736 -0.0017425762 
		-0.022889592 -0.0017425762 -0.018619567 -0.0017425762 -0.014349446 -0.0017425762 
		-0.010079361 -0.0017425762 -0.0058092475 -0.0017425762 -0.0015392229 -0.0017425762 
		0.0027307793 -0.0017425762 0.0070009455 -0.0017425762 0.011271067 -0.0017425762 0.015541054 
		-0.0017425762 0.019811084 -0.0017425762 -0.03142976 -0.001825567 -0.027159736 -0.001825567 
		-0.022889592 -0.001825567 -0.018619567 -0.001825567 -0.014349446 -0.001825567 -0.010079361 
		-0.001825567 -0.0058092475 -0.001825567 -0.0015392229 -0.001825567 0.0027307793 -0.001825567 
		0.0070009455 -0.001825567 0.011271067 -0.001825567 0.015541054 -0.001825567 0.019811084 
		-0.001825567 -0.03142976 -0.0019085576 -0.027159736 -0.0019085576 -0.022889592 -0.0019085576 
		-0.018619567 -0.0019085576 -0.014349446 -0.0019085576 -0.010079361 -0.0019085576 
		-0.0058092475 -0.0019085576 -0.0015392229 -0.0019085576 0.0027307793 -0.0019085576 
		0.0070009455 -0.0019085576 0.011271067 -0.0019085576 0.015541054 -0.0019085576 0.019811084 
		-0.0019085576 -0.03142976 -0.001991489 -0.027159736 -0.001991489 -0.022889592 -0.001991489 
		-0.018619567 -0.001991489 -0.014349446 -0.001991489 -0.010079361 -0.001991489 -0.0058092475 
		-0.001991489 -0.0015392229 -0.001991489 0.0027307793 -0.001991489 0.0070009455 -0.001991489 
		0.011271067 -0.001991489 0.015541054 -0.001991489 0.019811084 -0.001991489 0.071052194 
		0.0019920468 0.066782072 0.0019920468 0.062512025 0.0019920468 0.058241963 0.0019920468 
		0.053971857 0.0019920468 0.049701855 0.0019920468 0.045431733 0.0019920468 0.041161567 
		0.0019920468 0.036891565 0.0019920468 0.03262157 0.0019920468 0.028351374 0.0019920468 
		0.024081325 0.0019920468 0.071052194 0.0019090276 0.066782072 0.0019090276 0.062512025 
		0.0019090276 0.058241963 0.0019090276 0.053971857 0.0019090276 0.049701855 0.0019090276 
		0.045431733 0.0019090276 0.041161567 0.0019090276 0.036891565 0.0019090276 0.03262157 
		0.0019090276 0.028351374 0.0019090276 0.024081325 0.0019090276 0.071052194 0.0018260665 
		0.066782072 0.0018260665 0.062512025 0.0018260665 0.058241963 0.0018260665 0.053971857 
		0.0018260665 0.049701855 0.0018260665 0.045431733 0.0018260665 0.041161567 0.0018260665 
		0.036891565 0.0018260665 0.03262157 0.0018260665 0.028351374 0.0018260665 0.024081325 
		0.0018260665 0.071052194 0.0017430497 0.066782072 0.0017430497 0.062512025 0.0017430497 
		0.058241963 0.0017430497 0.053971857 0.0017430497 0.049701855 0.0017430497 0.045431733 
		0.0017430497 0.041161567 0.0017430497 0.036891565 0.0017430497 0.03262157 0.0017430497 
		0.028351374 0.0017430497 0.024081325 0.0017430497 0.071052194 0.001660059 0.066782072 
		0.001660059 0.062512025 0.001660059 0.058241963 0.001660059 0.053971857 0.001660059 
		0.049701855 0.001660059 0.045431733 0.001660059 0.041161567 0.001660059 0.036891565 
		0.001660059 0.03262157 0.001660059 0.028351374 0.001660059 0.024081325 0.001660059 
		0.071052194 0.0015770905 0.066782072 0.0015770905 0.062512025 0.0015770905 0.058241963 
		0.0015770905 0.053971857 0.0015770905 0.049701855 0.0015770905 0.045431733 0.0015770905 
		0.041161567 0.0015770905 0.036891565 0.0015770905 0.03262157 0.0015770905 0.028351374 
		0.0015770905 0.024081325 0.0015770905 0.071052194 0.0014941072 0.066782072 0.0014941072 
		0.062512025 0.0014941072 0.058241963 0.0014941072 0.053971857 0.0014941072 0.049701855 
		0.0014941072 0.045431733 0.0014941072 0.041161567 0.0014941072 0.036891565 0.0014941072 
		0.03262157 0.0014941072 0.028351374 0.0014941072 0.024081325 0.0014941072 0.071052194 
		0.0014111015 0.066782072 0.0014111015 0.062512025 0.0014111015 0.058241963 0.0014111015 
		0.053971857 0.0014111015 0.049701855 0.0014111015 0.045431733 0.0014111015 0.041161567 
		0.0014111015 0.036891565 0.0014111015 0.03262157 0.0014111015 0.028351374 0.0014111015 
		0.024081325 0.0014111015 0.071052194 0.0013281108 0.066782072 0.0013281108 0.062512025 
		0.0013281108 0.058241963 0.0013281108 0.053971857 0.0013281108 0.049701855 0.0013281108 
		0.045431733 0.0013281108 0.041161567 0.0013281108 0.036891565 0.0013281108 0.03262157 
		0.0013281108 0.028351374 0.0013281108 0.024081325 0.0013281108 0.071052194 0.00124512 
		0.066782072 0.00124512 0.062512025 0.00124512 0.058241963 0.00124512 0.053971857 
		0.00124512;
	setAttr ".uvtk[750:948]" 0.049701855 0.00124512 0.045431733 0.00124512 0.041161567 
		0.00124512 0.036891565 0.00124512 0.03262157 0.00124512 0.028351374 0.00124512 0.024081325 
		0.00124512 0.071052194 0.0011621143 0.066782072 0.0011621143 0.062512025 0.0011621143 
		0.058241963 0.0011621143 0.053971857 0.0011621143 0.049701855 0.0011621143 0.045431733 
		0.0011621143 0.041161567 0.0011621143 0.036891565 0.0011621143 0.03262157 0.0011621143 
		0.028351374 0.0011621143 0.024081325 0.0011621143 0.071052194 0.0010791236 0.066782072 
		0.0010791236 0.062512025 0.0010791236 0.058241963 0.0010791236 0.053971857 0.0010791236 
		0.049701855 0.0010791236 0.045431733 0.0010791236 0.041161567 0.0010791236 0.036891565 
		0.0010791236 0.03262157 0.0010791236 0.028351374 0.0010791236 0.024081325 0.0010791236 
		0.071052194 0.00099614775 0.066782072 0.00099614775 0.062512025 0.00099614775 0.058241963 
		0.00099614775 0.053971857 0.00099614775 0.049701855 0.00099614775 0.045431733 0.00099614775 
		0.041161567 0.00099614775 0.036891565 0.00099614775 0.03262157 0.00099614775 0.028351374 
		0.00099614775 0.024081325 0.00099614775 -0.082670778 0.0019920468 -0.078400686 0.0019920468 
		-0.074130654 0.0019920468 -0.069860563 0.0019920468 -0.065590471 0.0019920468 -0.061320387 
		0.0019920468 -0.057050295 0.0019920468 -0.052780211 0.0019920468 -0.048510142 0.0019920468 
		-0.04424002 0.0019920468 -0.039969951 0.0019920468 -0.035699889 0.0019920468 -0.082670778 
		0.0019090276 -0.078400686 0.0019090276 -0.074130654 0.0019090276 -0.069860563 0.0019090276 
		-0.065590471 0.0019090276 -0.061320387 0.0019090276 -0.057050295 0.0019090276 -0.052780211 
		0.0019090276 -0.048510142 0.0019090276 -0.04424002 0.0019090276 -0.039969951 0.0019090276 
		-0.035699889 0.0019090276 -0.082670778 0.0018260665 -0.078400686 0.0018260665 -0.074130654 
		0.0018260665 -0.069860563 0.0018260665 -0.065590471 0.0018260665 -0.061320387 0.0018260665 
		-0.057050295 0.0018260665 -0.052780211 0.0018260665 -0.048510142 0.0018260665 -0.04424002 
		0.0018260665 -0.039969951 0.0018260665 -0.035699889 0.0018260665 -0.082670778 0.0017430497 
		-0.078400686 0.0017430497 -0.074130654 0.0017430497 -0.069860563 0.0017430497 -0.065590471 
		0.0017430497 -0.061320387 0.0017430497 -0.057050295 0.0017430497 -0.052780211 0.0017430497 
		-0.048510142 0.0017430497 -0.04424002 0.0017430497 -0.039969951 0.0017430497 -0.035699889 
		0.0017430497 -0.082670778 0.001660059 -0.078400686 0.001660059 -0.074130654 0.001660059 
		-0.069860563 0.001660059 -0.065590471 0.001660059 -0.061320387 0.001660059 -0.057050295 
		0.001660059 -0.052780211 0.001660059 -0.048510142 0.001660059 -0.04424002 0.001660059 
		-0.039969951 0.001660059 -0.035699889 0.001660059 -0.082670778 0.0015770905 -0.078400686 
		0.0015770905 -0.074130654 0.0015770905 -0.069860563 0.0015770905 -0.065590471 0.0015770905 
		-0.061320387 0.0015770905 -0.057050295 0.0015770905 -0.052780211 0.0015770905 -0.048510142 
		0.0015770905 -0.04424002 0.0015770905 -0.039969951 0.0015770905 -0.035699889 0.0015770905 
		-0.082670778 0.0014941072 -0.078400686 0.0014941072 -0.074130654 0.0014941072 -0.069860563 
		0.0014941072 -0.065590471 0.0014941072 -0.061320387 0.0014941072 -0.057050295 0.0014941072 
		-0.052780211 0.0014941072 -0.048510142 0.0014941072 -0.04424002 0.0014941072 -0.039969951 
		0.0014941072 -0.035699889 0.0014941072 -0.082670778 0.0014111015 -0.078400686 0.0014111015 
		-0.074130654 0.0014111015 -0.069860563 0.0014111015 -0.065590471 0.0014111015 -0.061320387 
		0.0014111015 -0.057050295 0.0014111015 -0.052780211 0.0014111015 -0.048510142 0.0014111015 
		-0.04424002 0.0014111015 -0.039969951 0.0014111015 -0.035699889 0.0014111015 -0.082670778 
		0.0013281108 -0.078400686 0.0013281108 -0.074130654 0.0013281108 -0.069860563 0.0013281108 
		-0.065590471 0.0013281108 -0.061320387 0.0013281108 -0.057050295 0.0013281108 -0.052780211 
		0.0013281108 -0.048510142 0.0013281108 -0.04424002 0.0013281108 -0.039969951 0.0013281108 
		-0.035699889 0.0013281108 -0.082670778 0.00124512 -0.078400686 0.00124512 -0.074130654 
		0.00124512 -0.069860563 0.00124512 -0.065590471 0.00124512 -0.061320387 0.00124512 
		-0.057050295 0.00124512 -0.052780211 0.00124512 -0.048510142 0.00124512 -0.04424002 
		0.00124512 -0.039969951 0.00124512 -0.035699889 0.00124512 -0.082670778 0.0011621143 
		-0.078400686 0.0011621143 -0.074130654 0.0011621143 -0.069860563 0.0011621143 -0.065590471 
		0.0011621143 -0.061320387 0.0011621143 -0.057050295 0.0011621143 -0.052780211 0.0011621143 
		-0.048510142 0.0011621143 -0.04424002 0.0011621143 -0.039969951 0.0011621143 -0.035699889 
		0.0011621143 -0.082670778 0.0010791236 -0.078400686 0.0010791236 -0.074130654 0.0010791236 
		-0.069860563 0.0010791236 -0.065590471 0.0010791236 -0.061320387 0.0010791236 -0.057050295 
		0.0010791236 -0.052780211 0.0010791236 -0.048510142 0.0010791236 -0.04424002 0.0010791236 
		-0.039969951 0.0010791236 -0.035699889 0.0010791236 -0.082670778 0.00099614775 -0.078400686 
		0.00099614775 -0.074130654 0.00099614775 -0.069860563 0.00099614775 -0.065590471 
		0.00099614775 -0.061320387 0.00099614775 -0.057050295 0.00099614775 -0.052780211 
		0.00099614775 -0.048510142 0.00099614775 -0.04424002 0.00099614775 -0.039969951 0.00099614775 
		-0.035699889 0.00099614775;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n"
		+ "                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n"
		+ "                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n"
		+ "                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n"
		+ "            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n"
		+ "                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 8192\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 8192\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n"
		+ "            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -constrainDrag 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n"
		+ "                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -constrainDrag 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n"
		+ "                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n"
		+ "                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"multiListerPanel\" (localizedPanelLabel(\"Multilister\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"multiListerPanel\" -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"devicePanel\" (localizedPanelLabel(\"Devices\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tdevicePanel -unParent -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tdevicePanel -edit -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"webBrowserPanel\" (localizedPanelLabel(\"Web Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"webBrowserPanel\" -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 8192\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels yes -displayOrthographicLabels yes -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode file -n "file1";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "C:/Documents and Settings/Danny/My Documents/maya/projects/asteroids//images/ateroiduv copy.tga";
createNode place2dTexture -n "place2dTexture1";
createNode bump2d -n "bump2d1";
	setAttr ".vc1" -type "float3" 0 2.9999999e-005 0 ;
	setAttr ".vc2" -type "float3" 9.9999997e-006 9.9999997e-006 0 ;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".maxr" 2;
	setAttr -s 27 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "raster use opacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode file -n "file2";
	setAttr ".ftn" -type "string" "C:/Documents and Settings/Danny/My Documents/maya/projects/asteroids//images/ateroiduv copy.tga";
createNode place2dTexture -n "place2dTexture2";
createNode blinn -n "blinn1";
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode file -n "file3";
	setAttr ".ail" yes;
	setAttr ".ftn" -type "string" "C:/Documents and Settings/Danny/My Documents/maya/projects/asteroids//images/ateroiduv copy.tga";
createNode place2dTexture -n "place2dTexture3";
createNode bump2d -n "bump2d2";
	setAttr ".vc1" -type "float3" 0 9.9999997e-006 0 ;
	setAttr ".vc2" -type "float3" 9.9999997e-006 9.9999997e-006 0 ;
createNode file -n "file4";
	setAttr ".ftn" -type "string" "C:/Documents and Settings/Danny/My Documents/maya/projects/asteroids//images/ateroid colour.jpg";
createNode place2dTexture -n "place2dTexture4";
select -ne :time1;
	setAttr ".o" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 7 ".u";
select -ne :lightList1;
select -ne :defaultTextureList1;
	setAttr -s 4 ".tx";
select -ne :lambert1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :initialMaterialInfo;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
select -ne :defaultLightSet;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyTweakUV1.out" "pCubeShape1.i";
connectAttr "polyTweakUV1.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
connectAttr ":defaultLightSet.msg" "lightLinker1.lnk[0].llnk";
connectAttr ":initialShadingGroup.msg" "lightLinker1.lnk[0].olnk";
connectAttr ":defaultLightSet.msg" "lightLinker1.lnk[1].llnk";
connectAttr ":initialParticleSE.msg" "lightLinker1.lnk[1].olnk";
connectAttr ":defaultLightSet.msg" "lightLinker1.lnk[2].llnk";
connectAttr "blinn1SG.msg" "lightLinker1.lnk[2].olnk";
connectAttr ":defaultLightSet.msg" "lightLinker1.slnk[0].sllk";
connectAttr ":initialShadingGroup.msg" "lightLinker1.slnk[0].solk";
connectAttr ":defaultLightSet.msg" "lightLinker1.slnk[1].sllk";
connectAttr ":initialParticleSE.msg" "lightLinker1.slnk[1].solk";
connectAttr ":defaultLightSet.msg" "lightLinker1.slnk[2].sllk";
connectAttr "blinn1SG.msg" "lightLinker1.slnk[2].solk";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyMapCut3.ip";
connectAttr "polyMapCut3.out" "polyMapCut4.ip";
connectAttr "polyMapCut4.out" "polyTweakUV1.ip";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "file1.oa" "bump2d1.bv";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr "bump2d2.o" "blinn1.n";
connectAttr "file4.oc" "blinn1.c";
connectAttr "file4.ot" "blinn1.it";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "pCubeShape1.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo1.sg";
connectAttr "blinn1.msg" "materialInfo1.m";
connectAttr "file4.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture3.c" "file3.c";
connectAttr "place2dTexture3.tf" "file3.tf";
connectAttr "place2dTexture3.rf" "file3.rf";
connectAttr "place2dTexture3.mu" "file3.mu";
connectAttr "place2dTexture3.mv" "file3.mv";
connectAttr "place2dTexture3.s" "file3.s";
connectAttr "place2dTexture3.wu" "file3.wu";
connectAttr "place2dTexture3.wv" "file3.wv";
connectAttr "place2dTexture3.re" "file3.re";
connectAttr "place2dTexture3.of" "file3.of";
connectAttr "place2dTexture3.r" "file3.ro";
connectAttr "place2dTexture3.n" "file3.n";
connectAttr "place2dTexture3.vt1" "file3.vt1";
connectAttr "place2dTexture3.vt2" "file3.vt2";
connectAttr "place2dTexture3.vt3" "file3.vt3";
connectAttr "place2dTexture3.vc1" "file3.vc1";
connectAttr "place2dTexture3.o" "file3.uv";
connectAttr "place2dTexture3.ofs" "file3.fs";
connectAttr "file3.oa" "bump2d2.bv";
connectAttr "place2dTexture4.c" "file4.c";
connectAttr "place2dTexture4.tf" "file4.tf";
connectAttr "place2dTexture4.rf" "file4.rf";
connectAttr "place2dTexture4.mu" "file4.mu";
connectAttr "place2dTexture4.mv" "file4.mv";
connectAttr "place2dTexture4.s" "file4.s";
connectAttr "place2dTexture4.wu" "file4.wu";
connectAttr "place2dTexture4.wv" "file4.wv";
connectAttr "place2dTexture4.re" "file4.re";
connectAttr "place2dTexture4.of" "file4.of";
connectAttr "place2dTexture4.r" "file4.ro";
connectAttr "place2dTexture4.n" "file4.n";
connectAttr "place2dTexture4.vt1" "file4.vt1";
connectAttr "place2dTexture4.vt2" "file4.vt2";
connectAttr "place2dTexture4.vt3" "file4.vt3";
connectAttr "place2dTexture4.vc1" "file4.vc1";
connectAttr "place2dTexture4.o" "file4.uv";
connectAttr "place2dTexture4.ofs" "file4.fs";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "bump2d2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "_spotLight1_mrLoc.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "lightLinker1.msg" ":lightList1.ln" -na;
connectAttr "spotLightShape1.ltd" ":lightList1.l" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file3.msg" ":defaultTextureList1.tx" -na;
connectAttr "file4.msg" ":defaultTextureList1.tx" -na;
connectAttr "bump2d1.o" ":lambert1.n";
connectAttr "file2.oc" ":lambert1.c";
connectAttr "file2.ot" ":lambert1.it";
connectAttr "file2.msg" ":initialMaterialInfo.t" -na;
connectAttr "spotLight1.iog" ":defaultLightSet.dsm" -na;
// End of asteroid-01.ma
