#include "Camera.h"

GameCamera::GameCamera(Ogre::SceneManager* sceneManager, Ogre::RenderWindow* rendWin, Character* c, bool followChar)
{

    renderWindow = rendWin;
    focus = c;
    tracking = followChar;

    camera = sceneManager->createCamera("Camera");
    camera->setPosition(0.0f, 500.0f, 535.0f);

    camera->setNearClipDistance(5.0f);
    camera->setFarClipDistance(6000.0f);

    // Set the camera to look at our handiwork

    camera->yaw(Degree(-90.0f));
    camera->pitch(Degree(-35.0f));

    viewport = renderWindow->addViewport(camera);
    viewport->setDimensions(0.0f, 0.0f, 1.0f, 1.0f);

    camera->setAspectRatio(Real(viewport->getActualWidth()) / Real(viewport->getActualHeight()));

    viewport->setCamera(camera);
    //ctor
}

GameCamera::~GameCamera()
{
    //dtor
}


Ogre::Camera* GameCamera::getCamera()
{
        return camera;
}

void GameCamera::track()
{
    tracking = true;
    camera->setAutoTracking(true, focus->myNode);
}


void GameCamera::track(Character* c)
{
    focus = c;
    tracking = true;
    camera->setAutoTracking(true, focus->myNode);
    //camera->
    focus->addCamera(camera);
    camera->setPosition(Ogre::Vector3(-400.0 ,400.0, 0.0));
}

bool GameCamera::isTracking()
{
        return tracking;
}
