//|||||||||||||||||||||||||||||||||||||||||||||||

#include "DemoApp.hpp"
#include "character.h"

#include <OgreLight.h>
#include <OgreWindowEventUtilities.h>
#include <cmath>
///*****************
///***********initalize vars
///*****************
DemoApp::DemoApp()
{
    bosco			= 0;
    alien           = 0;

}
///*****************
///************destructor
///*****************
DemoApp::~DemoApp()
{
    delete OgreFramework::getSingletonPtr();
}

///*****************
///**************start up game
///*****************
void DemoApp::startDemo()
{
    new OgreFramework();
    if (!OgreFramework::getSingletonPtr()->initOgre("Day of Reckoning v1.0", this, 0))
        return;

    m_bShutdown = false;

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("DoR initialized!");

    setupDemoScene();
    runDemo();
}

///*****************
///*****************place world objects
///*****************

void DemoApp::setupDemoScene()
{
    OgreFramework::getSingletonPtr()->m_pSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox");

    OgreFramework::getSingletonPtr()->m_pSceneMgr->createLight("Light");


    bosco = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Bosco", "robot.mesh", "boscoNode", OgreFramework::getSingletonPtr()->m_pLog, true);
    bosco->addToWorld(OgreFramework::getSingletonPtr()->getWorld());

    alien = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Alien", "test.mesh", "alienNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    alien->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    alien->scale(20.0,20.0,20.0);


}

///*****************
///********game loop
///*****************

void DemoApp::runDemo()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Start main loop...");

    double timeSinceLastFrame = 0.0;
    double startTime = 0.0;
    double lastTime = 0.0;

    OgreFramework::getSingletonPtr()->m_pRenderWnd->resetStatistics();

    while (!m_bShutdown && !OgreFramework::getSingletonPtr()->isOgreToBeShutDown())
    {
        //cout << timeSinceLastFrame << endl;
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;

//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        Ogre::WindowEventUtilities::messagePump();
#endif
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
        {
            startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();

            OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
            OgreFramework::getSingletonPtr()->m_pMouse->capture();

            OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
            OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();

            timeSinceLastFrame = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;
            if (!OgreFramework::getSingletonPtr()->pressed)
                bosco->idleState(timeSinceLastFrame);
            else
            {
                if (OgreFramework::getSingletonPtr()->left)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(-0.5, 0.0, 0.0), Quaternion(0.0,  0.0, 1.0, 0.0));
                if (OgreFramework::getSingletonPtr()->right)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.5, 0.0, 0.0), Quaternion(1.0,  0.0, 0.0, 0.0));
                if (OgreFramework::getSingletonPtr()->up)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, -0.5), Quaternion(sqrt(0.5), 0.0, sqrt(0.5), 0.0));
                if (OgreFramework::getSingletonPtr()->down)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, 0.5), Quaternion(sqrt(0.5), 0.0, -sqrt(0.5), 0.0));
            }
        }
        else
        {
            sleep(4000);
        }
    }

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Main loop quit");
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F))
    {
        //do something
    }

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

