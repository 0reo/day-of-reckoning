//|||||||||||||||||||||||||||||||||||||||||||||||
#define lout OgreFramework::getSingletonPtr()->m_pLog->logMessage


#include "DemoApp.hpp"
#include "Character.h"
#include "MainCharacter.h"
#include "Enemy.h"
#include "Weapon.h"

#include <OgreLight.h>
#include <OgreWindowEventUtilities.h>
#include <cmath>
///*****************
///***********initalize vars
///*****************
DemoApp::DemoApp()
{
    bosco			= 0;
    alien           = 0;
    chomppa         = 0;
    boagy           = 0;
    for (int i = 0; i < 5; i++)
    {
        chomppa2[i] = 0;

    }
        tree[0]     = 0;

}
///*****************
///************destructor
///*****************
DemoApp::~DemoApp()
{
    delete bosco;
    delete chomppa;
    delete boagy;
    delete chomppa2;
    delete tree;
    delete OgreFramework::getSingletonPtr();


}

///*****************
///**************start up game
///*****************
void DemoApp::startDemo()
{
    new OgreFramework();
    if (!OgreFramework::getSingletonPtr()->initOgre("Day of Reckoning v10.03.31", this, 0))
        return;


                /// modify this to set up inital render config options
        //RenderSystem *rs = mRoot->getRenderSystemByName("Direct3D9 Rendering Subsystem");
                                            // or use "OpenGL Rendering Subsystem"
        //mRoot->setRenderSystem(rs);
        //rs->setConfigOption("Full Screen", "No");
        //rs->setConfigOption("Video Mode", "800 x 600 @ 32-bit colour");
        ///use this to get settings available
        //Root::getAvailableRenderers

    m_bShutdown = false;

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("DoR initialized!");

    setupDemoScene();
    runDemo();
}

///*****************
///*****************place world objects
///*****************

void DemoApp::setupDemoScene()
{
    OgreFramework::getSingletonPtr()->m_pSceneMgr->setSkyDome(true, "Examples/CloudySky");

    OgreFramework::getSingletonPtr()->m_pSceneMgr->createLight("Light");

    ///bosco
    bosco = new MainCharacter(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Bosco", "bosco.mesh", "boscoNode", OgreFramework::getSingletonPtr()->m_pLog, true);
    bosco->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    bosco->setPosition(300.0, 0.0, 200.0);
    bosco->scale(100.0, 100.0, 100.0);
    OgreFramework::getSingletonPtr()->mainCamera->track(bosco);

    ///bosco's weapon
    Weapon* test = new Weapon(OgreFramework::getSingletonPtr()->m_pSceneMgr, "sword", "Cube.mesh", "testNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    bosco->addToCharacter(test, OgreFramework::getSingletonPtr()->getWorld());
    //test->setPosition(30.0, 30.0, 30.0);
    test->scale(50.0, 300.0, 11.0);
    test->addToWorld(OgreFramework::getSingletonPtr()->getWorld());


    ///chomppa
    //chomppa = new Enemy(OgreFramework::getSingletonPtr()->m_pSceneMgr, "chomppa", "Chomppa.mesh", "chomppaNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    //chomppa->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    //chomppa->scale(200.0, 200.0, 200.0);
    //chomppa->setPosition(600.0, 500.0, 0.0);
//
    ///boagy
    //boagy = new Enemy(OgreFramework::getSingletonPtr()->m_pSceneMgr, "boagy", "badguy2.mesh", "boagyNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    //boagy->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    //boagy->scale(200.0, 200.0, 200.0);
    //boagy->setPosition(200.0, 0.0, 0.0);


    ///chasing chomppa (demo)
    for (int i = 0; i < 5; i++)
    {

    chomppa2[i] = new Enemy(OgreFramework::getSingletonPtr()->m_pSceneMgr, string("chomppa2")+char(i), "Chomppa.mesh", string("chomppa2Node")+char(i), OgreFramework::getSingletonPtr()->m_pLog, true, char(i));
    chomppa2[i]->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    chomppa2[i]->setPosition(i*450.0, 0.0, 0.0);
    chomppa2[i]->scale(200.0, 200.0, 200.0);

    }

    bosco->addToCharacter(test, OgreFramework::getSingletonPtr()->getWorld());///weapon

    ///tree
    for (int i = 0; i < 1; i++)
    {
    tree[i] = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "tree"+i, "Trunk.mesh", "treeNode"+i, OgreFramework::getSingletonPtr()->m_pLog, false);
    //tree[i]->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    //tree[i]->scale(500.0,500.0,500.0);
    //tree[i]->setPosition(50*i, 0.0, 50*i);
    }

//    bosco->addToCharacter(alien->myEntity);

}

///*****************
///********game loop
///*****************

void DemoApp::runDemo()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Start main loop...");

    double timeSinceLastFrame = 0.0;
    double startTime = 0.0;
    double lastTime = 0.0;

    OgreFramework::getSingletonPtr()->m_pRenderWnd->resetStatistics();

    while (!m_bShutdown && !OgreFramework::getSingletonPtr()->isOgreToBeShutDown())
    {

        //cout << timeSinceLastFrame << endl;
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;

//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        Ogre::WindowEventUtilities::messagePump();
#endif

        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
        {

            startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();


            OgreFramework::getSingletonPtr()->inputMgr->m_pKeyboard->capture();
            //OIS::KeyEvent e(OgreFramework::getSingletonPtr()->inputMgr->m_pKeyboard, OIS::KC_UNASSIGNED, 0);


            OgreFramework::getSingletonPtr()->inputMgr->m_pMouse->capture();

            OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
            OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();

            timeSinceLastFrame = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;

            if (!OgreFramework::getSingletonPtr()->inputMgr->pressed)
                bosco->idleState(timeSinceLastFrame);
            else
            {
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("Key Pressed2");
                if (OgreFramework::getSingletonPtr()->inputMgr->left)
                    bosco->walk(timeSinceLastFrame, 5.5, Quaternion(0.0,  0.0, 1.0,  0.0), true);
                if (OgreFramework::getSingletonPtr()->inputMgr->right)
                    bosco->walk(timeSinceLastFrame, 5.5, Quaternion(0.0,  0.0, -1.0, 0.0), true);
                if (OgreFramework::getSingletonPtr()->inputMgr->up)
                  bosco->walk(timeSinceLastFrame, 33.0);
                if (OgreFramework::getSingletonPtr()->inputMgr->down)
                    bosco->walk(timeSinceLastFrame, 3, Quaternion(0.0, 0.0, 1.0, 0.0), true);
                if (OgreFramework::getSingletonPtr()->inputMgr->jump)
                {
                    cout << "yeah" << endl;
                    bosco->jumping = true;
                    bosco->setPosition(bosco->getPosition()+Ogre::Vector3(0.0, 5.0, 0.0));
                }
            }

            for (int i = 0; i < 5; i++)
                chomppa2[i]->detect();
        }
        else
        {
            sleep(4000);
                        //OgreFramework::getSingletonPtr()->m_pLog->logMessage("sleeping");
        }
    }

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Main loop quit");
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->inputMgr->keyPressed(keyEventRef);
//    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
//
//    if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F))
//    {
//        ///do something
//    }
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyReleased(const OIS::KeyEvent &keyEventRef)
{
//    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
OgreFramework::getSingletonPtr()->inputMgr->keyReleased(keyEventRef);
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||
