#ifndef CHARACTER_H
#define CHARACTER_H
#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "Weapon.h"

using namespace Ogre;
using namespace std;


class Character
{
public:
    Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated);
    virtual ~Character();

    void setPosition(Ogre::Vector3 pos);
    void setPosition(Real x, Real y, Real z);
    Ogre::Vector3 getPosition();
    Ogre::SceneNode* getNode();
    Ogre::Quaternion getOrientation();
    void addCamera(Ogre::Camera* cam);
    void addToCharacter(Weapon* weapon, btDiscreteDynamicsWorld* world);
    void addToWorld(btDiscreteDynamicsWorld* world);
    void clampToTerrain(BtOgre::RigidBodyState world);
    void scale(Ogre::Vector3 size);
    void scale(Real x, Real y, Real z);
    bool nextLocation();
    void walk(Real t, Real speed = 1.5, Ogre::Quaternion rotate = Ogre::Quaternion(1, 0, 0 ,0), bool rot = false);
    Ogre::Log* _log;
    void idleState(Real t);

    ///ogre vars
    Ogre::SceneNode* myCamera;
        Ogre::SceneNode* mySightNode;

    SceneManager* sceneManager;
    Ogre::Vector3 mDirection;
    Ogre::Vector3 oldPosition;
    Real mDistance;
    std::deque<Ogre::Vector3> mWalkList;   /// The list of points we are walking to

    ///bullet/btogre vars
    btRigidBody* mCharacterBody;      ///actual character
    btCollisionShape* mCharacterShape;///bounding shape
    BtOgre::RigidBodyState* characterState;//character motion state
    btTransform charTransform;          ///character transform info(translate/rotate)
    btTransform worldTransform;          ///world transform info(translate/rotate)

    ///other
    Real mWalkSpeed;
    Weapon* weapons[2];///weapons



    Ogre::Entity* myEntity;


protected:
private:

    btScalar mass;
    btVector3 inertia;

    Ogre::AnimationState* ani;
    RaySceneQuery* query;
    Ogre::Vector3 position;
    Ogre::Quaternion rotation;

    Ogre::SceneNode* myNode;


    void clampToTerrain();
};

#endif // CHARACTER_H

