#include "Character.h"
#include "btBulletDynamicsCommon.h"
#include <cmath>
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "iostream"
#include "BulletDynamics/Character/btKinematicCharacterController.h"
using namespace BtOgre;
using namespace std;

Character::Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log*	m_pLog, bool animated)
{
    //ctor


    _log = m_pLog;
    _log->logMessage(charName);
    sceneManager = sceneMgr;
    mWalkSpeed = 3.0;
    mDirection = Ogre::Vector3::ZERO;
    position = Vector3(100.0, 0.0, 100.0);
    rotation = Ogre::Quaternion::IDENTITY;
    mass = 500;
    inertia = btVector3(0.0, 0.0, 0.0);

    ///****************************************************setup character***//
    myEntity = sceneManager->createEntity(charName, meshFileName);                                  ///make entity
    myEntity->setCastShadows(true);                                                                 ///turn on shadows
    myNode = sceneManager->getRootSceneNode()->createChildSceneNode(nodeName, position, rotation);  ///make Node
    myNode->attachObject(myEntity);
    ///*************animations
    if (animated)
    {
        AnimatedMeshToShapeConverter converter(myEntity);                                           ///convert to btOgre format
        mCharacterShape = converter.createSphere();                                                    ///make shape (make sure to chage base on what char)

        ani = myEntity->getAnimationState("idle");
        ani->setLoop(true);
        ani->setEnabled(true);

    }
    else
    {

        StaticMeshToShapeConverter converter(myEntity);                                             ///convert to btOgre format
        mCharacterShape = converter.createBox();                                                    ///make shape (make sure to chage base on what char)

    }
    myNode->setInitialState();

    myEntity->setDisplaySkeleton(true);

    mCharacterShape->calculateLocalInertia(mass, inertia);                                          ///sets current character inertia

    characterState = new BtOgre::RigidBodyState(myNode);

    ///Create the Body.
    btRigidBody::btRigidBodyConstructionInfo mCharacterBodyCI(mass, characterState, mCharacterShape, inertia);
    mCharacterBodyCI.m_friction = 1.0;
    mCharacterBody = new btRigidBody(mCharacterBodyCI);

    mCharacterBody->setCollisionFlags( mCharacterBody->getCollisionFlags() | btCollisionObject::CF_CHARACTER_OBJECT);///do i want this flag??
    mCharacterBody->setActivationState(DISABLE_DEACTIVATION);
    mCharacterBody->setAngularFactor(btVector3(0,0,0));
    mCharacterBody->setDamping(1,5);
    mCharacterBody->applyDamping(1);


    //mCharacterBody->setLinearFactor(btVector3(1,0,1));






jumping = false;


//myEntity->mSkeletonInstance->createAnimation("test", 1.0);
    ///*******************************************************end character***//
}

//|||||||||||||||||||||||||||||||||||||||||||||||

Character::~Character()
{
    delete _log;                        ///log file
    delete myCamera;              ///node to place camera a distance from character
    delete mySightNode;           ///where the camera should look
    delete myEntity;                 ///the entity(make private)
    delete sceneManager;              ///scene manager;

    delete mCharacterBody;            ///actual character
    delete mCharacterShape;      ///bounding shape
    delete test;
    delete characterState; ///character motion state
    delete weapons;


    delete ani;              ///animations
    delete query;                   ///ray query(for terrain clamping)
    delete myNode;                ///character sceneNode
    //dtor
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Character::setPosition(Ogre::Vector3 pos)
{
    position = pos;
    //position.y = -800.0;

    characterState->getWorldTransform(charTransform);
    charTransform.setOrigin(BtOgre::Convert::toBullet(position));

    characterState->setWorldTransform(charTransform);
    mCharacterBody->setMotionState(characterState);

 //   clampToTerrain();
}

void Character::setPosition(Real x, Real y, Real z)
{
    position.x = x;
    position.y = y;
    position.z = z;
    characterState->getWorldTransform(charTransform);
    charTransform.setOrigin(BtOgre::Convert::toBullet(position));

    characterState->setWorldTransform(charTransform);
    mCharacterBody->setMotionState(characterState);

    //clampToTerrain();
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::Vector3 Character::getPosition()
{
    return position;
}

Ogre::Quaternion Character::getOrientation()
{
    return myNode->convertLocalToWorldOrientation(myNode->getOrientation());
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addCamera(Ogre::Camera* cam)
{
    myCamera = myNode->createChildSceneNode("myCam", Ogre::Vector3(this->getPosition().x-400, 0, this->getPosition().z-200) );
    mySightNode = myNode->createChildSceneNode ("sight", Ogre::Vector3(this->getPosition().x-200, 50, this->getPosition().z-200) );
//   Ogre::Entity* myEntity2 = sceneManager->createEntity("cament", "RABBIT_03.mesh"); ///make entity
//   mySightNode->attachObject(myEntity2);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addToCharacter(Weapon* weapon, btDiscreteDynamicsWorld* world)
{
    weapons[0] = weapon;///add weapon to weapon list
//    myNode->attachObject(obj);
    //myNode->addChild(weapons[0]->myNode);
    btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*mCharacterBody, *weapons[0]->mWeaponBody, btVector3(0.0, 300.0, 50.0), btVector3(-100.0, -100.0, -100.0));///make constraint for weapon/character
    world->addConstraint(p2p);///ad constraint
//    weapons[0] = w;

}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addToWorld(btDiscreteDynamicsWorld* world)
{
    world->addRigidBody(mCharacterBody);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::clampToTerrain(BtOgre::RigidBodyState world)
{
//    characterState->getWorldTransform(charTransform);
//    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());
//
////    world->getWorldTransform(worldTransform);
//    Ogre::Vector3 worldPos = BtOgre::Convert::toOgre(worldTransform.getOrigin());
//
//
//    mCharacterShape->
//
//
//
//    Ray cameraRay(Ogre::Vector3(charPos.x, 5000.0f, charPos.z), Ogre::Vector3::NEGATIVE_UNIT_Y);
//
//    query = sceneManager->createRayQuery(cameraRay);
//
//    static Ray cameraRay;
//    cameraRay.setOrigin(Ogre::Vector3(charPos.x, 5000.0f, charPos.z));
//    cameraRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
//    query->setRay(cameraRay);
//    RaySceneQueryResult& result = query->execute();
//    if (result.size() == 0)
//    {
//        // no result means we are below the terrain
//        // and need to go up fast.  Let's look up to find
//        // the position of the terrain
//        cameraRay.setOrigin(BtOgre::Convert::toOgre(charTransform.getOrigin()));
//        cameraRay.setDirection(Ogre::Vector3::UNIT_Y);
//        query->setRay(cameraRay);
//        result = query->execute();
//    }
//
//
//    RaySceneQueryResult::iterator i = result.begin();
//    //std::cout << i->worldFragment->singleIntersection.y << "\n";
//    cout << "running clamp" << endl;
//    if (i != result.end() && i->worldFragment)
//    {
//        //worldFragment wf = i->worldFragment;
//        Real terrainHeight = i->worldFragment->singleIntersection.y;
//        cout << terrainHeight << endl;
//
//        charTransform.setOrigin( btVector3(charPos.x, terrainHeight, charPos.z ) );
//        characterState->setWorldTransform(charTransform);
//        mCharacterBody->setMotionState(characterState);
//    }
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::clampToTerrain()
{

    characterState->getWorldTransform(charTransform);

    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());
//cout << "RAY - pos x = "<< charPos.x << " - pos y = "<< charPos.y << " - pos z = "<< charPos.z << endl;

    Ray cameraRay(Ogre::Vector3(charPos.x+25000.0, 50000.0f, charPos.z+25000.0), Ogre::Vector3::NEGATIVE_UNIT_Y);///did some voodoo here.  because of the differences in origin
                                                                                                                 ///in ogre and bullet, had too add an offset to the char position and
                                                                                                                 ///flip the z and x
    query = sceneManager->createRayQuery(cameraRay);

    RaySceneQueryResult& result = query->execute();
    if (result.size() == 0)
    {
        /// no result means we are below the terrain
        /// and need to go up fast.  Let's look up to find
        /// the position of the terrain
        cameraRay.setOrigin(BtOgre::Convert::toOgre(charTransform.getOrigin()));
        cameraRay.setDirection(Ogre::Vector3::UNIT_Y);
        query->setRay(cameraRay);
        result = query->execute();
    }
    //result = query->execute();

    RaySceneQueryResult::iterator i = result.begin();
    //std::cout << i->worldFragment->singleIntersection.y << "\n";
    ///cout << "running clamp" << endl;
    if (i != result.end() && i->worldFragment)
    {
        //worldFragment wf = i->worldFragment;
        Real terrainHeight = i->worldFragment->singleIntersection.y;


        cout << "terrain height: " << terrainHeight/255.0<< endl;
        if (!jumping)
        {
            setPosition((Ogre::Vector3(charPos.x, terrainHeight-(1630.0), charPos.z ) ));
        }

    }
    else
    {
        cout << "clamp not running" << endl;
    }
    mCharacterBody->setInterpolationWorldTransform(mCharacterBody->getWorldTransform());
    mCharacterBody->setInterpolationLinearVelocity(btVector3(0,0,0));
    mCharacterBody->setInterpolationAngularVelocity(btVector3(0,0,0));
    jumping = false;
}

//|||||||||||||||||||||||||||||||||||||||||||||||
void Character::jump()
{
    ani = myEntity->getAnimationState("walk");

	btVector3 up = charTransform.getBasis()[1];
	up.normalize ();
	btScalar magnitude = (btScalar(1.0)/mCharacterBody->getInvMass()) * btScalar(8.0);
	mCharacterBody->applyCentralImpulse (up * magnitude);


	characterState->getWorldTransform(charTransform);               ///get current character translation/orientation, put into charTransform
    charTransform.setOrigin(BtOgre::Convert::toBullet( BtOgre::Convert::toOgre( charTransform.getOrigin()+ mCharacterBody->getLinearVelocity()) ) );///translate character forward
    characterState->setWorldTransform(charTransform);                   ///set character transform
    mCharacterBody->setMotionState(characterState);                     ///apply transform

            ani->setLoop(true);
        ani->setEnabled(true);
        ani->addTime(Real(1.0));

cout << BtOgre::Convert::toOgre(mCharacterBody->getLinearVelocity()).y << endl;
    //mCharacterBody->applyForce(btVector3(0, 5000, 0), mCharacterBody->getCenterOfMassPosition());


}

void Character::walk(Real t, Real speed, Ogre::Quaternion rotate, bool rot)
{
mCharacterBody->clearForces();

    Ogre::Quaternion mOrientIntA;
    Ogre::Quaternion mOrientIntB;

//cout << myEntity->getName() << " is walking" << endl;
    //mDirection = myNode->getPosition();

    Real move = t*mWalkSpeed;
    //rotation = rotate;

    //myNode->setOrientation(rotation);

//    mDirection = translationVector;
    ani = myEntity->getAnimationState("walk");
    //myNode->translate(translationVector);
    //mCharacterBody->translate(BtOgre::Convert::toBullet(translationVector));
    characterState->getWorldTransform(charTransform);               ///get current character translation/orientation, put into charTransform
    if (rot)
    {
        mOrientIntA = Ogre::Quaternion::Slerp(.3, BtOgre::Convert::toOgre(charTransform.getRotation()), BtOgre::Convert::toOgre(charTransform.getRotation()) * rotate, true);
        mOrientIntB = Ogre::Quaternion::Slerp(.5, BtOgre::Convert::toOgre(charTransform.getRotation()), BtOgre::Convert::toOgre(charTransform.getRotation()) * rotate, true);

        //rotation = Ogre::Quaternion::Slerp(1.0/10.0, BtOgre::Convert::toOgre(charTransform.getRotation()), BtOgre::Convert::toOgre(charTransform.getRotation()) * rotate, true);///slerp to find current rotation
        rotation = Ogre::Quaternion::Squad(1.0/100.0, BtOgre::Convert::toOgre(charTransform.getRotation()), mOrientIntA, mOrientIntB, BtOgre::Convert::toOgre(charTransform.getRotation()) * rotate, true);///slerp to find current rotation

        //rotation = (((sin((1.0-t)*theta))/sin(theta))*rotate) + (((sin((t)*theta))/sin(theta))*rotate);

        charTransform.setRotation(charTransform.getRotation() + BtOgre::Convert::toBullet(rotation));///set new rotation
    }
    Ogre::Matrix3 rotationMatrix;
    Ogre::Matrix4 translation;
    //translate.makeTrans(BtOgre::Convert::toOgre(charTransform.getOrigin()));


    rotation = BtOgre::Convert::toOgre(charTransform.getRotation());///take current orientation
    rotation.ToRotationMatrix(rotationMatrix);                      ///make rotation matrix

    Ogre::Vector3 translationVector = rotationMatrix.GetColumn(0);  ///get orientation vector for forward movement
    translationVector *= move;                                     ///apply a velocity
    translation.makeTrans(translationVector);                       ///make a translation matrix



    ///translate

    myNode->convertWorldToLocalPosition(translationVector);        ///get local char position
    ///cout << "local x: "<< translationVector.x << "robot y: "<< translationVector.y <<" z: "<< translationVector.z <<endl;
    //setPosition(translation*BtOgre::Convert::toOgre( charTransform.getOrigin() ) );
    charTransform.setOrigin(BtOgre::Convert::toBullet( translation*BtOgre::Convert::toOgre( charTransform.getOrigin() ) ) );///translate character forward


    //weapons[0]->weaponTransform.setRotation(charTransform.getRotation());
    //weapons[0]->weaponTransform.setOrigin(weapons[0]->weaponTransform.getOrigin()+BtOgre::Convert::toBullet(translationVector));

    //btVector3 v = charTransform.getOrigin();
    ///cout << "robot x: "<< BtOgre::Convert::toOgre(v).x << "robot y: "<< BtOgre::Convert::toOgre(v).y <<"robot z: "<< BtOgre::Convert::toOgre(v).z <<endl;

    characterState->setWorldTransform(charTransform);                   ///set character transform

    //weapons[0]->weaponState->setWorldTransform(weapons[0]->weaponTransform);
    //mCharacterBody->applyCentralImpulse(btVector3(0, 0, 500));

    mCharacterBody->setMotionState(characterState);                     ///apply transform


    if (myEntity->getName() == "Bosco")
        weapons[0]->mWeaponBody->setMotionState(weapons[0]->weaponState);   ///apply transform to weapon

    //myNode->rotate(
    if (!jumping)
    {
        ///animate
        ani->setLoop(true);
        ani->setEnabled(true);
        ani->addTime(t/Real(200.0));

        clampToTerrain();
    }
//cout << mCharacterBody->btRigidBodyConstructionInfo::m_friction << endl;
    //cout << "***" << endl << endl;

}



//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::idleState(Real t)
{

        mCharacterBody->applyGravity();

	characterState->getWorldTransform(charTransform);               ///get current character translation/orientation, put into charTransform
    charTransform.setOrigin(BtOgre::Convert::toBullet( BtOgre::Convert::toOgre( charTransform.getOrigin()+ mCharacterBody->getLinearVelocity()) ) );///translate character forward
    characterState->setWorldTransform(charTransform);                   ///set character transform
    mCharacterBody->setMotionState(characterState);                     ///apply transform

    ani = myEntity->getAnimationState("idle");
    ani->setLoop(true);
    ani->setEnabled(true);
//    clampToTerrain();

cout << mCharacterShape-> << endl;


    ani->addTime(t/Real(400.0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::scale(Ogre::Vector3 size)
{
    clampToTerrain();
    mCharacterShape->setLocalScaling(BtOgre::Convert::toBullet(size));
    myNode->scale(size);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Character::scale(Real x, Real y, Real z)
{
    clampToTerrain();
    mCharacterShape->setLocalScaling(btVector3(x, y, z));
    myNode->scale(x, y, z);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::SceneNode* Character::getNode()
{
    return myNode;
}
