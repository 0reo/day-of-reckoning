#ifndef ENEMY_H
#define ENEMY_H

#include "Character.h"
#define CHOMPPA 0
#define BOAGY 1
typedef bool EnemyType;


class Enemy : public Character
{
    public:
        Enemy(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated, char charId);
        virtual ~Enemy();
        void detect();

        Ogre::Vector3 detectRadius;
        EnemyType type;
        double sensitivity;
        RaySceneQuery* detectQuery;                       ///ray query(for detecting surrounding area)
        RaySceneQuery* detectQueryY;                   ///ray query(for detecting surrounding area)
        Ogre::RegionSceneQuery* test;

    protected:
    private:
    char id;
    ManualObject* detectLine;
    SceneNode* detectLineNode;
};

#endif // ENEMY_H
