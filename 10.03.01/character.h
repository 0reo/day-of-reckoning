#ifndef CHARACTER_H
#define CHARACTER_H
#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"


using namespace Ogre;
using namespace std;

class Character
{
    public:
        Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated);
        virtual ~Character();

        void setPosition(Ogre::Vector3 pos);
        void setPosition(Real x, Real y, Real z);
        void addToWorld(btDiscreteDynamicsWorld* world);
        void clampToTerrain();
        void scale(Ogre::Vector3 size);
        void scale(Real x, Real y, Real z);
        bool nextLocation();
        void walk(Real t, Ogre::Vector3 translationVector, Ogre::Quaternion rotate);
        Ogre::Log* _log;
        void idleState(Real t);


        Ogre::SceneNode* myNode;
        Ogre::Entity* myEntity;
        SceneManager* sceneManager;
        Ogre::AnimationState* ani;
        RaySceneQuery* query;

        btRigidBody* mCharacterBody;
        btCollisionShape* mCharacterShape;
        BtOgre::RigidBodyState* characterState;

        Real mWalkSpeed;
        Ogre::Vector3 mDirection;
        Ogre::Vector3 oldPosition;
        Real mDistance;
        std::deque<Ogre::Vector3> mWalkList;   // The list of points we are walking to





    protected:
    private:

        btScalar mass;
        btVector3 inertia;
    Ogre::Vector3 position;
    Ogre::Quaternion rotation;
};

#endif // CHARACTER_H

