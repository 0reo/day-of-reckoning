#include "character.h"
#include "btBulletDynamicsCommon.h"
#include <cmath>
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
using namespace BtOgre;

Character::Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log*	m_pLog, bool animated)
{
    //ctor
    sceneManager = sceneMgr;
    mWalkSpeed = 35.0;
    mDirection = Ogre::Vector3::ZERO;
    _log = m_pLog;
    position = Vector3(0,0,0);
	rotation = Ogre::Quaternion::IDENTITY;
    mass = 50;//set character mass
    inertia = btVector3(0.0, 0.0, 0.0);

///****************************************************setup character***//
    myEntity = sceneManager->createEntity(charName, meshFileName); //make entity
    myNode = sceneManager->getRootSceneNode()->createChildSceneNode(nodeName, position, rotation); //make Node
    myNode->attachObject(myEntity);

//*************animations
if (animated)
{
    AnimatedMeshToShapeConverter converter(myEntity); //convert to btOgre format
    mCharacterShape = converter.createBox();//make shape (make sure to chage base on what char)

    ani = myEntity->getAnimationState("Idle");
    ani->setLoop(true);
    ani->setEnabled(true);

}
else
{
    StaticMeshToShapeConverter converter(myEntity); //convert to btOgre format
    mCharacterShape = converter.createBox();//make shape (make sure to chage base on what char)

}





    //myNode->setPosition(750.0, 0.0, 735.0);
    myNode->setInitialState();
    myEntity->setDisplaySkeleton(true);

    mass = 50;//set character mass
    inertia = btVector3(0.0, 0.0, 0.0);
	mCharacterShape->calculateLocalInertia(mass, inertia);//sets current character inertia

	characterState = new BtOgre::RigidBodyState(myNode);

		    //Create the Body.
    mCharacterBody = new btRigidBody(mass, characterState, mCharacterShape, inertia);


//myEntity->mSkeletonInstance->createAnimation("test", 1.0);


///*******************************************************end character***//
}

Character::~Character()
{
    //dtor
}

void Character::setPosition(Ogre::Vector3 pos)
{
    position = pos;
    myNode->setPosition(position);

}

void Character::setPosition(Real x, Real y, Real z)
{
    position.x = x;
    position.y = y;
    position.z = z;
    myNode->setPosition(position);
}

void Character::addToWorld(btDiscreteDynamicsWorld* world)
{
        world->addRigidBody(mCharacterBody);
}


void Character::clampToTerrain()
{

    Ogre::Vector3 charPos = myNode->getPosition();
    Ray cameraRay(Ogre::Vector3(charPos.x, 5000.0f, charPos.z), Ogre::Vector3::NEGATIVE_UNIT_Y);
    query = sceneManager->createRayQuery(cameraRay);

    static Ray updateRay;
    updateRay.setOrigin(Ogre::Vector3(charPos.x, 5000.0f, charPos.z));
    updateRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
    query->setRay(updateRay);
    RaySceneQueryResult& result = query->execute();
    if (result.size() == 0)
    {
        // no result means we are below the terrain
        // and need to go up fast.  Lets look up to find
        // the position of the terrain
        updateRay.setOrigin(myNode->getPosition());
        updateRay.setDirection(Ogre::Vector3::UNIT_Y);
        query->setRay(updateRay);
    }


    RaySceneQueryResult::iterator i = result.begin();
    //std::cout << i->worldFragment->singleIntersection.y << "\n";

    if (i != result.end() && i->worldFragment)
    {
        //worldFragment wf = i->worldFragment;
        Real terrainHeight = i->worldFragment->singleIntersection.y;

        myNode->setPosition(
            myNode->getPosition().x,
            terrainHeight,
            myNode->getPosition().z);
    }

}


bool Character::nextLocation()
{
    if (mWalkList.empty())
        return false;

//    mDestination = mWalkList.front();  // this gets the front of the deque
    mWalkList.pop_front();             // this removes the front of the deque


    mDistance = mDirection.normalise();
    return true;
}

void Character::walk(Real t, Ogre::Vector3 translationVector, Ogre::Quaternion rotate)
{


    mDirection = myNode->getPosition();

    Real move = t/mWalkSpeed;
    //myNode->getOrientation;
    rotation = rotate;

    myNode->setOrientation(rotation);




//    mDirection = translationVector;


    ani = myEntity->getAnimationState("Walk");
    //myNode->translate(translationVector);
    mCharacterBody->translate(BtOgre::Convert::toBullet(translationVector));
    //myNode->rotate(

   // ani->setLoop(true);
   // ani->setEnabled(true);

    clampToTerrain();

}
void Character::idleState(Real t)
{
    ani = myEntity->getAnimationState("Idle");
        ani->setLoop(true);
    ani->setEnabled(true);
    clampToTerrain();
    ani->addTime(t/Real(400.0));
}

void Character::scale(Ogre::Vector3 size)
{
    clampToTerrain();
    mCharacterShape->setLocalScaling(BtOgre::Convert::toBullet(size));
    myNode->scale(size);
}
void Character::scale(Real x, Real y, Real z)
{
       clampToTerrain();
           mCharacterShape->setLocalScaling(btVector3(x, y, z));
myNode->scale(x, y, z);
}
