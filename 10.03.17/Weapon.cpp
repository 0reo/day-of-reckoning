#include "Weapon.h"
#include "btBulletDynamicsCommon.h"
#include <cmath>
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
using namespace BtOgre;

Weapon::Weapon(SceneManager* sceneMgr, string weaponName, string meshFileName, string nodeName, Ogre::Log*	m_pLog, bool animated)
{
    //ctor
    sceneManager = sceneMgr;
    mMoveSpeed = 35.0;
    mDirection = Ogre::Vector3::ZERO;
    _log = m_pLog;
    position = Vector3(0,0,0);
    rotation = Ogre::Quaternion::IDENTITY;
    mass = 2;//set character mass
    inertia = btVector3(0.0, 0.0, 0.0);

///****************************************************setup character***//
    myEntity = sceneManager->createEntity(weaponName, meshFileName); //make entity
    myNode = sceneManager->getRootSceneNode()->createChildSceneNode(nodeName); //make Node


    myNode->attachObject(myEntity);


//*************animations
    if (animated)
    {
        AnimatedMeshToShapeConverter converter(myEntity); //convert to btOgre format
        mWeaponShape = converter.createBox();//make shape (make sure to chage base on what char)

        ani = myEntity->getAnimationState("Idle");
        ani->setLoop(true);
        ani->setEnabled(true);

    }
    else
    {
        StaticMeshToShapeConverter converter(myEntity); //convert to btOgre format
        mWeaponShape = converter.createBox();//make shape (make sure to chage base on what char)

    }





    //myNode->setPosition(750.0, 0.0, 735.0);
    //myNode->setInitialState();
    myEntity->setDisplaySkeleton(true);

    mass = 5;//set character mass
    inertia = btVector3(0.0, 0.0, 0.0);
    mWeaponShape->calculateLocalInertia(mass, inertia);//sets current character inertia
//mWeaponBody->setCenterOfMassTransform(btTransform::getIdentity);
    weaponState = new BtOgre::RigidBodyState(myNode);

    //Create the Body.
    mWeaponBody = new btRigidBody(mass, weaponState, mWeaponShape, inertia);


//mWeaponBody->setCollisionFlags( mWeaponBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
//mWeaponBody->setActivationState(DISABLE_DEACTIVATION);
//myEntity->mSkeletonInstance->createAnimation("test", 1.0);


///*******************************************************end character***//
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Weapon::~Weapon()
{
    //dtor
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Weapon::setPosition(Ogre::Vector3 pos)
{
    position = pos;
    myNode->setPosition(position);
    mWeaponBody->translate(BtOgre::Convert::toBullet(position));
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Weapon::setPosition(Real x, Real y, Real z)
{
    position.x = x;
    position.y = y;
    position.z = z;
    myNode->setPosition(position);
    mWeaponBody->translate(BtOgre::Convert::toBullet(position));
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::Vector3 Weapon::getPosition()
{
    return position;
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::Quaternion Weapon::getOrientation()
{
    return myNode->convertLocalToWorldOrientation(myNode->getOrientation());
}


//|||||||||||||||||||||||||||||||||||||||||||||||

void Weapon::addToWorld(btDiscreteDynamicsWorld* world)
{
    world->addRigidBody(mWeaponBody);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool Weapon::nextLocation()
{
    if (mWalkList.empty())
        return false;

//    mDestination = mWalkList.front();  // this gets the front of the deque
    mWalkList.pop_front();             // this removes the front of the deque


    mDistance = mDirection.normalise();
    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Weapon::walk(Real t, Ogre::Vector3 translationVector, Ogre::Quaternion rotate)
{


    //mDirection = myNode->getPosition();

    Real move = t/mMoveSpeed;
    //rotation = rotate;

    //myNode->setOrientation(rotation);

//    mDirection = translationVector;
    ani = myEntity->getAnimationState("Walk");
    //myNode->translate(translationVector);
    //mWeaponBody->translate(BtOgre::Convert::toBullet(translationVector));
    weaponState->getWorldTransform(weaponTransform);

    rotation = Ogre::Quaternion::Slerp(move, BtOgre::Convert::toOgre(weaponTransform.getRotation()), BtOgre::Convert::toOgre(weaponTransform.getRotation()) + rotate, true);

    //rotation = (((sin((1.0-t)*theta))/sin(theta))*rotate) + (((sin((t)*theta))/sin(theta))*rotate);

    weaponTransform.setRotation(weaponTransform.getRotation() + BtOgre::Convert::toBullet(rotation));
    weaponTransform.setOrigin(weaponTransform.getOrigin()+BtOgre::Convert::toBullet(translationVector));

    weaponState->setWorldTransform(weaponTransform);
    mWeaponBody->setMotionState(weaponState);


    //myNode->rotate(

    ani->setLoop(true);
    ani->setEnabled(true);
    ani->addTime(t/Real(400.0));

    //clampToTerrain();

}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Weapon::move()
{
    weaponTransform.setOrigin(weaponTransform.getOrigin()+btVector3(0.0, 0.0, -3.0));

    weaponState->setWorldTransform(weaponTransform);
    mWeaponBody->setMotionState(weaponState);

}


//|||||||||||||||||||||||||||||||||||||||||||||||

void Weapon::idleState(Real t)
{
    ani = myEntity->getAnimationState("Idle");
    ani->setLoop(true);
    ani->setEnabled(true);

    ani->addTime(t/Real(400.0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Weapon::scale(Ogre::Vector3 size)
{

    mWeaponShape->setLocalScaling(BtOgre::Convert::toBullet(size));
    myNode->scale(size);
}
//|||||||||||||||||||||||||||||||||||||||||||||||


void Weapon::scale(Real x, Real y, Real z)
{

    mWeaponShape->setLocalScaling(btVector3(x, y, z));
    myNode->scale(x, y, z);
}
