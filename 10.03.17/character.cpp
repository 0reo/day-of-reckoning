#include "character.h"
#include "btBulletDynamicsCommon.h"
#include <cmath>
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "iostream"
#include "BulletDynamics/Character/btKinematicCharacterController.h"
using namespace BtOgre;
using namespace std;

Character::Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log*	m_pLog, bool animated)
{
    //ctor
    sceneManager = sceneMgr;
    mWalkSpeed = 35.0;
    mDirection = Ogre::Vector3::ZERO;
    _log = m_pLog;
    position = Vector3(100.0, 1000.0, 100.0);
    rotation = Ogre::Quaternion::IDENTITY;
    mass = 0;
    inertia = btVector3(0.0, 0.0, 0.0);

    ///****************************************************setup character***//
    myEntity = sceneManager->createEntity(charName, meshFileName);                                  ///make entity
    myEntity->setCastShadows(true);                                                                 ///turn on shadows
    myNode = sceneManager->getRootSceneNode()->createChildSceneNode(nodeName, position, rotation);  ///make Node
    myNode->attachObject(myEntity);

    ///*************animations
    if (animated)
    {
        AnimatedMeshToShapeConverter converter(myEntity);                                           ///convert to btOgre format
        mCharacterShape = converter.createBox();                                                    ///make shape (make sure to chage base on what char)

        ani = myEntity->getAnimationState("Idle");
        ani->setLoop(true);
        ani->setEnabled(true);

    }
    else
    {

        StaticMeshToShapeConverter converter(myEntity);                                             ///convert to btOgre format
        mCharacterShape = converter.createBox();                                                    ///make shape (make sure to chage base on what char)

    }
    myNode->setInitialState();
    myEntity->setDisplaySkeleton(true);

    mCharacterShape->calculateLocalInertia(mass, inertia);                                          ///sets current character inertia

    characterState = new BtOgre::RigidBodyState(myNode);

    ///Create the Body.
    mCharacterBody = new btRigidBody(mass, characterState, mCharacterShape, inertia);

    //mCharacterBody->setCollisionFlags( mCharacterBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);///do i want this flag??
    //mCharacterBody->setActivationState(DISABLE_DEACTIVATION);
test =

//myEntity->mSkeletonInstance->createAnimation("test", 1.0);
    ///*******************************************************end character***//
}

//|||||||||||||||||||||||||||||||||||||||||||||||

Character::~Character()
{
    //dtor
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Character::setPosition(Ogre::Vector3 pos)
{
    position = pos;

    characterState->getWorldTransform(charTransform);
    charTransform.setOrigin(BtOgre::Convert::toBullet(position));

    characterState->setWorldTransform(charTransform);
    mCharacterBody->setMotionState(characterState);

    clampToTerrain();
}

void Character::setPosition(Real x, Real y, Real z)
{
    position.x = x;
    position.y = y;
    position.z = z;
    characterState->getWorldTransform(charTransform);
    charTransform.setOrigin(BtOgre::Convert::toBullet(position));

    characterState->setWorldTransform(charTransform);
    mCharacterBody->setMotionState(characterState);

    clampToTerrain();
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::Vector3 Character::getPosition()
{
    return position;
}

Ogre::Quaternion Character::getOrientation()
{
    return myNode->convertLocalToWorldOrientation(myNode->getOrientation());
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addCamera(Ogre::Camera* cam)
{
    myCamera = myNode->createChildSceneNode("myCam", Ogre::Vector3(this->getPosition().x-400, 0, this->getPosition().z-200) );
    mySightNode = myNode->createChildSceneNode ("sight", Ogre::Vector3(this->getPosition().x-200, 50, this->getPosition().z-200) );
//   Ogre::Entity* myEntity2 = sceneManager->createEntity("cament", "RABBIT_03.mesh"); ///make entity
//   mySightNode->attachObject(myEntity2);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addToCharacter(Weapon* weapon, btDiscreteDynamicsWorld* world)
{
    weapons[0] = weapon;///add weapon to weapon list
//    myNode->attachObject(obj);
    //myNode->addChild(weapons[0]->myNode);
    btPoint2PointConstraint* p2p = new btPoint2PointConstraint(*mCharacterBody, *weapons[0]->mWeaponBody, btVector3(0.0, 10.0, 100.0), btVector3(0.0, 0.0, 0.0));///make constraint for weapon/character
    world->addConstraint(p2p);///ad constraint
//    weapons[0] = w;

}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::addToWorld(btDiscreteDynamicsWorld* world)
{
    world->addRigidBody(mCharacterBody);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::clampToTerrain(BtOgre::RigidBodyState world)
{
//    characterState->getWorldTransform(charTransform);
//    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());
//
////    world->getWorldTransform(worldTransform);
//    Ogre::Vector3 worldPos = BtOgre::Convert::toOgre(worldTransform.getOrigin());
//
//
//    mCharacterShape->
//
//
//
//    Ray cameraRay(Ogre::Vector3(charPos.x, 5000.0f, charPos.z), Ogre::Vector3::NEGATIVE_UNIT_Y);
//
//    query = sceneManager->createRayQuery(cameraRay);
//
//    static Ray cameraRay;
//    cameraRay.setOrigin(Ogre::Vector3(charPos.x, 5000.0f, charPos.z));
//    cameraRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);
//    query->setRay(cameraRay);
//    RaySceneQueryResult& result = query->execute();
//    if (result.size() == 0)
//    {
//        // no result means we are below the terrain
//        // and need to go up fast.  Let's look up to find
//        // the position of the terrain
//        cameraRay.setOrigin(BtOgre::Convert::toOgre(charTransform.getOrigin()));
//        cameraRay.setDirection(Ogre::Vector3::UNIT_Y);
//        query->setRay(cameraRay);
//        result = query->execute();
//    }
//
//
//    RaySceneQueryResult::iterator i = result.begin();
//    //std::cout << i->worldFragment->singleIntersection.y << "\n";
//    cout << "running clamp" << endl;
//    if (i != result.end() && i->worldFragment)
//    {
//        //worldFragment wf = i->worldFragment;
//        Real terrainHeight = i->worldFragment->singleIntersection.y;
//        cout << terrainHeight << endl;
//
//        charTransform.setOrigin( btVector3(charPos.x, terrainHeight, charPos.z ) );
//        characterState->setWorldTransform(charTransform);
//        mCharacterBody->setMotionState(characterState);
//    }
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::clampToTerrain()
{



    characterState->getWorldTransform(charTransform);

    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());

    Ray cameraRay(Ogre::Vector3(charPos.x, 5000.0f, charPos.z), Ogre::Vector3::NEGATIVE_UNIT_Y);

    query = sceneManager->createRayQuery(cameraRay);

    RaySceneQueryResult& result = query->execute();
    if (result.size() == 0)
    {
        // no result means we are below the terrain
        // and need to go up fast.  Let's look up to find
        // the position of the terrain
        cameraRay.setOrigin(BtOgre::Convert::toOgre(charTransform.getOrigin()));
        cameraRay.setDirection(Ogre::Vector3::UNIT_Y);
        query->setRay(cameraRay);
        //result = query->execute();
    }


    RaySceneQueryResult::iterator i = result.begin();
    //std::cout << i->worldFragment->singleIntersection.y << "\n";
    cout << "running clamp" << endl;
    if (i != result.end() && i->worldFragment)
    {
        //worldFragment wf = i->worldFragment;
        Real terrainHeight = i->worldFragment->singleIntersection.y;
        cout << "terrain height: " <<terrainHeight << endl;

        charTransform.setOrigin( btVector3(charPos.x, terrainHeight, charPos.z ) );
        characterState->setWorldTransform(charTransform);
        mCharacterBody->setMotionState(characterState);
    }
    mCharacterBody->setInterpolationWorldTransform(mCharacterBody->getWorldTransform());
    mCharacterBody->setInterpolationLinearVelocity(btVector3(0,0,0));
    mCharacterBody->setInterpolationAngularVelocity(btVector3(0,0,0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Character::walk(Real t, Real speed, Ogre::Quaternion rotate, bool rot)
{


    //mDirection = myNode->getPosition();

    Real move = t/mWalkSpeed;
    //rotation = rotate;

    //myNode->setOrientation(rotation);

//    mDirection = translationVector;
    ani = myEntity->getAnimationState("Walk");
    //myNode->translate(translationVector);
    //mCharacterBody->translate(BtOgre::Convert::toBullet(translationVector));
    characterState->getWorldTransform(charTransform);///get current character translation/orientation, put into charTransform
    if (rot)
    {
        rotation = Ogre::Quaternion::Slerp(move/30.0, BtOgre::Convert::toOgre(charTransform.getRotation()), BtOgre::Convert::toOgre(charTransform.getRotation()) * rotate, true);///slerp to find current rotation

        //rotation = (((sin((1.0-t)*theta))/sin(theta))*rotate) + (((sin((t)*theta))/sin(theta))*rotate);

        charTransform.setRotation(charTransform.getRotation() + BtOgre::Convert::toBullet(rotation));///set new rotation
    }
    Ogre::Matrix3 rotationMatrix;
    Ogre::Matrix4 translation;
    //translate.makeTrans(BtOgre::Convert::toOgre(charTransform.getOrigin()));


    rotation = BtOgre::Convert::toOgre(charTransform.getRotation());///take current orientation
    rotation.ToRotationMatrix(rotationMatrix);                      ///make rotation matrix

    Ogre::Vector3 translationVector = rotationMatrix.GetColumn(0);  ///get orientation vector for forward movement
    translationVector *= speed;                                     ///apply a velocity
    translation.makeTrans(translationVector);                       ///make a translation matrix



    ///translate
    myNode->convertWorldToLocalPosition(translationVector);         ///get local char position
    cout << "local x: "<< translationVector.x << "robot y: "<< translationVector.y <<" z: "<< translationVector.z <<endl;

    charTransform.setOrigin(BtOgre::Convert::toBullet( translation*BtOgre::Convert::toOgre( charTransform.getOrigin() ) ) );///translate character forward


    //weapons[0]->weaponTransform.setRotation(charTransform.getRotation());
    //weapons[0]->weaponTransform.setOrigin(weapons[0]->weaponTransform.getOrigin()+BtOgre::Convert::toBullet(translationVector));

    btVector3 v = charTransform.getOrigin();
    cout << "robot x: "<< BtOgre::Convert::toOgre(v).x << "robot y: "<< BtOgre::Convert::toOgre(v).y <<"robot z: "<< BtOgre::Convert::toOgre(v).z <<endl;

    characterState->setWorldTransform(charTransform);                   ///set character transform
    //weapons[0]->weaponState->setWorldTransform(weapons[0]->weaponTransform);

    mCharacterBody->setMotionState(characterState);                     ///apply transform
    weapons[0]->mWeaponBody->setMotionState(weapons[0]->weaponState);   ///apply transform to weapon

    //myNode->rotate(

    ///animate
    ani->setLoop(true);
    ani->setEnabled(true);
    ani->addTime(t/Real(400.0));

    clampToTerrain();
    // mCharacterBody->applyCentralImpulse(btVector3(200, 20, 200));

    cout << "***" << endl << endl;

}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::idleState(Real t)
{
    ani = myEntity->getAnimationState("Idle");
    ani->setLoop(true);
    ani->setEnabled(true);
    clampToTerrain();
    ani->addTime(t/Real(400.0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void Character::scale(Ogre::Vector3 size)
{
    clampToTerrain();
    mCharacterShape->setLocalScaling(BtOgre::Convert::toBullet(size));
    myNode->scale(size);
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void Character::scale(Real x, Real y, Real z)
{
    clampToTerrain();
    mCharacterShape->setLocalScaling(btVector3(x, y, z));
    myNode->scale(x, y, z);
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::SceneNode* Character::getNode()
{
    return myNode;
}
