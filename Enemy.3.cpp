#include "Enemy.h"
#include <Ogre.h>
using namespace Ogre;

Enemy::Enemy(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated)
        :Character(sceneMgr, charName, meshFileName, nodeName, m_pLog, animated)
{
    //ctor
    if (nodeName == "chomppaNode")
        type = CHOMPPA;
    else if (nodeName == "boagyNode")
        type = BOAGY;

    detectRadius = Ogre::Vector3(300.0, 0.0, 30.0);

    switch (type)
    {
    case CHOMPPA:
        detectRadius = Ogre::Vector3(30.0, 0.0, 30.0);
        sensitivity = 0.5;
        break;
    case BOAGY:
        detectRadius = Ogre::Vector3(30.0, 30.0, 10.0);
        sensitivity = 0.8;
        break;
    }

    detectLine =  sceneManager->createManualObject("line");
    detectLineNode = sceneManager->getRootSceneNode()->createChildSceneNode("line_node");

        MaterialPtr detectLineMaterial = MaterialManager::getSingleton().create("lineMaterial","General");
detectLineMaterial->setReceiveShadows(false);
detectLineMaterial->getTechnique(0)->setLightingEnabled(true);
detectLineMaterial->getTechnique(0)->getPass(0)->setDiffuse(1,0,0,0);
detectLineMaterial->getTechnique(0)->getPass(0)->setAmbient(1,0,0);
detectLineMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0);
//detectLineMaterial->dispose();  // dispose pointer, not the material





}

Enemy::~Enemy()
{
    delete detectLine;
    delete detectLineNode;
    delete detectQuery;
    delete detectQueryY;
    delete test;
    //dtor
}


void Enemy::detect()
{

    characterState->getWorldTransform(charTransform);
    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());

    Ray detectRay(charPos, Ogre::Vector3::UNIT_X);  ///set up ray pointing forward

    test = sceneManager->createSphereQuery(Ogre::Sphere(charPos, 5050.0));
    test->setQueryMask(Ogre::SceneManager::ENTITY_TYPE_MASK);

    detectQuery = sceneManager->createRayQuery(detectRay);      ///make forward ray
    detectQuery->setQueryTypeMask(Ogre::SceneManager::ENTITY_TYPE_MASK);


    SceneQueryResult& resultS = test->execute();
    RaySceneQueryResult& result = detectQuery->execute();       ///get forward ray results


    if (result.size() == 0 )              ///no y means we are below ground;  no x means player is not in range
    {
        detectRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_X);

        detectQuery->setRay(detectRay);

        result = detectQuery->execute();
    }

    SceneQueryResultMovableList::iterator iS = resultS.movables.begin();
    RaySceneQueryResult::iterator i = result.begin();

    cout << "running detect" << endl;

    for (; (iS != resultS.movables.end() ) ; iS++)
    {
        if ( (*iS)->getName() != myEntity->getName())
        {
            if ((*iS)->getName() == "Bosco")
            {

                Ogre::Vector3 cPos = (*iS)->getParentNode()->getPosition();

                detectLine->clear();

                detectLine->begin("lineMaterial", Ogre::RenderOperation::OT_LINE_LIST);
                detectLine->position(charPos.x, charPos.y, charPos.z);
                detectLine->position(cPos.x, cPos.y, cPos.z);
// etc
                detectLine->end();




                if (detectLineNode->numAttachedObjects() == 0)
                    detectLineNode->attachObject(detectLine);

                Vector3 mDirection = cPos - charPos;
                Vector3 src = myNode->getOrientation() * Vector3::UNIT_X;
                src.y = 0;                                                    /// Ignore pitch difference angle
                mDirection.y = 0;
                src.normalise();
                Real mDistance = mDirection.normalise( );                     /// Both vectors modified so renormalize them
                Quaternion turn = src.getRotationTo(mDirection, Vector3::UNIT_X);

                if (mDistance < 0.0)
                    mDistance *= -1.0;


                if (mDistance < 550.0)
                {

                }
                else if (mDistance < 1450.0)
                {
                    cout << "don't turn - " << mDistance << endl;

                        charTransform->setRotation();
                        characterState->setWorldTransform(charTransform);                   ///set character transform
                        mCharacterBody->setMotionState(characterState);                     ///apply transform

                    walk(10.0, 8.0, turn, false);
                }
                else
                {
                    cout << "turn - " << mDistance << endl;
                    walk(10.0, 20.0, turn, true);
                }
            }
        }
    }

    mCharacterBody->setInterpolationWorldTransform(mCharacterBody->getWorldTransform());
    mCharacterBody->setInterpolationLinearVelocity(btVector3(0,0,0));
    mCharacterBody->setInterpolationAngularVelocity(btVector3(0,0,0));
}
