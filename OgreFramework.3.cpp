//|||||||||||||||||||||||||||||||||||||||||||||||

#include "OgreFramework.hpp"
#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"
#include "iostream"


//|||||||||||||||||||||||||||||||||||||||||||||||

using namespace Ogre;
using namespace std;
//|||||||||||||||||||||||||||||||||||||||||||||||

template<> OgreFramework* Ogre::Singleton<OgreFramework>::ms_Singleton = 0;

//|||||||||||||||||||||||||||||||||||||||||||||||

OgreFramework::OgreFramework()
{
    m_MoveSpeed			= 10.5f;
    m_RotateSpeed		= 17.5f;

    m_bShutDownOgre		= false;
    m_iNumScreenShots	= 0;

    m_pRoot				= 0;
    m_pSceneMgr			= 0;
    m_pRenderWnd		= 0;
    mainCamera          = 0;
    m_pViewport			= 0;
    m_pLog				= 0;
    m_pTimer			= 0;

    inputMgr            = 0;
//    m_pInputMgr			= 0;
//    m_pKeyboard			= 0;
//    m_pMouse			= 0;
//    pressed             = false;

    m_pDebugOverlay		= 0;
    m_pInfoOverlay		= 0;
//    left                = false;
//    right               = false;
//    up                  = false;
//    down                = false;
    hWnd = 0;

}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool OgreFramework::initOgre(Ogre::String wndTitle, OIS::KeyListener *pKeyListener, OIS::MouseListener *pMouseListener)
{
    Ogre::LogManager* logMgr = new Ogre::LogManager();

    m_pLog = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
    m_pLog->setDebugOutputEnabled(true);

    m_pRoot = new Ogre::Root();

    if (!m_pRoot->restoreConfig() && !m_pRoot->showConfigDialog())
        return false;
    m_pRenderWnd = m_pRoot->initialise(true, wndTitle);


    m_pSceneMgr = m_pRoot->createSceneManager("TerrainSceneManager", "SceneManager");
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

    //m_pSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE);


    mainCamera = new GameCamera(m_pSceneMgr, m_pRenderWnd);

    //OIS::ParamList paramList;///
    m_pRenderWnd->getCustomAttribute("WINDOW", &hWnd);

    //paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

    //m_pInputMgr = OIS::InputManager::createInputSystem(paramList);
    inputMgr = new DorInputManager(m_pRenderWnd, hWnd, pKeyListener, pMouseListener);


    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile rcf;///resources config file
    rcf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator seci = rcf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    m_pSceneMgr->setWorldGeometry("terrain.cfg");


    /**physics stuff*/

    /// Build the broadphase
    mBroadphase = new btAxisSweep3(btVector3(-10000,-10000,-10000), btVector3(10000,10000,10000), 1024);


    /// Set up the collision configuration and dispatcher
    mCollisionConfig = new btDefaultCollisionConfiguration();
    mDispatcher = new btCollisionDispatcher(mCollisionConfig);

    /// The actual physics solver
    mSolver = new btSequentialImpulseConstraintSolver();

    /// The world.
    mWorld = new btDiscreteDynamicsWorld(mDispatcher, mBroadphase, mSolver, mCollisionConfig);
    mWorld->setGravity(btVector3(0.0, -9.8, 0.0));

    Ogre::ConfigFile pcf;///plugins config file

    pcf.load( "./media/terrain.cfg" );
    /** Load the various settings we need.
      * These are:
      * width:  Saved in the terrainFile as PageSize.  We pass width to Bullet twice, as both width and height,
      *     because heightfields in Ogre are square.
      * imgFileStr: the string representing the image from which we want to load the heightmap data.
      * heightmap: an Ogre::Image object, which contains the data itself.  A reference to this needs to be kept
      *     somewhere, because just keeps a pointer to the data, and the data itself will be deleted when the
      *     Image is deleted at the end of this block.  I found this out the hard way >.>
      * maxHeight: the heightmap will be scaled to go from zero to maxHeight
      * heightScale: this is how much each number in the heightmap (0 - 255) will translate to in Ogre/Bullet units.
      *    This number is obtained by dividing the maxHeight by 256 which is the total number of different steps in
      *    the heightmap before interpolation.
      */

    float width =        atof( pcf.getSetting( "PageSize"   ).c_str() );
    float terrainSizeX = atof( pcf.getSetting( "PageWorldX" ).c_str() );
    float terrainSizeZ = atof( pcf.getSetting( "PageWorldZ" ).c_str() );
    Ogre::String imgFileStr = pcf.getSetting( "Heightmap.image" );

    Ogre::Image* heightmap = new Ogre::Image();
    heightmap->load("height_map3.jpg", Ogre::ResourceGroupManager::getSingleton().getWorldResourceGroupName());

    Ogre::String maxHeightStr = pcf.getSetting("MaxHeight");
    btScalar maxHeight = atof(maxHeightStr.c_str());
    btScalar heightScale = maxHeight / 256;

    btVector3 localScaling(1, 1, 1);
    btScalar vScaleX = terrainSizeX;
    vScaleX /= (width - 1);
    btScalar vScaleZ = terrainSizeZ;
    vScaleZ /= (width - 1);
    localScaling.setX(vScaleX);
    localScaling.setZ(vScaleZ);


    mGroundShape = new btHeightfieldTerrainShape(width, width, heightmap->getData(), heightScale, 0, maxHeight, 1, PHY_UCHAR, false);

    mGroundShape->setLocalScaling(localScaling);

    btVector3 min, max;
    mGroundShape->getAabb(btTransform::getIdentity(), min, max);

    //cout << "min x: " << BtOgre::Convert::toOgre(min).x << "min y: " << BtOgre::Convert::toOgre(min).y << "min z: " << BtOgre::Convert::toOgre(min).z << endl;

    SceneNode *sNode = m_pSceneMgr->getSceneNode("Terrain");
    sNode->setPosition(BtOgre::Convert::toOgre(min));



    BtOgre::RigidBodyState* terrainState = new BtOgre::RigidBodyState(sNode, btTransform (btQuaternion(0,0,0, 1), btVector3(0, 0, 0) ) );
    mGroundBody = new btRigidBody(0, terrainState, mGroundShape, btVector3(0, 0, 0));
    mGroundBody->setCollisionFlags(mGroundBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);
    mWorld->addRigidBody(mGroundBody);

    dbgdraw = new BtOgre::DebugDrawer(m_pSceneMgr->getRootSceneNode(), mWorld);
    mWorld->setDebugDrawer(dbgdraw);
    /**end physics stuff*/


    m_pTimer = new Ogre::Timer();
    m_pTimer->reset();

    m_pDebugOverlay = OverlayManager::getSingleton().getByName("Core/DebugOverlay");
    m_pDebugOverlay->show();

    m_pRenderWnd->setActive(true);


    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

OgreFramework::~OgreFramework()
{
    //if (m_pInputMgr)
    //    OIS::InputManager::destroyInputSystem(m_pInputMgr);

    delete mBroadphase;
    delete mWorld;
    delete mSolver;
    delete mDispatcher;
    delete mCollisionConfig;

	delete			m_pSceneMgr;
	delete			m_pRenderWnd;
	delete				m_pViewport;
	delete					m_pLog;
	delete				m_pTimer;
	delete                 mainCamera;

    delete            inputMgr;
	delete				m_pKeyboard;
	delete					m_pMouse;


    delete m_pDebugOverlay;
	delete m_pInfoOverlay;


    delete ani;

    delete dbgdraw;
    delete mBroadphase;///broadphase collision detection
	delete mCollisionConfig;///fine tune algorithms for non-broadphase collisions
	delete mDispatcher;               ///|||| |||| |||||||||| ||| |||||||||||||| ||||||||||
	delete mSolver;///applies interections involving gravity, forces, etc
    delete mWorld;///broadphase world
    delete mGroundShape;
    delete mGroundBody;

    delete m_pRoot;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

//bool OgreFramework::keyPressed(const OIS::KeyEvent &keyEventRef)
//{
//    if (m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
//    {
////        keepRendering = false;
//        m_bShutDownOgre = true;
//        return true;
//    }
//
//    if (m_pKeyboard->isKeyDown(OIS::KC_SYSRQ))
//    {
//        m_pRenderWnd->writeContentsToTimestampedFile("BOF_Screenshot_", ".jpg");
//        return true;
//    }
//
//    if (m_pKeyboard->isKeyDown(OIS::KC_M))
//    {
//        static int mode = 0;
//
//        if (mode == 2)
//        {
//            mainCamera->getCamera()->setPolygonMode(PM_SOLID);
//            mode = 0;
//        }
//        else if (mode == 0)
//        {
//            mainCamera->getCamera()->setPolygonMode(PM_WIREFRAME);
//            mode = 1;
//        }
//        else if (mode == 1)
//        {
//            mainCamera->getCamera()->setPolygonMode(PM_POINTS);
//            mode = 2;
//        }
//    }
//
//    if (m_pKeyboard->isKeyDown(OIS::KC_O))
//    {
//        if (m_pDebugOverlay)
//        {
//            if (!m_pDebugOverlay->isVisible())
//                m_pDebugOverlay->show();
//            else
//                m_pDebugOverlay->hide();
//        }
//    }
//
//    return true;
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

//bool OgreFramework::keyReleased(const OIS::KeyEvent &keyEventRef)
//{
//    return true;
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

//bool OgreFramework::mouseMoved(const OIS::MouseEvent &evt)
//{
//    //mainCamera->getCamera()->yaw(Degree(evt.state.X.rel * -0.1f));
//    //mainCamera->getCamera()->pitch(Degree(evt.state.Y.rel * -0.1f));
//    return true;
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

//bool OgreFramework::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
//{
//    return true;
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

//bool OgreFramework::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
//{
//    return true;
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

void OgreFramework::updateOgre(double timeSinceLastFrame)
{
    m_MoveScale = m_MoveSpeed   * (float)timeSinceLastFrame;
    m_RotScale  = m_RotateSpeed * (float)timeSinceLastFrame;

    m_TranslateVector = Ogre::Vector3::ZERO;
//            OgreFramework::getSingletonPtr()->m_pLog->logMessage("Made it");

    inputMgr->getInput();
    moveCamera();

    updateStats();
    mWorld->stepSimulation(timeSinceLastFrame, 10);
    debugView();




}

//|||||||||||||||||||||||||||||||||||||||||||||||

void OgreFramework::updateStats()
{
    static string currFps	= "Current FPS: ";
    static string avgFps	= "Average FPS: ";
    static string bestFps	= "Best FPS: ";
    static string worstFps	= "Worst FPS: ";
    static string tris		= "Triangle Count: ";
    static string batches	= "Batch Count: ";

    OverlayElement* guiAvg	= OverlayManager::getSingleton().getOverlayElement("Core/AverageFps");
    OverlayElement* guiCurr	= OverlayManager::getSingleton().getOverlayElement("Core/CurrFps");
    OverlayElement* guiBest	= OverlayManager::getSingleton().getOverlayElement("Core/BestFps");
    OverlayElement* guiWorst = OverlayManager::getSingleton().getOverlayElement("Core/WorstFps");

    const RenderTarget::FrameStats& stats = m_pRenderWnd->getStatistics();
    guiAvg->setCaption(avgFps + StringConverter::toString(stats.avgFPS));
    guiCurr->setCaption(currFps + StringConverter::toString(stats.lastFPS));
    guiBest->setCaption(bestFps + StringConverter::toString(stats.bestFPS)
                        +" "+StringConverter::toString(stats.bestFrameTime)+" ms");
    guiWorst->setCaption(worstFps + StringConverter::toString(stats.worstFPS)
                         +" "+StringConverter::toString(stats.worstFrameTime)+" ms");

    OverlayElement* guiTris = OverlayManager::getSingleton().getOverlayElement("Core/NumTris");
    guiTris->setCaption(tris + StringConverter::toString(stats.triangleCount));

    OverlayElement* guiBatches = OverlayManager::getSingleton().getOverlayElement("Core/NumBatches");
    guiBatches->setCaption(batches + StringConverter::toString(stats.batchCount));

    OverlayElement* guiDbg = OverlayManager::getSingleton().getOverlayElement("Core/DebugText");
    guiDbg->setCaption("");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void OgreFramework::moveCamera()
{
    //if (m_pKeyboard->isKeyDown(OIS::KC_LSHIFT))
    //    mainCamera->getCamera()->moveRelative(m_TranslateVector);
    //else
    if (inputMgr->camUp)
    {
        m_TranslateVector.z = -m_MoveScale;
    }

    if (inputMgr->camDown)
    {
        m_TranslateVector.z = m_MoveScale;
    }

    if (inputMgr->camRight)
    {
        m_TranslateVector.x = m_MoveScale;
    }

    if (inputMgr->camLeft)
    {
        m_TranslateVector.x = -m_MoveScale;
    }
        mainCamera->getCamera()->moveRelative(m_TranslateVector / 10);

    mainCamera->track();
}


void OgreFramework::debugView()
{


    //mWorld->debugDrawWorld();
    if (inputMgr->debug != dbgdraw->getDebugMode())
        dbgdraw->setDebugMode(inputMgr->debug);

    dbgdraw->step();

}
//|||||||||||||||||||||||||||||||||||||||||||||||

//void OgreFramework::getInput()
//{
//    pressed = false;
//    left                = false;
//    right               = false;
//    up                  = false;
//    down                = false;
//
//    m_TranslateVector.x = 0.0;
//    m_TranslateVector.z = 0.0;
//
//    if (inputMgr->m_pKeyboard->isKeyDown(KC_UP) )
//    {
////        oldPosition.z = m_TranslateVector.z;
//        //m_TranslateVector.z = -m_MoveScale;
//        //direction.z = oldPosition.z - translateVector.z;
//        pressed = true;
//        up = pressed;
//    }
////    if (m_pKeyboard->isKeyDown(KC_DOWN) )
////    {
////        //oldPosition.z = m_translateVector.z;
////        //m_TranslateVector.z = m_MoveScale;
////        //std::cout << oldPosition.z - translateVector.z << "\n";
////        pressed = true;
////        down = pressed;
////    }
////    if (m_pKeyboard->isKeyDown(KC_RIGHT) )
////    {
////        // oldPosition.x = m_TranslateVector.x;
////        //m_TranslateVector.x = m_MoveScale;
////        pressed = true;
////        right = pressed;
////    }
////
////    if (m_pKeyboard->isKeyDown(KC_LEFT) )
////    {
////        //oldPosition.x = m_TranslateVector.x;
////        // m_TranslateVector.x = -m_MoveScale;
////        pressed = true;
////        left = pressed;
////    }
////

//
//    Ogre::Vector3 pos(0,0,0);
//
//}

//|||||||||||||||||||||||||||||||||||||||||||||||

btDiscreteDynamicsWorld* OgreFramework::getWorld()
{
    return mWorld;
}
