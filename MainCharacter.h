#ifndef MAINCHARACTER_H
#define MAINCHARACTER_H

#include "Character.h"
#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "Weapon.h"


class MainCharacter : public Character
{
    public:
        MainCharacter(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated);
        virtual ~MainCharacter();
    protected:
    private:
};

#endif // MAINCHARACTER_H
