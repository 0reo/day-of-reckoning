#include "InputManager.h"
#include "OgreFramework.hpp"

DorInputManager::DorInputManager()
{}
DorInputManager::DorInputManager(Ogre::RenderWindow* m_pRendWnd, unsigned long hWnd, OIS::KeyListener *pKeyListener, OIS::MouseListener *pMouseListener)
{

    //ctor
    m_pInputMgr			= 0;
    m_pKeyboard			= 0;
    m_pMouse			= 0;
    pressed             = false;

    left                = false;
    right               = false;
    up                  = false;
    down                = false;
    move                = false;

    camLeft                = false;
    camRight               = false;
    camUp                  = false;
    camDown                = false;

    debug               = false;

    m_pRenderWnd = m_pRendWnd;


    OIS::ParamList paramList;///
    paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));


    m_pInputMgr = OIS::InputManager::createInputSystem(paramList);
    m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputMgr->createInputObject(OIS::OISKeyboard, true));

    m_pMouse = static_cast<OIS::Mouse*>(m_pInputMgr->createInputObject(OIS::OISMouse, true));

    m_pMouse->getMouseState().height = m_pRenderWnd->getHeight();
    m_pMouse->getMouseState().width	 = m_pRenderWnd->getWidth();



    if (pKeyListener == 0)
        m_pKeyboard->setEventCallback(this);
    else
        m_pKeyboard->setEventCallback(pKeyListener);

    if (pMouseListener == 0)
        m_pMouse->setEventCallback(this);
    else
        m_pMouse->setEventCallback(pMouseListener);

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Input Manager Started");

}

DorInputManager::~DorInputManager()
{
    //dtor
    OIS::InputManager::destroyInputSystem(m_pInputMgr);
	delete m_pKeyboard;
	delete m_pKeyListener;
	delete m_pMouse;
    delete m_pRenderWnd;
}

DorInputManager* DorInputManager::getInstance()
{
//    if (!inputMgr)
//        inputMgr = new DorInputManager(OgreFramework::m_pRenderWnd, OgreFramework::hWnd, OgreFramework::pKeyListener, OgreFramework::pMouseListener);
//    return inputMgr;

}

bool DorInputManager::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    //pressed = true;
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("KeyPressed Works");



    if (m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {

        OgreFramework::getSingletonPtr()->m_bShutDownOgre = true;
        return true;

    }
    ///*********************************************************************///
    ///*********************************************************************///
    ///*********************************************************************///





    if (m_pKeyboard->isKeyDown(OIS::KC_V))                                                  ///kick
    {
        return true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_SPACE) && m_pKeyboard->isKeyDown(OIS::KC_DOWN))      ///duck
    {

        return true;
    }

        if (m_pKeyboard->isKeyDown(OIS::KC_A))                                              ///move weapon left
    {
        return true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_S))///move weapon down
    {
        m_pRenderWnd->writeContentsToTimestampedFile("BOF_Screenshot_", ".jpg");
        return true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_D))///move weapon right
    {
        return true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_W))///move weapon up
    {
        return true;
    }

        if (m_pKeyboard->isKeyDown(OIS::KC_F))///fire
    {
        return true;
    }

    ///*********************************************************************///
    ///*********************************************************************///
    ///*********************************************************************///

    if (m_pKeyboard->isKeyDown(OIS::KC_SYSRQ))
    {
        m_pRenderWnd->writeContentsToTimestampedFile("BOF_Screenshot_", ".jpg");
        return true;
    }



    if (m_pKeyboard->isKeyDown(OIS::KC_M))
    {
        static int mode = 0;

        if (mode == 2)
        {
//            mainCamera->getCamera()->setPolygonMode(PM_SOLID);
            mode = 0;
        }
        else if (mode == 0)
        {
 //           mainCamera->getCamera()->setPolygonMode(PM_WIREFRAME);
            mode = 1;
        }
        else if (mode == 1)
        {
//            mainCamera->getCamera()->setPolygonMode(PM_POINTS);
            mode = 2;
        }
    }



    return true;
}

bool DorInputManager::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}


bool DorInputManager::mouseMoved(const OIS::MouseEvent &evt)
{
    //mainCamera->getCamera()->yaw(Degree(evt.state.X.rel * -0.1f));
    //mainCamera->getCamera()->pitch(Degree(evt.state.Y.rel * -0.1f));
    return true;
}

bool DorInputManager::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

bool DorInputManager::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

void DorInputManager::getInput()///unbuffered input, use for movement
{


    pressed             = false;
    left                = false;
    right               = false;
    up                  = false;
    down                = false;
    move                = false;
    camLeft                = false;
    camRight               = false;
    camUp                  = false;
    camDown                = false;
    jump                   = false;

//    m_TranslateVector.x = 0.0;
//    m_TranslateVector.z = 0.0;
    if (m_pKeyboard->isKeyDown(OIS::KC_UP))                                                 ///move forward
    {
        //OgreFramework::getSingletonPtr()->m_pLog->logMessage("Up Pressed");
//        oldPosition.z = m_TranslateVector.z;
        //m_TranslateVector.z = -m_MoveScale;
        //direction.z = oldPosition.z - translateVector.z;
        pressed = true;
        up = true;
        move = true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_DOWN))                                               ///move back
    {
        //oldPosition.z = m_translateVector.z;
        //m_TranslateVector.z = m_MoveScale;
        //std::cout << oldPosition.z - translateVector.z << "\n";
        pressed = true;
        down = pressed;
        move = true;
    }

    if (m_pKeyboard->isKeyDown(OIS::KC_LEFT))                                               ///move left
    {
        // oldPosition.x = m_TranslateVector.x;
        //m_TranslateVector.x = m_MoveScale;
        pressed = true;
        left = pressed;
        move = true;

    }
    if (m_pKeyboard->isKeyDown(OIS::KC_RIGHT))                                              ///move right
    {
        //oldPosition.x = m_TranslateVector.x;
        // m_TranslateVector.x = -m_MoveScale;
        pressed = true;
        right = pressed;
        move = true;

    }
        if (m_pKeyboard->isKeyDown(OIS::KC_SPACE))                                              ///jump
    {
        pressed = true;
        jump = true;
        //cout << "jump" << endl;
    }

    if (m_pKeyboard->isKeyDown(KC_W))
    {
        //m_TranslateVector.z = -m_MoveScale;
        pressed = true;
        camUp = pressed;
    }

    if (m_pKeyboard->isKeyDown(KC_S))
    {
     //   m_TranslateVector.z = m_MoveScale;
             pressed = true;
        camDown = pressed;
    }

    if (m_pKeyboard->isKeyDown(KC_D))
    {
                pressed = true;
        camRight = pressed;
        //m_TranslateVector.x = m_MoveScale;
    }

    if (m_pKeyboard->isKeyDown(KC_A))
    {
                pressed = true;
        camLeft = pressed;
        //m_TranslateVector.x = -m_MoveScale;
    }

    if (m_pKeyboard->isKeyDown(KC_O))
    {
        debug = !debug;
    }

    Ogre::Vector3 pos(0,0,0);

}
