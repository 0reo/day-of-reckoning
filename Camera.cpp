#include "Camera.h"

GameCamera::GameCamera(Ogre::SceneManager* sceneManager, Ogre::RenderWindow* rendWin, Character* c, bool followChar)
{

    renderWindow = rendWin; ///set render window
    focus = c;              ///set character this camera will follow
    tracking = followChar;  ///set if camera will follow character

    myCamera = sceneManager->getRootSceneNode()->createChildSceneNode ("Camera");///make camera scene node
    myTarget = sceneManager->getRootSceneNode()->createChildSceneNode ("Target");///make target scene node

    camera = sceneManager->createCamera("Camera");///make camera
    camera->setPosition(0.0f, 400.0f, -235.0f);
    myCamera->setPosition(0.0f, 400.0f, -235.0f);

    camera->setNearClipDistance(50.0f);
    camera->setFarClipDistance(50000.0f);

    // Set the camera to look at our handiwork

    //camera->yaw(Degree(-90.0f));
    //camera->pitch(Degree(-35.0f));

    viewport = renderWindow->addViewport(camera);
    viewport->setDimensions(0.0f, 0.0f, 1.0f, 1.0f);

    camera->setAspectRatio(Real(viewport->getActualWidth()) / Real(viewport->getActualHeight()));

    viewport->setCamera(camera);///set viewport

    myCamera->attachObject(camera);
    myCamera->setFixedYawAxis (true);
    //ctor
}

//|||||||||||||||||||||||||||||||||||||||||||||||


GameCamera::~GameCamera()
{
    delete camera;
    delete myCamera;
    delete myTarget;
   	delete viewport;
    delete renderWindow;
    delete focus;
    //dtor
}

//|||||||||||||||||||||||||||||||||||||||||||||||


Ogre::Camera* GameCamera::getCamera()
{
    return camera;
}

//|||||||||||||||||||||||||||||||||||||||||||||||


void GameCamera::track()
{
    if (!isTracking())
    {
        myCamera->setAutoTracking(true, myTarget);
        tracking = true;
        focus->addCamera(camera);
    }
    myTarget->setPosition(focus->getPosition());


    myCamera->translate((focus->myCamera->_getDerivedPosition()- myCamera->getPosition()) *0.1);
    myTarget->translate((focus->mySightNode->_getDerivedPosition() - myTarget->getPosition()) *0.1);
//camera->lookAt(focus->getPosition());
    camera->lookAt(focus->mySightNode->_getDerivedPosition());
    //camera->setPosition(Ogre::Vector3(-400.0 ,400.0, 0.0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void GameCamera::track(Character* c)
{
    focus = c;

    if (!isTracking())
    {
        myCamera->setAutoTracking(true, myTarget);
        tracking = true;
        focus->addCamera(camera);
    }
    myTarget->setPosition(focus->getPosition());


    myCamera->translate((focus->myCamera->_getDerivedPosition()- myCamera->getPosition()) *0.1);
    myTarget->translate((focus->mySightNode->_getDerivedPosition() - myTarget->getPosition()) *0.1);
//camera->lookAt(focus->getPosition());
    camera->lookAt(focus->mySightNode->_getDerivedPosition());
    //camera->setPosition(Ogre::Vector3(-400.0 ,400.0, 0.0));
}

//|||||||||||||||||||||||||||||||||||||||||||||||


bool GameCamera::isTracking()
{
    return tracking;
}
