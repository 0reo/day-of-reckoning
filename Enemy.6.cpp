#include "Enemy.h"
#include <Ogre.h>
using namespace Ogre;

Enemy::Enemy(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated)
        :Character(sceneMgr, charName, meshFileName, nodeName, m_pLog, animated)
{
    //ctor
    if (nodeName == "chomppaNode")
        type = CHOMPPA;
    else if (nodeName == "boagyNode")
        type = BOAGY;

    detectRadius = Ogre::Vector3(300.0, 0.0, 30.0);

    switch (type)
    {
    case CHOMPPA:
        detectRadius = Ogre::Vector3(30.0, 0.0, 30.0);
        sensitivity = 0.5;
        break;
    case BOAGY:
        detectRadius = Ogre::Vector3(30.0, 30.0, 10.0);
        sensitivity = 0.8;
        break;
    }

    detectLine =  sceneManager->createManualObject("line");
    detectLineNode = sceneManager->getRootSceneNode()->createChildSceneNode("line_node");

        MaterialPtr detectLineMaterial = MaterialManager::getSingleton().create("lineMaterial","General");
detectLineMaterial->setReceiveShadows(false);
detectLineMaterial->getTechnique(0)->setLightingEnabled(true);
detectLineMaterial->getTechnique(0)->getPass(0)->setDiffuse(1,0,0,0);
detectLineMaterial->getTechnique(0)->getPass(0)->setAmbient(1,0,0);
detectLineMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0);
//detectLineMaterial->dispose();  // dispose pointer, not the material





}

Enemy::~Enemy()
{
    delete detectLine;
    delete detectLineNode;
    delete detectQuery;
    delete detectQueryY;
    delete test;
    //dtor
}


void Enemy::detect()
{


    characterState->getWorldTransform(charTransform);
    Ogre::Vector3 charPos = BtOgre::Convert::toOgre(charTransform.getOrigin());

    Ray detectRay(charPos, Ogre::Vector3::UNIT_X);  ///set up ray pointing forward
    //Ray detectRayY(charPos, Ogre::Vector3::UNIT_Y); ///set up ray point to ground (get rid of this)

    test = sceneManager->createSphereQuery(Ogre::Sphere(charPos, 2050.0));
    test->setQueryMask(Ogre::SceneManager::ENTITY_TYPE_MASK);

    detectQuery = sceneManager->createRayQuery(detectRay);      ///make forward ray
    detectQuery->setQueryTypeMask(Ogre::SceneManager::ENTITY_TYPE_MASK);
    //detectQuery->setSortByDistance(true, 3);

    //detectQueryY = sceneManager->createRayQuery(detectRayY);    ///make ground ray (delete this)
    //detectQueryY->setQueryTypeMask(Ogre::SceneManager::ENTITY_TYPE_MASK);
    //detectQueryY->setSortByDistance(true, 3);

    SceneQueryResult& resultS = test->execute();
    RaySceneQueryResult& result = detectQuery->execute();       ///get forward ray results
    //RaySceneQueryResult& resultY = detectQueryY->execute();     ///get ground ray ray results (delete this)


    if (result.size() == 0 )              ///no y means we are below ground;  no x means player is not in range
    {
        detectRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_X);
        //detectRayY.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);

        detectQuery->setRay(detectRay);
        //detectQueryY->setRay(detectRayY);

        result = detectQuery->execute();
        //resultY = detectQueryY->execute();
    }


    SceneQueryResultMovableList::iterator iS = resultS.movables.begin();
    RaySceneQueryResult::iterator i = result.begin();
    //RaySceneQueryResult::iterator iY = resultY.begin();
    //std::cout << i->worldFragment->singleIntersection.y << "\n";

    cout << "running detect" << endl;
//    if ()
//    {
//            cout << "sphere" << endl;
//    }

    for (; (iS != resultS.movables.end() ) ; iS++)
    {
        if ( (*iS)->getName() != myEntity->getName())
        {

               // cout << "found "<< (*iS)->getName() << endl;
            if ((*iS)->getName() == "Bosco")
            {

                Ogre::Vector3 cPos = (*iS)->getParentNode()->getPosition();

                ///charPos += (cPos - charPos)/35.0;

                //this->walk()



                ///charTransform.setOrigin( btVector3(charPos.x, charPos.y, charPos.z) );
                ///characterState->setWorldTransform(charTransform);
                ///mCharacterBody->setMotionState(characterState);

                detectLine->clear();

                detectLine->begin("lineMaterial", Ogre::RenderOperation::OT_LINE_LIST);
                    detectLine->position(charPos.x, charPos.y, charPos.z);
                    detectLine->position(cPos.x, cPos.y, cPos.z);
// etc
                    detectLine->end();

                    if (detectLineNode->numAttachedObjects() == 0)
                        detectLineNode->attachObject(detectLine);

                    ///ani = myEntity->getAnimationState("walk");


    ///ani->setLoop(true);
    ///ani->setEnabled(true);
    ///ani->addTime(Real(100.0));
    Quaternion turn = charPos.getRotationTo(cPos);
    walk(10.0, 5.5, turn, true);
    cout << "walked" << endl;
            }
        }
    }


    mCharacterBody->setInterpolationWorldTransform(mCharacterBody->getWorldTransform());
    mCharacterBody->setInterpolationLinearVelocity(btVector3(0,0,0));
    mCharacterBody->setInterpolationAngularVelocity(btVector3(0,0,0));
}
