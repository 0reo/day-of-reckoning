//|||||||||||||||||||||||||||||||||||||||||||||||

#include "DemoApp.hpp"
#include "character.h"
#include "Weapon.h"

#include <OgreLight.h>
#include <OgreWindowEventUtilities.h>
#include <cmath>
///*****************
///***********initalize vars
///*****************
DemoApp::DemoApp()
{
    bosco			= 0;
    alien           = 0;

}
///*****************
///************destructor
///*****************
DemoApp::~DemoApp()
{
    delete OgreFramework::getSingletonPtr();
}

///*****************
///**************start up game
///*****************
void DemoApp::startDemo()
{
    new OgreFramework();
    if (!OgreFramework::getSingletonPtr()->initOgre("Day of Reckoning v1.0", this, 0))
        return;

    m_bShutdown = false;

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("DoR initialized!");

    setupDemoScene();
    runDemo();
}

///*****************
///*****************place world objects
///*****************

void DemoApp::setupDemoScene()
{
    OgreFramework::getSingletonPtr()->m_pSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox");

    OgreFramework::getSingletonPtr()->m_pSceneMgr->createLight("Light");


    bosco = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Bosco", "robot.mesh", "boscoNode", OgreFramework::getSingletonPtr()->m_pLog, true);
    bosco->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    bosco->setPosition(0.0, 0.0, 0.0);
    //bosco->addCamera(OgreFramework::getSingletonPtr()->m_pCamera);

   // OgreFramework::getSingletonPtr()->m_pCamera->setOrientation(bosco->getOrientation());
    //OgreFramework::getSingletonPtr()->m_pCamera->pitch(Degree(-35.0f));
    OgreFramework::getSingletonPtr()->m_pCamera->setAutoTracking(true, bosco->myNode);
    //OgreFramework::getSingletonPtr()->m_pCamera->setPosition(bosco->getPosition().x+300, bosco->getPosition().y, bosco->getPosition().z-150);
    Weapon* test = new Weapon(OgreFramework::getSingletonPtr()->m_pSceneMgr, "sword", "RABBIT_03.mesh", "testNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    bosco->addToCharacter(test, OgreFramework::getSingletonPtr()->getWorld());
    test->setPosition(30.0, 30.0, 30.0);
    test->addToWorld(OgreFramework::getSingletonPtr()->getWorld());

    alien = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Alien", "RABBIT_03.mesh", "alienNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    alien->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    alien->scale(10.0,10.0,10.0);
    alien->setPosition(10.0, 0.0, 500.0);

    //bosco->addToCharacter(alien->myEntity);

}

///*****************
///********game loop
///*****************

void DemoApp::runDemo()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Start main loop...");

    double timeSinceLastFrame = 0.0;
    double startTime = 0.0;
    double lastTime = 0.0;

    OgreFramework::getSingletonPtr()->m_pRenderWnd->resetStatistics();

    while (!m_bShutdown && !OgreFramework::getSingletonPtr()->isOgreToBeShutDown())
    {
        //cout << timeSinceLastFrame << endl;
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;

//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        Ogre::WindowEventUtilities::messagePump();
#endif
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
        {
            startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();

            OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
            OgreFramework::getSingletonPtr()->m_pMouse->capture();

            OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
            OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();

            timeSinceLastFrame = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;
            if (!OgreFramework::getSingletonPtr()->pressed)
                bosco->idleState(timeSinceLastFrame);
            else
            {
                if (OgreFramework::getSingletonPtr()->left)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(-1.5, 0.0, -1.0), Quaternion(0.0,  0.0, 1.0,  0.0));
                if (OgreFramework::getSingletonPtr()->right)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(1.5, 0.0, 0.0), Quaternion(0.0,  0.0, -1.0, 0.0));
                if (OgreFramework::getSingletonPtr()->up)
                bosco->move();
                    //bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, -1.5), Quaternion(1.0, 0.0, 1.3, 0.0));
                if (OgreFramework::getSingletonPtr()->down)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, 1.5), Quaternion(1.0, 0.0, 2.2, 0.0));
            }
        }
        else
        {
            sleep(4000);
        }
    }

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Main loop quit");
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F))
    {
        //do something
    }

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

