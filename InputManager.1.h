#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H
#ifndef lout
#define lout OgreFramework::getSingletonPtr()->m_pLog->logMessage

#include "OgreRoot.h"
#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

//class OgreFramework;
using namespace OIS;
//using namespace Ogre;

class DorInputManager: public OIS::KeyListener,  OIS::MouseListener
{
    public:
        DorInputManager();
        DorInputManager(Ogre::RenderWindow*	m_pRendWnd, unsigned long hWnd, OIS::KeyListener *pKeyListener, OIS::MouseListener *pMouseListener);
        virtual ~DorInputManager();

    static DorInputManager* getInstance();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void getInput();


	OIS::InputManager*			m_pInputMgr;
	OIS::Keyboard*				m_pKeyboard;
	OIS::KeyListener*           m_pKeyListener;
	OIS::Mouse*					m_pMouse;
    bool                        pressed;
    bool                        left;
    bool                        right;
    bool                        up;
    bool                        jump;
    bool                        down;
    bool                        camLeft;
    bool                        camRight;
    bool                        camUp;
    bool                        camDown;
    bool                        debug;

    protected:
    private:
        Ogre::RenderWindow*         m_pRenderWnd;
        //static DorInputManager* inputMgr;
};
//DorInputManager* DorInputManager::inputMgr = NULL;
#endif // INPUTMANAGER_H
