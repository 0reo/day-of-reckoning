/*
----------------------------------------------------------------
This is a mini Ogre Application. It does nothing.
by Mythma (mythma@126.com)
----------------------------------------------------------------
*/
#include "stdafx.h"
#include "Ogre.h"
#include "OgreFrameListener.h"
#include "OgreEventListeners.h"
#include "OgreKeyEvent.h"


using namespace Ogre;

//////////////////////////////////////////////////////////////////////////
// class CSimpleFrameListener
//////////////////////////////////////////////////////////////////////////
class CSimpleFrameListener : public FrameListener
{
public:
    CSimpleFrameListener(InputReader* inputReader)
    {
        m_InputReader = inputReader;
    }
    bool frameStarted(const FrameEvent& evt)
    {
        m_InputReader->capture();
            cout << "test2";

//exit if key KC_ESCAPE pressed
        if(m_InputReader->isKeyDown(KC_ESCAPE))
            return false;

        return true;
    }

    bool frameEnded(const FrameEvent& evt)
    {
        return true;
    }
private:
    InputReader* m_InputReader;
};

//////////////////////////////////////////////////////////////////////////
// class CSimpleKeyListener
//////////////////////////////////////////////////////////////////////////
class CSimpleKeyListener : public KeyListener
{
public:
    void keyClicked(KeyEvent* e) {}

    void keyPressed(KeyEvent* e) {}

    void keyReleased(KeyEvent* e) {}
};


//////////////////////////////////////////////////////////////////////////
// class main
//////////////////////////////////////////////////////////////////////////

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
int main(int argc, char **argv)
#endif
{
//----------------------------------------------------
//1 enter ogre
//-----------------------------------------------------
    Root* root = new Root;

//----------------------------------------------------
// 2 Configures the application
//------------------------------------------------------
    root->showConfigDialog();
    root->initialise(true, "Simple Ogre App");

//------------------------------------------------------
// 3 Create the camera
//------------------------------------------------------
    SceneManager* sceneMgr = root->getSceneManager(ST_GENERIC);
    Camera* camera = sceneMgr->createCamera("SimpleCamera");

//------------------------------------------------------
// 4 Create one viewport, entire window
//------------------------------------------------------
    Viewport* viewPort = root->getAutoCreatedWindow()->addViewport(camera);

//---------------------------------------------------
// 5 add event processor
//--------------------------------------------------
    EventProcessor* eventProcessor = new EventProcessor();
    eventProcessor->initialise(root->getAutoCreatedWindow());
    eventProcessor->startProcessingEvents();
    CSimpleKeyListener* keyListener = new CSimpleKeyListener();
    eventProcessor->addKeyListener(keyListener);
    CSimpleFrameListener* frameListener = new CSimpleFrameListener(
        eventProcessor->getInputReader());
    root->addFrameListener(frameListener);

//---------------------------------------------------
// 6 start rendering
//--------------------------------------------------
    root->startRendering();

//------------------------------------------------
// 7 clean
//---------------------------------------------------
    delete frameListener;
    delete keyListener;
    delete eventProcessor;
    delete root;
    return 0;
}
