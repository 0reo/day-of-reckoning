#ifndef GAMECAMERA_H
#define GAMECAMERA_H

#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "character.h"

using namespace Ogre;
using namespace std;


class GameCamera
{
    public:
        GameCamera(Ogre::SceneManager* sceneManager, Ogre::RenderWindow* rendWin, Character* c = NULL, bool followChar = false);
        Ogre::Camera* getCamera();
        void track();
        void track(Character* c);
        bool isTracking();
        virtual ~GameCamera();
    protected:
    private:
    Ogre::Camera*               camera;
   	Ogre::Viewport*				viewport;
    Ogre::RenderWindow*			renderWindow;
    Character*                  focus;
    bool                        tracking;

};

#endif // CAMERA_H


///make scene nodes, and attach THEM to different camera positions.  then just attach the camera to those nodes
