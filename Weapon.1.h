#ifndef WEAPON_H
#define WEAPON_H
#ifndef lout
#define lout OgreFramework::getSingletonPtr()->m_pLog->logMessage
#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"

using namespace Ogre;
using namespace std;


class Weapon
{
    public:
        Weapon(SceneManager* sceneMgr, string weaponName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated);
        virtual ~Weapon();

        void setPosition(Ogre::Vector3 pos);
        void setPosition(Real x, Real y, Real z);
        Ogre::Vector3 getPosition();
        Ogre::Quaternion getOrientation();
        void addToWorld(btDiscreteDynamicsWorld* world);
        void scale(Ogre::Vector3 size);
        void scale(Real x, Real y, Real z);
        bool nextLocation();
        void walk(Real t, Ogre::Vector3 translationVector, Ogre::Quaternion rotate);
        void move();
        Ogre::Log* _log;
        void idleState(Real t);


        Ogre::SceneNode* myNode;
        Ogre::Entity* myEntity;
        SceneManager* sceneManager;
        Ogre::AnimationState* ani;
        RaySceneQuery* query;

        btRigidBody* mWeaponBody;

        btCollisionShape* mWeaponShape;
        BtOgre::RigidBodyState* weaponState;

        Real mMoveSpeed;
        Ogre::Vector3 mDirection;
        Ogre::Vector3 oldPosition;
        btTransform weaponTransform;
        Real mDistance;
        std::deque<Ogre::Vector3> mWalkList;   // The list of points we are walking to





    protected:
    private:

        btScalar mass;
        btVector3 inertia;
    Ogre::Vector3 position;
    Ogre::Quaternion rotation;
};

#endif // WEAPON_H
