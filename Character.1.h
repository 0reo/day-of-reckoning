#ifndef CHARACTER_H
#define CHARACTER_H
#include <Ogre.h>
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "Weapon.h"

using namespace Ogre;
using namespace std;


class Character
{
public:
    Character(SceneManager* sceneMgr, string charName, string meshFileName, string nodeName, Ogre::Log* m_pLog, bool animated);
    virtual ~Character();

    void setPosition(Ogre::Vector3 pos);                                                                                            ///set character position
    void setPosition(Real x, Real y, Real z);                                                                                       ///set character position
    Ogre::Vector3 getPosition();                                                                                                    ///get character position
    Ogre::SceneNode* getNode();                                                                                                     ///return scene node
    Ogre::Quaternion getOrientation();                                                                                              ///return orientation
    void addCamera(Ogre::Camera* cam);                                                                                              ///attach camera to character
    void addToCharacter(Weapon* weapon, btDiscreteDynamicsWorld* world);                                                            ///attach something to character, ie weapons
    void addToWorld(btDiscreteDynamicsWorld* world);                                                                                ///add to bullet world
    void clampToTerrain(BtOgre::RigidBodyState world);                                                                              ///clamp to world
    void scale(Ogre::Vector3 size);                                                                                                 ///scale mesh
    void scale(Real x, Real y, Real z);                                                                                             ///scala mesh
    void walk(Real t, Real speed = 1.5, Ogre::Quaternion rotate = Ogre::Quaternion(1, 0, 0 ,0), bool rot = false);                  ///walk (duh)
    void idleState(Real t);                                                                                                         ///run idle state animation

    ///ogre vars
    Ogre::Log* _log;                        ///log file
    Ogre::SceneNode* myCamera;              ///node to place camera a distance from character
    Ogre::SceneNode* mySightNode;           ///where the camera should look
    Ogre::Entity* myEntity;                 ///the entity(make private)
    SceneManager* sceneManager;              ///scene manager;
    Ogre::Vector3 mDirection;
    Ogre::Vector3 oldPosition;
    Real mDistance;
    std::deque<Ogre::Vector3> mWalkList;    /// The list of points we are walking to

    ///bullet/btogre vars
    btRigidBody* mCharacterBody;            ///actual character
    btCollisionShape* mCharacterShape;      ///bounding shape
    btCompoundShape* test;
    BtOgre::RigidBodyState* characterState; ///character motion state
    btTransform charTransform;              ///character transform info(translate/rotate)
    btTransform worldTransform;             ///world transform info(translate/rotate)

    ///other
    Real mWalkSpeed;
    Weapon* weapons[2];                     ///weapons

bool jumping;                           ///is jumping?




protected:

    btScalar mass;                          ///character mass (0 if kinetic)
    btVector3 inertia;                      ///local inertia

    Ogre::AnimationState* ani;              ///animations
    RaySceneQuery* query;                   ///ray query(for terrain clamping)
    Ogre::Vector3 position;                 ///current position
    Ogre::Quaternion rotation;              ///current orintation

    Ogre::SceneNode* myNode;                ///character sceneNode



    void clampToTerrain();                  ///clamps to terrain

private:
};

#endif // CHARACTER_H

