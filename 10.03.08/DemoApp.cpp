//|||||||||||||||||||||||||||||||||||||||||||||||

#include "DemoApp.hpp"
#include "character.h"
#include "Weapon.h"

#include <OgreLight.h>
#include <OgreWindowEventUtilities.h>
#include <cmath>
///*****************
///***********initalize vars
///*****************
DemoApp::DemoApp()
{
    bosco			= 0;
    alien           = 0;

}
///*****************
///************destructor
///*****************
DemoApp::~DemoApp()
{
    delete bosco;
    delete alien;
    delete OgreFramework::getSingletonPtr();
}

///*****************
///**************start up game
///*****************
void DemoApp::startDemo()
{
    new OgreFramework();
    if (!OgreFramework::getSingletonPtr()->initOgre("Day of Reckoning v10.03.08", this, 0))
        return;


                /// modify this to set up inital render config options
        //RenderSystem *rs = mRoot->getRenderSystemByName("Direct3D9 Rendering Subsystem");
                                            // or use "OpenGL Rendering Subsystem"
        //mRoot->setRenderSystem(rs);
        //rs->setConfigOption("Full Screen", "No");
        //rs->setConfigOption("Video Mode", "800 x 600 @ 32-bit colour");
        ///use this to get settings available
        //Root::getAvailableRenderers

    m_bShutdown = false;

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("DoR initialized!");

    setupDemoScene();
    runDemo();
}

///*****************
///*****************place world objects
///*****************

void DemoApp::setupDemoScene()
{
    OgreFramework::getSingletonPtr()->m_pSceneMgr->setSkyDome(true, "Examples/CloudySky");

    OgreFramework::getSingletonPtr()->m_pSceneMgr->createLight("Light");


    bosco = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Bosco", "robot.mesh", "boscoNode", OgreFramework::getSingletonPtr()->m_pLog, true);
    bosco->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    bosco->setPosition(2000.0, 200.0, 2000.0);
    OgreFramework::getSingletonPtr()->mainCamera->track(bosco);

    Weapon* test = new Weapon(OgreFramework::getSingletonPtr()->m_pSceneMgr, "sword", "Cube.mesh", "testNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    bosco->addToCharacter(test, OgreFramework::getSingletonPtr()->getWorld());
    //test->setPosition(30.0, 30.0, 30.0);
    test->scale(1.0, 20.0, 1.0);
    test->addToWorld(OgreFramework::getSingletonPtr()->getWorld());

    alien = new Character(OgreFramework::getSingletonPtr()->m_pSceneMgr, "Alien", "RABBIT_03.mesh", "alienNode", OgreFramework::getSingletonPtr()->m_pLog, false);
    alien->addToWorld(OgreFramework::getSingletonPtr()->getWorld());
    alien->scale(10.0,10.0,10.0);
    alien->setPosition(10.0, 0.0, 500.0);

    //bosco->addToCharacter(alien->myEntity);

}

///*****************
///********game loop
///*****************

void DemoApp::runDemo()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Start main loop...");

    double timeSinceLastFrame = 0.0;
    double startTime = 0.0;
    double lastTime = 0.0;

    OgreFramework::getSingletonPtr()->m_pRenderWnd->resetStatistics();

    while (!m_bShutdown && !OgreFramework::getSingletonPtr()->isOgreToBeShutDown())
    {
        //cout << timeSinceLastFrame << endl;
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isClosed())m_bShutdown = true;

//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_LINUX
        Ogre::WindowEventUtilities::messagePump();
#endif
        if (OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
        {
            startTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();

            OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
            OgreFramework::getSingletonPtr()->m_pMouse->capture();


            OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
            OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();

            timeSinceLastFrame = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - startTime;
            if (!OgreFramework::getSingletonPtr()->pressed)
                bosco->idleState(timeSinceLastFrame);
            else
            {
                if (OgreFramework::getSingletonPtr()->left)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(-1.5, 0.0, -1.0), Quaternion(0.0,  0.0, 1.0,  0.0));
                if (OgreFramework::getSingletonPtr()->right)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(1.5, 0.0, 0.0), Quaternion(0.0,  0.0, -1.0, 0.0));
                if (OgreFramework::getSingletonPtr()->up)
                  bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, -1.5), Quaternion(1.0, 0.0, 1.3, 0.0));
                //bosco->move(timeSinceLastFrame);

                if (OgreFramework::getSingletonPtr()->down)
                    bosco->walk(timeSinceLastFrame, Ogre::Vector3(0.0, 0.0, 1.5), Quaternion(1.0, 0.0, 2.2, 0.0));
            }
//            bosco->clampToTerrain(*OgreFramework::getSingletonPtr()->terrainState);
        }
        else
        {
            sleep(4000);
        }
    }

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Main loop quit");
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Shutdown OGRE...");
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    if (OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_F))
    {
        //do something
    }

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

bool DemoApp::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);

    return true;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

